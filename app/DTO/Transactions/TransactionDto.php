<?php

namespace App\DTO\Transactions;

use App\DTO\BaseDto;
use App\Entity\Bill\Bill;

class TransactionDto extends BaseDto
{

    /** @var string */
    private $receipt;

    /** @var string */
    private $platform;

    /** @var Bill */
    private $bill;

    /**
     * @return Bill
     */
    public function getBill(): Bill
    {
        return $this->bill;
    }

    /**
     * @param Bill $bill
     */
    public function setBill(Bill $bill): void
    {
        $this->bill = $bill;
    }

    /**
     * @return string
     */
    public function getReceipt(): string
    {
        return $this->receipt;
    }

    /**
     * @param string $receipt
     */
    public function setReceipt(string $receipt): void
    {
        $this->receipt = $receipt;
    }

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform(string $platform): void
    {
        $this->platform = $platform;
    }

}
