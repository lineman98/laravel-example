<?php

namespace App\DTO\Claims;

use App\DTO\BaseDto;
use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class ClaimCreateDto extends BaseDto {

    /** @var string */
    private $content;

    /** @var User */
    private $user;

    /** @var Model */
    private $claimable;

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Model
     */
    public function getClaimable(): Model
    {
        return $this->claimable;
    }

    /**
     * @param Model $claimable
     */
    public function setClaimable(Model $claimable): void
    {
        $this->claimable = $claimable;
    }

}
