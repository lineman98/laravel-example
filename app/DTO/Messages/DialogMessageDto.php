<?php

namespace App\DTO\Messages;

use App\DTO\BaseDto;
use App\Entity\Messages\Dialog;
use App\Entity\Users\User;

class DialogMessageDto extends BaseDto
{

    /** @var string */
    private $content;

    /** @var User */
    private $user_from;

    /** @var User */
    private $dialog;

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return User
     */
    public function getUserFrom(): User
    {
        return $this->user_from;
    }

    /**
     * @param User $user_from
     */
    public function setUserFrom(User $user_from): void
    {
        $this->user_from = $user_from;
    }

    /**
     * @return User
     */
    public function getDialog(): Dialog
    {
        return $this->dialog;
    }

    /**
     * @param Dialog $dialog
     */
    public function setDialog(Dialog $dialog): void
    {
        $this->dialog = $dialog;
    }

}
