<?php

namespace App\DTO\Users;

use App\DTO\BaseDto;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Options\Eye\Eye;
use App\Entity\Options\Hair\Hair;
use App\Entity\Options\Nationality\Nationality;

class UserUpdateDto extends BaseDto {

    /** @var string */
    private $name;

    /** @var string|null */
    private $email;

    /** @var int|null */
    private $age;

    /** @var int|null */
    private $boobs_size;

    /** @var string|null */
    private $boobs_type;

    /** @var int|null */
    private $weight;

    /** @var int|null */
    private $height;

    /** @var int|null */
    private $bust;

    /** @var int|null */
    private $waist;

    /** @var int|null */
    private $hip;

    /** @var bool|null */
    private $has_foreign_passport;

    /** @var Country|null */
    private $country;

    /** @var City|null */
    private $city;

    /** @var Eye|null */
    private $eye;

    /** @var Hair|null */
    private $hair;

    /** @var Nationality|null */
    private $nationality;

    /** @var int|null */
    private $minimum_cost;

    /** @var string|null */
    private $whatsapp;

    /** @var int[]|null */
    private $model_statuses;

    /** @var int[]|null */
    private $statuses;

    /** @var int[]|null */
    private $offer_categories;

    /** @var int[]|null */
    private $visas;

    /** @var string|null */
    private $role;

    /** @var string|null */
    private $gender;

    /** @var bool|null */
    private $can_create_offers;

    /** @var bool|null */
    private $can_create_tours;

    /** @var string|null */
    private $website;

    /** @var string|null */
    private $aboutme;

    /**
     * @return int[]|null
     */
    public function getVisas(): ?array
    {
        return $this->visas;
    }

    /**
     * @param int[]|null $visas
     */
    public function setVisas(?array $visas): void
    {
        $this->visas = $visas;
    }

    /**
     * @return int[]|null
     */
    public function getOfferCategories(): ?array
    {
        return $this->offer_categories;
    }

    /**
     * @param int[]|null $offer_categories
     */
    public function setOfferCategories(?array $offer_categories): void
    {
        $this->offer_categories = $offer_categories;
    }

    /**
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string|null $role
     */
    public function setRole(?string $role): void
    {
        $this->role = $role;
    }


    /**
     * @return int[]|null
     */
    public function getModelStatuses(): ?array
    {
        return $this->model_statuses;
    }

    /**
     * @param int[]|null $model_statuses
     */
    public function setModelStatuses(?array $model_statuses): void
    {
        $this->model_statuses = $model_statuses;
    }

    /**
     * @return int[]|null
     */
    public function getStatuses(): ?array
    {
        return $this->statuses;
    }

    /**
     * @param int[]|null $statuses
     */
    public function setStatuses(?array $statuses): void
    {
        $this->statuses = $statuses;
    }

    /**
     * @return string|null
     */
    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    /**
     * @param string|null $whatsapp
     */
    public function setWhatsapp(?string $whatsapp): void
    {
        $this->whatsapp = $whatsapp;
    }

    /**
     * @return int|null
     */
    public function getMinimumCost(): ?int
    {
        return $this->minimum_cost;
    }

    /**
     * @param int|null $minimum_cost
     */
    public function setMinimumCost(?int $minimum_cost): void
    {
        $this->minimum_cost = $minimum_cost;
    }

    /**
     * @return string
     */
    public function getBoobsType(): ?string
    {
        return $this->boobs_type;
    }

    /**
     * @param string $boobs_type
     */
    public function setBoobsType(?string $boobs_type): void
    {
        $this->boobs_type = $boobs_type;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    /**
     * @return City|null
     */
    public function getCity(): ?City
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getAboutme(): ?string
    {
        return $this->aboutme;
    }

    /**
     * @param City $city
     */
    public function setCity(?City $city): void
    {
        $this->city = $city;
    }

    /**
     * @param String $aboutme
     */
    public function setAboutme(?string $aboutme): void
    {
        $this->aboutme = $aboutme;
    }

    /**
     * @return Eye|null
     */
    public function getEye(): ?Eye
    {
        return $this->eye;
    }

    /**
     * @param Eye|null $eye
     */
    public function setEye(?Eye $eye): void
    {
        $this->eye = $eye;
    }

    /**
     * @return Hair|null
     */
    public function getHair(): ?Hair
    {
        return $this->hair;
    }

    /**
     * @param Hair|null $hair
     */
    public function setHair(?Hair $hair): void
    {
        $this->hair = $hair;
    }

    /**
     * @return Nationality|null
     */
    public function getNationality(): ?Nationality
    {
        return $this->nationality;
    }

    /**
     * @param Nationality|null $nationality
     */
    public function setNationality(?Nationality $nationality): void
    {
        $this->nationality = $nationality;
    }

    /**
     * @return int
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(?int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getBoobsSize(): ?int
    {
        return $this->boobs_size;
    }

    /**
     * @param int $boobs_size
     */
    public function setBoobsSize(?int $boobs_size): void
    {
        $this->boobs_size = $boobs_size;
    }

    /**
     * @return int
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(?int $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(?int $height): void
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getBust(): ?int
    {
        return $this->bust;
    }

    /**
     * @param int $bust
     */
    public function setBust(?int $bust): void
    {
        $this->bust = $bust;
    }

    /**
     * @return int
     */
    public function getWaist(): ?int
    {
        return $this->waist;
    }

    /**
     * @param int $waist
     */
    public function setWaist(?int $waist): void
    {
        $this->waist = $waist;
    }

    /**
     * @return int
     */
    public function getHip(): ?int
    {
        return $this->hip;
    }

    /**
     * @param int $hip
     */
    public function setHip(?int $hip): void
    {
        $this->hip = $hip;
    }

    /**
     * @return bool
     */
    public function isHasForeignPassport(): ?bool
    {
        return $this->has_foreign_passport;
    }

    /**
     * @param bool $has_foreign_passport
     */
    public function setHasForeignPassport(?bool $has_foreign_passport): void
    {
        $this->has_foreign_passport = $has_foreign_passport;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }


    /**
     * @return bool|null
     */
    public function canCreateOffers(): ?bool
    {
        return $this->can_create_offers;
    }

    /**
     * @param bool|null $can_create_offers
     */
    public function setCanCreateOffers(?bool $can_create_offers): void
    {
        $this->can_create_offers = $can_create_offers;
    }

    /**
     * @return bool|null
     */
    public function canCreateTours(): ?bool
    {
        return $this->can_create_tours;
    }

    /**
     * @param bool|null $can_create_tours
     */
    public function setCanCreateTours(?bool $can_create_tours): void
    {
        $this->can_create_tours = $can_create_tours;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @param string|null $website
     */
    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }
}
