<?php

namespace App\DTO\Users;

use App\DTO\BaseDto;

class PushTokenCreateDto extends BaseDto {

    /** @var string */
    private $token;

    /** @var string|null */
    private $device;

    /** @var string|null */
    private $os;

    /**
     * @return string|null
     */
    public function getOs(): ?string
    {
        return $this->os;
    }

    /**
     * @param string|null $os
     */
    public function setOs(?string $os): void
    {
        $this->os = $os;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string|null
     */
    public function getDevice(): ?string
    {
        return $this->device;
    }

    /**
     * @param string|null $device
     */
    public function setDevice(?string $device): void
    {
        $this->device = $device;
    }

}
