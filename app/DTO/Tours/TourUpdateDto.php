<?php

namespace App\DTO\Tours;

use App\DTO\BaseDto;
use App\Entity\Geo\Country\Country;
use App\Entity\Users\User;

class TourUpdateDto extends BaseDto {

    /** @var string */
    private $description;

    /** @var Country */
    private $country;

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }

}
