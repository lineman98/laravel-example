<?php

namespace App\DTO\Offers;

use App\DTO\BaseDto;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Entity\Options\Eye\Eye;
use App\Entity\Options\Hair\Hair;
use App\Entity\Options\Nationality\Nationality;
use App\Entity\Users\User;

class OfferUpdateDto extends BaseDto {

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $description;

    /** @var Country|null */
    private $country;

    /** @var City|null */
    private $city;

    /** @var int|null */
    private $age_from;

    /** @var int|null */
    private $age_to;

    /** @var int|null */
    private $weight_from;

    /** @var int|null */
    private $weight_to;

    /** @var int|null */
    private $height_from;

    /** @var int|null */
    private $height_to;

    /** @var int|null */
    private $boobs_from;

    /** @var int|null */
    private $boobs_to;

    /** @var string|null */
    private $boobs_type;

    /** @var int|null */
    private $cost;

    /** @var bool|null */
    private $possible_increase_cost;

    /** @var bool|null */
    private $only_with_reviews;

    /** @var bool|null */
    private $only_verified;

    /** @var string|null */
    private $status;

    /** @var int[]|null */
    private $nationalities;

    /** @var int[]|null */
    private $eyes;

    /** @var int[]|null */
    private $hairs;

    /** @var string|null */
    private $meeting_date;

    /** @var int[]|null */
    private $visas;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country|null $country
     */
    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    /**
     * @return City|null
     */
    public function getCity(): ?City
    {
        return $this->city;
    }

    /**
     * @param City|null $city
     */
    public function setCity(?City $city): void
    {
        $this->city = $city;
    }

    /**
     * @return int[]|null
     */
    public function getVisas(): ?array
    {
        return $this->visas;
    }

    /**
     * @param int[]|null $visas
     */
    public function setVisas(?array $visas): void
    {
        $this->visas = $visas;
    }

    /**
     * @return int|null
     */
    public function getAgeFrom(): ?int
    {
        return $this->age_from;
    }

    /**
     * @param int|null $age_from
     */
    public function setAgeFrom(?int $age_from): void
    {
        $this->age_from = $age_from;
    }

    /**
     * @return int|null
     */
    public function getAgeTo(): ?int
    {
        return $this->age_to;
    }

    /**
     * @param int|null $age_to
     */
    public function setAgeTo(?int $age_to): void
    {
        $this->age_to = $age_to;
    }

    /**
     * @return int|null
     */
    public function getWeightFrom(): ?int
    {
        return $this->weight_from;
    }

    /**
     * @param int|null $weight_from
     */
    public function setWeightFrom(?int $weight_from): void
    {
        $this->weight_from = $weight_from;
    }

    /**
     * @return int|null
     */
    public function getWeightTo(): ?int
    {
        return $this->weight_to;
    }

    /**
     * @param int|null $weight_to
     */
    public function setWeightTo(?int $weight_to): void
    {
        $this->weight_to = $weight_to;
    }

    /**
     * @return int|null
     */
    public function getHeightFrom(): ?int
    {
        return $this->height_from;
    }

    /**
     * @param int|null $height_from
     */
    public function setHeightFrom(?int $height_from): void
    {
        $this->height_from = $height_from;
    }

    /**
     * @return int|null
     */
    public function getHeightTo(): ?int
    {
        return $this->height_to;
    }

    /**
     * @param int|null $height_to
     */
    public function setHeightTo(?int $height_to): void
    {
        $this->height_to = $height_to;
    }

    /**
     * @return int|null
     */
    public function getBoobsFrom(): ?int
    {
        return $this->boobs_from;
    }

    /**
     * @param int|null $boobs_from
     */
    public function setBoobsFrom(?int $boobs_from): void
    {
        $this->boobs_from = $boobs_from;
    }

    /**
     * @return int|null
     */
    public function getBoobsTo(): ?int
    {
        return $this->boobs_to;
    }

    /**
     * @param int|null $boobs_to
     */
    public function setBoobsTo(?int $boobs_to): void
    {
        $this->boobs_to = $boobs_to;
    }

    /**
     * @return string|null
     */
    public function getBoobsType(): ?string
    {
        return $this->boobs_type;
    }

    /**
     * @param string|null $boobs_type
     */
    public function setBoobsType(?string $boobs_type): void
    {
        $this->boobs_type = $boobs_type;
    }

    /**
     * @return int|null
     */
    public function getCost(): ?int
    {
        return $this->cost;
    }

    /**
     * @param int|null $cost
     */
    public function setCost(?int $cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return bool
     */
    public function isPossibleIncreaseCost(): bool
    {
        return $this->possible_increase_cost;
    }

    /**
     * @param bool $possible_increase_cost
     */
    public function setPossibleIncreaseCost(bool $possible_increase_cost): void
    {
        $this->possible_increase_cost = $possible_increase_cost;
    }

    /**
     * @return bool|null
     */
    public function isOnlyWithReviews(): ?bool
    {
        return $this->only_with_reviews;
    }

    /**
     * @param bool|null $only_with_reviews
     */
    public function setOnlyWithReviews(?bool $only_with_reviews): void
    {
        $this->only_with_reviews = $only_with_reviews;
    }

    /**
     * @return bool|null
     */
    public function isOnlyVerified(): ?bool
    {
        return $this->only_verified;
    }

    /**
     * @param bool|null $only_verified
     */
    public function setOnlyVerified(?bool $only_verified): void
    {
        $this->only_verified = $only_verified;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int[]|null
     */
    public function getNationalities(): ?array
    {
        return $this->nationalities;
    }

    /**
     * @param int[]|null $nationalities
     */
    public function setNationalities(?array $nationalities): void
    {
        $this->nationalities = $nationalities;
    }

    /**
     * @return int[]|null
     */
    public function getEyes(): ?array
    {
        return $this->eyes;
    }

    /**
     * @param int[]|null $eyes
     */
    public function setEyes(?array $eyes): void
    {
        $this->eyes = $eyes;
    }

    /**
     * @return int[]|null
     */
    public function getHairs(): ?array
    {
        return $this->hairs;
    }

    /**
     * @param int[]|null $hairs
     */
    public function setHairs(?array $hairs): void
    {
        $this->hairs = $hairs;
    }

    /**
     * @return string|null
     */
    public function getMeetingDate(): ?string
    {
        return $this->meeting_date;
    }

    /**
     * @param string|null $meeting_date
     */
    public function setMeetingDate(?string $meeting_date): void
    {
        $this->meeting_date = $meeting_date;
    }

}
