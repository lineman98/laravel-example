<?php
namespace App\DTO\Offers\OfferUsers;

use App\DTO\BaseDto;

class OfferUserUpdateDto extends BaseDto
{
    /** @var string|null */
    private $message;

    /** @var enum */
    private $status;

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }
}
