<?php

namespace App\DTO\Reviews;

use App\DTO\BaseDto;
use App\Entity\Offers\OfferLink;
use App\Entity\Users\User;

class ReviewCreateDto extends BaseDto {

    /** @var string */
    private $content;

    /** @var User */
    private $user;

    /** @var User */
    private $userFrom;

    /** @var OfferLink|null */
    private $offerLink;

    /** @var bool */
    private $positive;

    /**
     * @return bool
     */
    public function isPositive(): bool
    {
        return $this->positive;
    }

    /**
     * @param bool $positive
     */
    public function setPositive(bool $positive): void
    {
        $this->positive = $positive;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUserFrom(): User
    {
        return $this->userFrom;
    }

    /**
     * @param User $userFrom
     */
    public function setUserFrom(User $userFrom): void
    {
        $this->userFrom = $userFrom;
    }

    /**
     * @return OfferLink|null
     */
    public function getOfferLink(): ?OfferLink
    {
        return $this->offerLink;
    }

    /**
     * @param OfferLink|null $offerLink
     */
    public function setOfferLink(?OfferLink $offerLink): void
    {
        $this->offerLink = $offerLink;
    }

}
