<?php

namespace App\Http\Resources\Options;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;

class NationalityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_male' => $this->name_male,
            'name_female' => $this->name_female
        ];
    }
}
