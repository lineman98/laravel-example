<?php

namespace App\Http\Resources\Offers;

use App\Http\Resources\Users\UserShortResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user ? new UserShortResource($this->user) : null,
            'offer_id' => $this->offer_id,
            //'offer' => $this->offer ? new OfferResource($this->offer) : null,
            'status' => $this->status,
            'message' => $this->message,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
