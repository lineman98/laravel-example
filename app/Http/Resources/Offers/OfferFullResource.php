<?php

namespace App\Http\Resources\Offers;

use App\Http\Resources\Geo\CityResource;
use App\Http\Resources\Geo\CountryResource;
use App\Http\Resources\Users\GirlInOfferResource;
use App\Http\Resources\Users\GirlResource;
use App\Http\Resources\Users\UserShortResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Messages\DialogResource;

class OfferFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category' => $this->category ? new OfferCategoryResource($this->category) : null,
            'city' => $this->city ? new CityResource($this->city) : null,
            'country' => $this->country ? new CountryResource($this->country) : null,
            'city_from' =>  $this->cityFrom ? new CityResource($this->cityFrom) : null,
            'country_from' => $this->countryFrom ? new CountryResource($this->countryFrom) : null,
            'nearest_city_range' => $this->nearest_city_range,
            'age_from' => $this->age_from,
            'age_to' => $this->age_to,
            'weight_from' => $this->weight_from,
            'weight_to' => $this->weight_to,
            'height_from' => $this->height_from,
            'height_to' => $this->height_to,
            'boobs_from' => $this->boobs_from,
            'boobs_to' => $this->boobs_to,
            'boobs_type' => $this->boobs_type,
            'cost' => $this->cost,
            'meeting_date' => $this->meeting_date,
            'possible_increase_cost' => $this->possible_increase_cost,
            'only_with_reviews' => $this->only_with_reviews,
            'only_verified' => $this->only_verified,
            'status' => $this->status,
            'nationalities' => $this->nationalities->pluck('name'),
            'nationalities_ids' => $this->nationalities->pluck('id'),
            'eyes' => $this->eyes->pluck('name'),
            'eyes_ids' => $this->eyes->pluck('id'),
            'hairs' => $this->hairs->pluck('name'),
            'hairs_ids' => $this->hairs->pluck('id'),
            'visas' => $this->visas->pluck('name'),
            'visas_ids' => $this->visas->pluck('id'),
            'wait_models_count' =>  $this->waitModels()->count(),
            'refused_by_manager_models_count' =>  $this->refusedByManagerModels()->count(),
            'refused_models_count' =>  $this->refusedModels()->count(),
            'agreed_models' => GirlInOfferResource::collection($this->agreedModels),
            'agreed_by_manager_models' =>  GirlInOfferResource::collection($this->agreedByManagerModels),
            'manager' => $this->user ? new UserShortResource($this->user) : null,
            'started_at' => $this->started_at,
            'stopped_at' => $this->stopped_at,
            'created_at' => $this->created_at,
            'rejection_reason' => $this->rejection_reason,
            'application' => $this->application ? new OfferUserResource($this->application) : null,
            'users_going' => $this->application && $this->usersGoing ? UserShortResource::collection($this->usersGoing) : null,
            'users_requesting' => $this->application && $this->usersRequesting ? UserShortResource::collection($this->usersRequesting) : null,
            'dialog' => new DialogResource($this->dialog),
            'viewed' => !empty(\Auth::user()) ? $this->views()->where('users.id', \Auth::user()->id)->count() > 0 : null
        ];
    }
}
