<?php

namespace App\Http\Resources\Tours;

use App\Http\Resources\Geo\CityResource;
use App\Http\Resources\Geo\CountryResource;
use App\Http\Resources\Users\UserShortResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TourResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $city = $this->country ? $this->country->cities()->first() : null;
        return [
            'id' => $this->id,
            'city' => $city ? new CityResource($city) : null,
            'country' => $this->country ? new CountryResource($this->country) : null,
            'status' => $this->status,
            'description' => $this->description,
            'manager' => $this->user ? new UserShortResource($this->user) : null,
            'created_at' => $this->created_at,
        ];
    }
}
