<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UserImageResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tiny' => $this->getUrl('tiny'),
            'small' => $this->getUrl('small'),
            'big' => $this->getUrl('big'),
            'avatar' => isset($this->custom_properties['avatar'])
                ? $this->custom_properties['avatar']
                : false,
            'checked' => isset($this->custom_properties['checked'])
                ? $this->custom_properties['checked']
                : false,
        ];
    }
}
