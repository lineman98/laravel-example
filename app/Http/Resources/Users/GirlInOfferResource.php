<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class GirlInOfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'avatar' => $this->avatar ? new UserImageResource($this->avatar) : null,
            'verified' => $this->verified,
            'type' => $this->type,
            'reviews_positive_count' => $this->reviewsPositiveCount(),
            'reviews_negative_count' => $this->reviewsNegativeCount(),
            'created_at' => $this->created_at,
            'offer_link_status' => $this->pivot->status,
            'offer_link_id' => $this->pivot->id,
        ];
    }
}
