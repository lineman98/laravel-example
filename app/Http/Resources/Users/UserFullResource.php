<?php

namespace App\Http\Resources\Users;

use App\Entity\Users\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $countryName = $this->country->name ?? null;
        $cityName = $this->city->name ?? null;
        $hairName = $this->hair->name ?? null;
        $eyeName = $this->eye->name ?? null;
        $nationalityName = $this->nationality ? ($this->gender == 'male' ?
                                                    $this->nationality->name_male :
                                                    $this->nationality->name_female)
                                                : null;

        // есть ли этот пользователь в блеклисте у залогиненного пользователя
        $inBlackList = auth()->user() ? auth()->user()->hasInBlackList($this->resource) : false;
        // если ли залогиненный пользователь в блеклисте у запрашиваемого пользователя
        $inUsersBlackList = auth()->user() ? $this->resource->hasInBlackList(auth()->user()) : false;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'avatar' => $this->avatar ? new UserImageResource($this->avatar) : null,
            'verified' => $this->verified,
            'type' => $this->type,
            'reviews_positive_count' => $this->reviewsPositiveCount(),
            'reviews_negative_count' => $this->reviewsNegativeCount(),
            'created_at' => $this->created_at,
            'country' => $this->country_id,
            'countryName' => $countryName,
            'city' => $this->city_id,
            'cityName' => $cityName,
            'photos' => UserImageResource::collection(
                $this->getMedia(User::PUBLIC_PHOTOS, ['checked' => true])
            ),
            'videos' => UserVideoResource::collection(
                $this->getMedia(User::PUBLIC_VIDEOS, ['checked' => true])
            ),
            'hair' => $this->hair_id,
            'hairName' => $hairName,
            'eye' => $this->eye_id,
            'eyeName' => $eyeName,
            'nationality' => $this->nationality_id,
            'nationalityName' => $nationalityName,
            'visas' => $this->visas->count() ? $this->visas->pluck('name')->join(',') : null,
            'age' => $this->age,
            'hip' => $this->hip,
            'waist' => $this->waist,
            'bust' => $this->bust,
            'boobs_size' => $this->boobs_size,
            'boobs_type' => $this->boobs_type,
            'weight' => $this->weight,
            'height' => $this->height,
            'minimum_cost' => $this->minimum_cost,
            'whatsapp' => !$inBlackList ? $this->whatsapp : null,
            'in_blacklist' => $inBlackList,
            'in_users_blacklist' => $inUsersBlackList,
            'has_foreign_passport' => $this->has_foreign_passport,
            'last_seen' => $this->last_seen,
            'online' => $this->online,
            'gender' => $this->gender,
            'website' => $this->website,
            'aboutme' => $this->aboutme

        ];
    }
}
