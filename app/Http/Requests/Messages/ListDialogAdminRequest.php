<?php

namespace App\Http\Requests\Messages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ListDialogAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $admin_user_id = config('support.admin_support_id');
        return [
            'dialog_id' => [
                'required',
                Rule::exists('dialogs', 'id')
                    ->where(function($query) use ($admin_user_id) {
                        $query->where('user_first', $admin_user_id)
                            ->orWhere('user_second', $admin_user_id);
                    })
            ]
        ];
    }
}
