<?php

namespace App\Http\Requests\Messages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ListDialogRequest
 *
 * @queryParam dialog_id int required Id of dialog: Example: 1
 * @package App\Http\Requests\Messages
 */
class ListDialogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dialog_id' => [
                'required',
                'exists:dialogs,id',
                /*
                Rule::exists('dialog_users', 'dialog_id')
                    ->where(function($query) {
                        $query->where('user_id', $this->user()->id);
                    }*/
            ]
        ];
    }
}
