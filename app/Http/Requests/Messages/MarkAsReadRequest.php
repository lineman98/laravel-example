<?php

namespace App\Http\Requests\Messages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class MarkAsReadRequest
 * @package App\Http\Requests\Messages
 *
 * @bodyParam id int required Id of the message
 */
class MarkAsReadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                Rule::exists('messages', 'id')
                    ->where('user_to_id', $this->user()->id)
            ]
        ];
    }
}
