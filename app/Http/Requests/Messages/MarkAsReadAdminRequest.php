<?php

namespace App\Http\Requests\Messages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MarkAsReadAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $admin_user_id = config('support.admin_support_id');
        return [
            'id' => [
                'required',
                Rule::exists('messages', 'id')
                    ->where('user_to_id', $admin_user_id)
            ]
        ];
    }
}
