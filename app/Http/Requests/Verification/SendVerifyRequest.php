<?php

namespace App\Http\Requests\Verification;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SendVerifyRequest
 * @package App\Http\Requests\Verification
 *
 * @code code string required Verification code. Example: 8532
 */
class SendVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|min:4|max:4'
        ];
    }
}
