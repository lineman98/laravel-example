<?php

namespace App\Http\Requests\Offers;

use App\DTO\Offers\OfferUpdateDto;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Offers
 *
 * @bodyParam description string Description (10-1000) Example: Lorem ipsum dolor sit amen
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //if(!$this->user()->isAdmin())
            return [
                'description' => 'nullable|min:10|max:1000',
            ];

        return [
            'name' => 'nullable|min:3|max:30',
            'description' => 'nullable|min:10|max:1000',
            'country' => 'nullable|exists:countries,id',
            'city' => [
                'nullable',
                Rule::exists('cities', 'id')
                    ->where('country_id', $this->get('country'))
            ],
            'age_from' => 'nullable|integer|min:18|max:60',
            'age_to' => 'nullable|integer|min:18|max:60',
            'weight_from' => 'nullable|integer|min:30|max:200',
            'weight_to' => 'nullable|integer|min:30|max:200',
            'height_from' => 'nullable|integer|min:150|max:250',
            'height_to' => 'nullable|integer|min:150|max:250',
            'boobs_from' => 'nullable|integer|min:1|max:10',
            'boobs_to' => 'nullable|integer|min:1|max:10',
            'boobs_type' => [
                'nullable',
                Rule::in(['all', 'natural', 'silicon'])
            ],
            'cost' => 'nullable|integer|min:100|max:1000000',
            'meeting_date' => 'nullable|date_format:d.m.Y',
            'possible_increase_cost' => 'nullable|boolean',
            'only_with_reviews' => 'nullable|boolean',
            'only_verified' => 'nullable|boolean',
            'nationalities.*' => 'nullable|exists:nationalities,id',
            'eyes.*' => 'nullable|exists:eyes,id',
            'hairs.*' => 'nullable|exists:hairs,id',
            'visas.*' => 'nullable|exists:visas,id',
            'search_from_same_city' => 'nullable|boolean',
            "nearest_city_range" => ['nullable', Rule::in([50, 100, 250, 500])],
        ];
    }

    public function getDto(): OfferUpdateDto
    {
        if(!$this->user()->isAdmin())
            return new OfferUpdateDto([
                'description' => $this->get('description'),
            ]);

        return new OfferUpdateDto([
            'name' => $this->get('name'),
            'description' => $this->get('description'),
            'country' => Country::find($this->get('country')),
            'city' => City::find($this->get('city')),
            'age_from' => $this->get('age_from'),
            'age_to' => $this->get('age_to'),
            'weight_from' => $this->get('weight_from'),
            'weight_to' => $this->get('weight_to'),
            'height_from' => $this->get('height_from'),
            'height_to' => $this->get('height_to'),
            'boobs_from' => $this->get('boobs_from'),
            'boobs_to' => $this->get('boobs_to'),
            'boobs_type' => $this->get('boobs_type'),
            'cost' => $this->get('cost'),
            'meeting_date' => $this->get('meeting_date'),
            'possible_increase_cost' => $this->get('possible_increase_cost'),
            'only_with_reviews' => $this->get('only_with_reviews'),
            'only_verified' => $this->get('only_verified'),
            'user' => $this->user(),
            'nationalities' => $this->get('nationalities'),
            'eyes' => $this->get('eyes'),
            'hairs' => $this->get('hairs'),
            'visas' => $this->get('visas'),
            'search_from_same_city' => $this->get('search_from_same_city', false),
            'nearest_city_range' => $this->get('nearest_city_range'),
        ]);
    }
}
