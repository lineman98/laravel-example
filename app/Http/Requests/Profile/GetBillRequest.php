<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class GetBillRequest
 * @package App\Http\Requests\Profile
 *
 * @bodyParam id int required Id of bill. Example: 1
 */
class GetBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                Rule::exists('bills', 'id')
                    ->where('user_id', $this->user()->id)
            ]
        ];
    }
}
