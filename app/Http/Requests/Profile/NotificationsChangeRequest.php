<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class NotificationsChangeRequest
 * @package App\Http\Requests\Profile
 *
 * @bodyParam receive_offers_notifications bool required Recieve offers notifications Example: true
 * @bodyParam receive_messages_notifications bool required Recieve messages notifications Example: true
 */
class NotificationsChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'receive_offers_notifications' => 'required|boolean',
            'receive_messages_notifications' => 'required|boolean',
        ];
    }
}
