<?php

namespace App\Http\Requests\Reviews;

use App\DTO\Reviews\ReviewCreateDto;
use App\Entity\Offers\OfferLink;
use App\Entity\Users\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required|max:1000',
            'user' => [
                'required',
                Rule::exists('users', 'id'),
            ],
            'offer_link' => [
                'nullable',
                Rule::exists('offer_links', 'id')
                    ->where('user_id', $this->user()->id)
            ],
            'positive' => 'required|boolean'
        ];
    }

    public function getDto(): ReviewCreateDto
    {
        return new ReviewCreateDto([
            'content' => $this->get('content'),
            'user' => User::find($this->get('user')),
            'offerLink' => $this->get('offer_link') ? OfferLink::find($this->get('offer_link')) : null,
            'positive' => !!$this->get('positive'),
            'userFrom' => $this->user()
        ]);
    }
}
