<?php

namespace App\Http\Requests\Bills;

use App\DTO\Transactions\TransactionDto;
use App\Entity\Bill\Bill;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bill' => 'required|exists:bills,id',
            'platform' => ['required', Rule::in(['ios', 'android'])],
            'receipt' => 'required',
        ];
    }

    /**
     * @return TransactionDto
     */
    public function getDto(): TransactionDto
    {
        return new TransactionDto([
            'bill' => Bill::find($this->get('bill')),
            'receipt' => $this->get('receipt'),
            'platform' => $this->get('platform')
        ]);
    }

}
