<?php

namespace App\Http\Requests\Users;

use App\DTO\Users\UserUpdateDto;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Options\Eye\Eye;
use App\Entity\Options\Hair\Hair;
use App\Entity\Options\Nationality\Nationality;
use App\Entity\Users\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Users
 *
 * @bodyParam name string required Name. Example: Alexander
 * @bodyParam country int required Id of country: Example: 1
 * @bodyParam city int required Id of city. Example: 1
 * @bodyParam age int User age. Example: 39
 * @bodyParam weight int required User weight. Example: 88
 * @bodyParam height int required User height. Example: 180
 * @bodyParamm has_foreign_passport bool Is has foreighn passport. Example: true
 * @bodyParam eye int Id of eye type. Example: 1
 * @bodyParam hair int Id of hair type. Example: 1
 * @bodyParam nationality int Id of nationality. Example: 1
 * @bodyParam minimum_cost int Minimal cost. Example: 20
 * @bodyParam whatsapp string Whatsapp number. Example: +79343424324
 * @bodyParam visas.* int Ids of visas types. Example: 1,2
 * @bodyParam gender string male \ female. Example: female
 * @bodyParam website string Url of website. Example: http://myblog.info
 * @bodyParam offer_categories.* int Ids of offer categories. Example: 1,2
 * @bodyParam aboutme string My Description (10-1000). Example: Hello! I'm very funny and pretty man
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->route('user')->isGirl())
            return $this->girlRules();
        return $this->managerRules();
    }

    public function girlRules(): array
    {
        $rules = [
            'name' => ['required', 'min:2', 'max:15'],
            'country' => 'required|exists:countries,id',
            'city' => [
                'required',
                Rule::exists('cities', 'id')
                    ->where('country_id', $this->get('country'))
            ],
            'age' => 'nullable|integer|min:18|max:60',
            /*'boobs_size' => 'nullable|integer|min:1|max:10',
            'boobs_type' => [
                'nullable',
                Rule::in(['natural', 'silicon'])
            ],*/
            'weight' => 'required|integer|min:30|max:240',
            'height' => 'required|integer|min:130|max:240',
            //'bust' => 'required|integer|min:40|max:200',
            //'waist' => 'nullable|integer|min:40|max:200',
            //'hip' => 'nullable|integer|min:40|max:200',
            'has_foreign_passport' => 'nullable|bool',
            'eye' => 'nullable|exists:eyes,id',
            'hair' => 'nullable|exists:hairs,id',
            'nationality' => 'nullable|exists:nationalities,id',
            'minimum_cost' => 'nullable|integer|min:0|max:100000',
            'whatsapp' => 'nullable|min:9|max:16',
            'visas.*' => 'nullable|exists:visas,id',
            'gender' => ['nullable', Rule::in(['male', 'female'])],
            'website' => 'nullable|url',
            'offer_categories.*' => 'nullable|exists:offer_categories,id',
            'aboutme' => 'nullable|min:10|max:1000'
        ];

        if ($this->user()->isAdmin()) {
            $rules['model_statuses.*'] = 'nullable|exists:model_statuses,id';
            $rules['statuses.*'] = 'nullable|exists:model_statuses,id';
            $rules['role'] = ['nullable', Rule::in(User::rolesList())];
            $rules['can_create_offers'] = 'nullable|bool';
            $rules['can_create_tours'] = 'nullable|bool';
        }

        return $rules;
    }

    public function managerRules(): array
    {
        $rules = [
            'name' => ['required', 'min:2', 'max:15'],
            'whatsapp' => 'nullable|max:16',
            'website' => 'nullable|url',
        ];

        if ($this->user()->isAdmin()) {
            $rules['statuses.*'] = 'nullable|exists:model_statuses,id';
            $rules['role'] = ['nullable', Rule::in(User::rolesList())];
            $rules['can_create_offers'] = 'nullable|bool';
            $rules['can_create_tours'] = 'nullable|bool';
        }

        return $rules;
    }

    public function getDto(): UserUpdateDto
    {
        $editUserIsAdmin = $this->user()->isAdmin();

        // if girl
        if($this->route('user')->isGirl())
            return new UserUpdateDto([
                'name' => $this->get('name'),
                'country' => Country::find($this->get('country')),
                'city' => City::find($this->get('city')),
                'age' => $this->get('age'),
                'boobs_size' => $this->get('boobs_size'),
                'boobs_type' => $this->get('boobs_type'),
                'weight' => $this->get('weight'),
                'height' => $this->get('height'),
                'bust' => $this->get('bust'),
                'waist' => $this->get('waist'),
                'hip' => $this->get('hip'),
                'has_foreign_passport' => (bool) $this->get('has_foreign_passport', false),
                'eye' => $this->get('eye') ? Eye::find($this->get('eye')) : null,
                'hair' => $this->get('hair') ? Hair::find($this->get('hair')) : null,
                'nationality' => $this->get('nationality') ? Nationality::find($this->get('nationality')) : null,
                'minimum_cost' => $this->get('minimum_cost'),
                'whatsapp' => $this->get('whatsapp'),
                'model_statuses' => $this->user()->isAdmin() ? $this->get('model_statuses') : null,
                'statuses' => $this->user()->isAdmin() ? $this->get('statuses') : [],
                'visas' => $this->get('visas', []),
                'offer_categories' => $this->get('offer_categories', []),
                'role' => $this->user()->isAdmin() ? $this->get('role') : null,
                'can_create_tours' => $editUserIsAdmin ? (bool) $this->get('can_create_tours', false) : null,
                'can_create_offers' => $editUserIsAdmin ? (bool) $this->get('can_create_offers', false) : null,
                'website' => $this->get('website'),
                'gender' => $this->get('gender'),
                'aboutme' => $this->get('aboutme')
            ]);

        // if manager
        return new UserUpdateDto([
            'name' => $this->get('name'),
            'whatsapp' => $this->get('whatsapp'),
            'statuses' => $this->user()->isAdmin() ? $this->get('statuses') : null,
            'role' => $this->user()->isAdmin() ? $this->get('role') : null,
            'can_create_tours' => $editUserIsAdmin ? (bool) $this->get('can_create_tours', false) : null,
            'can_create_offers' => $editUserIsAdmin ? (bool) $this->get('can_create_offers', false) : null,
            'website' => $this->get('website'),
        ]);
    }
}
