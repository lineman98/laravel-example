<?php

namespace App\Http\Requests\Users;

use App\Entity\Users\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CheckMediaRequest
 * @package App\Http\Requests\Users
 *
 * @bodyParam id int required Id of media. Example: 1
 */
class CheckMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                Rule::exists('media', 'id')
                    ->where('model_type', 'users')
                    ->where('model_id', $this->route('id'))
            ]
        ];
    }
}
