<?php

namespace App\Http\Requests\Users;

use App\Entity\Users\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class SetUnverifiedRequest
 * @package App\Http\Requests\Users
 *
 * @bodyParam reason string Example: Dont shown face
 */
class SetUnverifiedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason' => [
                'required',
                'max:255'
            ]
        ];
    }
}
