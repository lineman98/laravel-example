<?php

namespace App\Http\Requests\Auth;

use App\DTO\Users\UserCreateDto;
use App\Entity\Invites\Invite;
use App\Entity\Users\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Auth
 *
 * @bodyParam name string required User name. Example: Alexander
 * @bodyParam phone string required User phone. Example: +793243243243
 * @bodtParam password string required User Password (6-16 symbols). Example: FfefeFB12
 * @bodyParam invite string Invite code. Example: GoodEnv
 * @bodyParam birthday string required Birthday date. Example: 20-12-2000
 * @bodyParam gender string required male \ female. Example: male
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:15',
            'phone' => 'required|min:9|max:16|unique:users',
            'password' => 'required|min:6|max:16',
            'invite' => [
				'nullable',
                Rule::exists('invites', 'code')
                    ->where('active', 1)
            ],
            'birthday' => [
                'required',
                'string',
                'min:10',
                'date',
                'before:' . today()->subYears(18)->format('Y-m-d')
            ],
            'gender' => [
                'required',
                Rule::in('male', 'female')
            ]
        ];
    }


    public function messages()
    {
        return [
            'phone.unique' => 'Телефон уже зарегистрирован',
            'invite.exists' => 'Некорректный пригласительный код',
            'birthday.before' => 'Вам должно быть не менее 18 лет'
        ];
    }

    /**
     * Возвращает исправленный номер телефона
     * @return string
     */
    public function getPhone(): string
    {
        $phone = $this->get('phone');
        if (strpos($phone, '+89') === 0)
            return str_replace('+89', '+79', $phone);
        return $phone;
    }

    public function getDto(): UserCreateDto
    {
        return new UserCreateDto([
            'name' => $this->get('name'),
            'phone' => $this->getPhone(),
            'password' => $this->get('password'),
            'role' => User::ROLE_USER,
            'status' => User::STATUS_NOT_ACTIVE,
            'invite' => $this->get('invite') ? Invite::where('code', $this->get('invite'))->first() : null,
            'birthday' => $this->get('birthday'),
            'gender' => $this->get('gender')
        ]);
    }

}
