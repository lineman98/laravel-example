<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RestorePasswordCodeRequest
 * @package App\Http\Requests\Auth
 *
 * @bodyParam phone string required User phone. Example: 794324324343
 * @bodyParam code string required Verification code (4 symbols). Example: fjwo
 * @bodyParam new_password string required New user password. Example: efyEehfh3f
 */
class RestorePasswordCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|exists:users,phone',
            'code' => 'required|min:4|max:4',
            'new_password' => ['required', 'min:6', 'max:16'],
        ];
    }
}
