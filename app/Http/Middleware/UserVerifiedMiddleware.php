<?php

namespace App\Http\Middleware;

use Closure;

class UserVerifiedMiddleware
{
    /**
     * Check user block
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
        if ($request->user()->isVerified())
            return $next($request);

        return response()->json([
            'success' => false,
            'error' => 'Пройдите проверку чтобы получить доступ ко всем возможностям приложения'
        ], 403);
        */
        return $next($request);
    }
}
