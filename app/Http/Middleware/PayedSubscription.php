<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;

class PayedSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->subscription_payed)
            return $next($request);

        return response()->json([
            'success' => false,
            'error' => 'Subscription is not active'
        ], 403);
    }
}
