<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Log\Logger;

class LogMiddleware
{

    private $logger;

    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * Check that phone is already confirmed
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $this->logger->info('Dump request', [
            'ip' => $request->getClientIp(),
            'url' => $request->getRequestUri(),
            'request' => serialize($request->all()),
            'response' => $response
        ]);
    }

}
