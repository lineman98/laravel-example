<?php

namespace App\Http\Middleware;

use Closure;

class PhoneVerifiedMiddleware
{
    /**
     * HCheck that phone is confirmed
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->isPhoneVerified())
            return $next($request);

        return response()->json([
            'success' => false,
            'error' => 'Телефон не подтвержден'
        ], 403);
    }
}
