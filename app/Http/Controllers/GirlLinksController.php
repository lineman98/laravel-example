<?php

namespace App\Http\Controllers;

use App\Services\Users\GirlLinksService;

class GirlLinksController extends Controller
{

    /** @var GirlLinksService */
    private $linksService;

    public function __construct(GirlLinksService $linksService)
    {
        $this->linksService = $linksService;
    }

    public function __invoke(string $alias)
    {
        try {
            $girl = $this->linksService->getGirlByLinkAlias($alias);
        } catch (\Exception $exception) {
            return view('public.model.not_active');
        }
        return view('public.model.index', compact('girl'));
    }

}
