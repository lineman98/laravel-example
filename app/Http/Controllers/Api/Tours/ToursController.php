<?php

namespace App\Http\Controllers\Api\Tours;

use App\Entity\Tours\Tour;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tours\CreateRequest;
use App\Http\Requests\Tours\UpdateRequest;
use App\Http\Resources\Bills\BillResource;
use App\Http\Resources\Tours\TourResource;
use App\Services\Bills\BillService;
use App\Services\Tours\ToursService;
use Illuminate\Http\Request;

/**
 * @group Tours
 * Class ToursController
 * @package App\Http\Controllers\Api\Tours
 */
class ToursController extends Controller
{

    private $toursService, $billService;

    public function __construct(ToursService $toursService, BillService $billService)
    {
        $this->toursService = $toursService;
        $this->billService = $billService;
    }

    /**
     * Туры для модели
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $tours = $this->toursService->paginateToursForModel();
        return TourResource::collection($tours);
    }

    /**
     * Все туры. Для админа
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function indexAll(Request $request)
    {
        $this->middleware('admin');
        $tours = $this->toursService->paginate(
            $request->get('count', 20),
            $request->all()
        );
        return TourResource::collection($tours);
    }

    /**
     * Созданные туры
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function my(Request $request)
    {
        $tours = $this->toursService->paginateOwnOffers($request->user());
        return TourResource::collection($tours);
    }

    /**
     * @param CreateRequest $request
     * @return TourResource
     * @throws \Throwable
     */
    public function store(CreateRequest $request)
    {
        $tour = $this->toursService->create($request->getDto());
        return new TourResource($tour);
    }

    /**
     * @param Request $request
     * @param $id
     * @return TourResource
     */
    public function show(Request $request, $id)
    {
        $offer = $this->toursService->fetchTourForManager($request->user(), $id);
        return new TourResource($offer);
    }

    /**
     * Получить счет на оплату публикации
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getBill(Request $request, $id)
    {
        $user = $request->user();
        $tour = $this->toursService->fetchTourForManager($user, $id);
        $bill = $this->billService->createTourPublicationBill($user, $tour);
        $payment_url = $this->billService->generateUrlForPay($bill);
        return response()->json([
            'bill' => new BillResource($bill),
            'payment_url' => $payment_url
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return TourResource
     */
    public function showForModel(Request $request, $id)
    {
        $tour = $this->toursService->findActive($id);
        return new TourResource($tour);
    }

    /**
     * @param UpdateRequest $request
     * @param Tour $tour
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function update(UpdateRequest $request, Tour $tour)
    {
        $this->authorize('edit-tour', $tour);
        $result = $this->toursService->update($tour, $request->getDto());
        return response()->json(compact('result'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function activate($id)
    {
        $tour = $this->toursService->find($id);
        $result = $this->toursService->setActive($tour);
        return response()->json(compact('result'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function block($id)
    {
        $tour = $this->toursService->find($id);
        $result = $this->toursService->setBlocked($tour);
        return response()->json(compact('result'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function close($id)
    {
        $tour = $this->toursService->find($id);
        $this->authorize('edit-tour', $tour);
        $result = $this->toursService->close($tour);
        return response()->json(compact('result'));
    }

    /**
     * @param Tour $tour
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Tour $tour)
    {
        $this->authorize('edit-tour', $tour);
        $result = $this->toursService->delete($tour);
        return response()->json(compact('result'));
    }

}
