<?php

namespace App\Http\Controllers\Api\Claims;

use App\Http\Requests\Claims\CreateRequest;
use App\Http\Resources\Claims\ClaimResource;
use App\Services\Claims\ClaimsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClaimsController extends Controller
{

    private $claimsService;

    public function __construct(ClaimsService $claimsService)
    {
        $this->claimsService = $claimsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param CreateRequest $request
     * @return ClaimResource
     * @throws \Throwable
     */
    public function store(CreateRequest $request)
    {
        $claim = $this->claimsService->create($request->getDto());
        return new ClaimResource($claim);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
