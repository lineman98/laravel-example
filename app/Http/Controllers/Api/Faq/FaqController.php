<?php

namespace App\Http\Controllers\Api\Faq;

use App\Entity\Faq\Faq;
use App\Http\Controllers\Controller;
use App\Http\Resources\Faq\FaqResource;
use Illuminate\Support\Facades\Cache;

/**
 * Class FaqController
 * @package App\Http\Controllers\Api\Faq
 * @group Help
 */
class FaqController extends Controller
{
    /**
     * Get Faq Questions & Answers
     *
     * @apiResourceCollection App\Http\Resources\Faq\FaqResource
     * @apiResourceModel App\Entity\Faq\Faq
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $faqs = Cache::tags(Faq::class)->rememberForever('all_faq', function () {
            return Faq::with('translations')->get();
        });
        return FaqResource::collection($faqs);
    }
}
