<?php

namespace App\Http\Controllers\Api\Geo;

use App\Http\Requests\Countries\CreateRequest;
use App\Http\Requests\Countries\UpdateRequest;
use App\Http\Resources\Geo\CountryResource;
use App\Services\Geo\CountryService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @group Geo
 * Class CountriesController
 * @package App\Http\Controllers\Api\Geo
 */
class CountriesController extends Controller
{

    protected $service;

    public function __construct(CountryService $service)
    {
        $this->service = $service;
    }


    /**
     * Get countries
     *
     * @apiResourceCollection \App\Http\Resources\Geo\CountryResource
     * @apiResourceModel \App\Entity\Geo\Country\Country
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CountryResource::collection($this->service->getAllCountriesWithCities());
    }

    /**
     * Add country
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @apiResource \App\Http\Resources\Geo\CountryResource
     * @apiResourceModel \App\Entity\Geo\Country\Country
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        return new CountryResource($this->service->createFromRequest($request));
    }

    /**
     * Show country
     *
     * @urlParam id required Id of country
     *
     * @apiResource \App\Http\Resources\Geo\CountryResource
     * @apiResourceModel \App\Entity\Geo\Country\Country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new CountryResource($this->service->find($id));
    }

    /**
     * Update country
     *
     * @param  \Illuminate\Http\Request $request
     * @urlParam id required Id of country
     *
     * @response {
     *   "status": "true"
     * }
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json([
            'status' => $this->service->updateFromRequest($request, $id)
        ]);
    }

    /**
     * Remove country (dont works)
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
