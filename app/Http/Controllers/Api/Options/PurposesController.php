<?php

namespace App\Http\Controllers\Api\Options;

use App\Entity\Options\Purpose\Purpose;
use App\Http\Resources\Options\PurposeResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

/**
 * @group Options
 * Class PurposesController
 * @package App\Http\Controllers\Api\Options
 */
class PurposesController extends Controller
{
    /**
     * Get purposes
     *
     * @apiResourceCollection App\Http\Resources\Options\PurposeResource
     * @apiResourceModel App\Entity\Options\Purpose\Purpose
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purposes = Cache::tags(Purpose::class)->rememberForever('all_purposes', function () {
            return Purpose::with('translations')->get();
        });
        return PurposeResource::collection($purposes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
