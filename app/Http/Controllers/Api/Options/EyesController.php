<?php

namespace App\Http\Controllers\Api\Options;

use App\Entity\Options\Eye\Eye;
use App\Http\Resources\Options\EyeResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

/**
 * @group Options
 * Class EyesController
 * @package App\Http\Controllers\Api\Options
 */
class EyesController extends Controller
{
    /**
     * Get eyes
     *
     * @apiResourceCollection App\Http\Resources\Options\EyeResource
     * @apiResourceModel App\Entity\Options\Eye\Eye
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eyes = Cache::tags(Eye::class)->rememberForever('all_eyes', function () {
            return Eye::with('translations')->get();
        });
        return EyeResource::collection($eyes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
