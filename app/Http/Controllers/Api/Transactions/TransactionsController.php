<?php

namespace App\Http\Controllers\Api\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Bills\TransactionRequest;
use App\Services\Bills\BillService;
use App\Services\Bills\TransactionService;
use Illuminate\Http\Request;

/**
 * @group Transactions
 * Class TransactionsController
 * @package App\Http\Controllers\Api\Transactions
 */
class TransactionsController extends Controller
{

    /** @var TransactionService */
    private $transactionService;
    /** @var BillService */
    private $billService;

    public function __construct(TransactionService $transactionService, BillService $billService)
    {
        $this->transactionService = $transactionService;
        $this->billService = $billService;
    }

    /**
     * @param TransactionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(TransactionRequest $request)
    {
        $transaction = $this->transactionService->create($request->getDto());
        return response()->json([
            'paid' => $transaction->isPaid()
        ]);
    }

    /**
     * Оплата счета с реферального баланса
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function payBillFromBalance($id)
    {
        $bill = $this->billService->findActiveBill($id);
        if (!$bill) {
            return response()->json([
                'success' => false,
                'error' => 'Not found bill'
            ]);
        }
        try {
            $this->billService->payFromReferralBalance($bill);
            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }

}
