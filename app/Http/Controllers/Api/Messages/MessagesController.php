<?php

namespace App\Http\Controllers\Api\Messages;

use App\Entity\Messages\Message;
use App\Entity\Messages\Dialog;
use App\Http\Requests\Messages\CreateRequest;
use App\Http\Requests\Messages\CreateByDialogRequest;
use App\Http\Requests\Messages\ListDialogRequest;
use App\Http\Requests\Messages\MarkAsReadRequest;
use App\Http\Requests\Messages\MarkDialogAsReadRequest;
use App\Http\Resources\Messages\DialogResource;
use App\Http\Resources\Messages\MessageResource;
use App\Services\Messages\DialogService;
use App\Services\Messages\MessageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @group Messages
 * Class MessagesController
 * @package App\Http\Controllers\Api\Messages
 */
class MessagesController extends Controller
{

    private $dialogService, $messageService;

    public function __construct(DialogService $dialogService, MessageService $messageService)
    {
        $this->dialogService = $dialogService;
        $this->messageService = $messageService;
    }

    /**
     * Get user dialogs
     *
     * @param Request $request
     *
     * @apiResourceCollection App\Http\Resources\Messages\DialogResource
     * @apiResourceModel App\Entity\Messages\Dialog
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $dialogs = $this->dialogService->getUserDialogs($request->user());
        return DialogResource::collection($dialogs);
    }

    /**
     * Send message to User
     *
     * @param CreateRequest $request
     *
     * @apiResource App\Http\Resources\Messages\MessageResource
     * @apiResourceModel App\Entity\Messages\Message
     * @return MessageResource
     * @throws \App\Exceptions\BlackListException
     * @throws \Throwable
     */
    public function create(CreateRequest $request)
    {
        $message = $this->messageService->createMessage($request->getDto());
        return new MessageResource($message);
    }

    /**
     * Send message to Dialog
     *
     * @apiResource App\Http\Resources\Messages\MessageResource
     * @apiResourceModel App\Entity\Messages\Message
     * @param CreateByDialogRequest $request
     * @return MessageResource
     * @throws \Exception
     */
    public function createByDialog(CreateByDialogRequest $request)
    {
        $message = $this->messageService->createDialogMessage($request->getDto());
        return new MessageResource($message);
    }

    /**
     * Show dialog messages

     * @apiResourceCollection App\Http\Resources\Messages\MessageResource
     * @apiResourceModel App\Entity\Messages\Message
     * @param ListDialogRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function listDialog(ListDialogRequest $request)
    {
        $messages = $this->messageService->listDialogMessages($request->get('dialog_id'));
        return MessageResource::collection($messages);
    }

    /**
     * Mark message as read
     *
     * @response {
     *   "status": "true"
     * }
     * @param MarkAsReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsRead(MarkAsReadRequest $request)
    {
        $status = $this->messageService->markMessageAsRead(
            Message::find($request->get('id')),
            $request->user()
        );
        return response()->json(compact('status'));
    }

    /**
     * Mark dialog as read
     *
     * @response {
     *   "status": "true"
     * }
     * @param MarkDialogAsReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markDialogAsRead(MarkDialogAsReadRequest $request)
    {
        $status = $this->dialogService->markDialogAsRead(
            Dialog::find($request->get('id')),
            $request->user()
        );
        return response()->json(compact('status'));
    }

    /**
     * Get unread dialogs
     *
     * @response {
     *   "unread_count": 10
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unreadCount(Request $request)
    {
        $unread_count = $this->messageService->getUnreadCount($request->user());
        return response()->json(compact('unread_count'));
    }

}
