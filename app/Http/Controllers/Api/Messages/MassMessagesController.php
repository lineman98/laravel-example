<?php

namespace App\Http\Controllers\Api\Messages;

use App\Entity\MassMessages\MassMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassMessages\CreateRequest;
use App\Http\Resources\MassMessages\MassMessageResource;
use App\Services\MassMessages\MassMessagesService;
use Illuminate\Http\Request;

class MassMessagesController extends Controller
{

    /** @var MassMessagesService */
    private $massMessagesService;

    public function __construct(MassMessagesService $massMessagesService)
    {
        $this->massMessagesService = $massMessagesService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $massMessagess = $this->massMessagesService->paginate(
            $request->get('count', 20)
        );
        return MassMessageResource::collection($massMessagess);
    }

    /**
     * @param CreateRequest $request
     * @return MassMessageResource
     * @throws \Throwable
     */
    public function store(CreateRequest $request)
    {
        $massMessage = $this->massMessagesService->create($request->getDto());
        return new MassMessageResource($massMessage);
    }

    /**
     * @param MassMessage $massMessage
     * @return MassMessageResource
     */
    public function show(MassMessage $massMessage)
    {
        return new MassMessageResource($massMessage);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function start($id)
    {
        $massMessage = $this->massMessagesService->find($id);
        $result = $this->massMessagesService->startSending($massMessage);
        return response()->json(compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param MassMessage $massMessage
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(MassMessage $massMessage)
    {
        $result = $this->massMessagesService->destroy($massMessage);
        return response()->json(compact('result'));
    }
}
