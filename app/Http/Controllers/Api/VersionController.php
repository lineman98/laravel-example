<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VersionController extends Controller
{

    public function __invoke(Request $request)
    {
        $os = $request->get('os', 'ios');
        $version = $request->get('version');

        $current_version_ios = '4.3.6';
        $current_version_android = '2.7';

        if ($os === 'ios') {
            return response()->json([
                'version' => $current_version_ios
            ]);
        }

        return response()->json([
            'version' => $current_version_android
        ]);
    }

}
