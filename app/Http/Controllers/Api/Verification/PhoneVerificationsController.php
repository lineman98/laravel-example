<?php

namespace App\Http\Controllers\Api\Verification;

use App\Http\Requests\Verification\SendVerifyByFirebaseRequest;
use App\Http\Requests\Verification\SendVerifyRequest;
use App\Services\Verification\Exceptions\AlreadyVerifiedException;
use App\Services\Verification\Exceptions\ExpiredException;
use App\Services\Verification\Exceptions\LimitException;
use App\Services\Verification\Exceptions\VerificationException;
use App\Services\Verification\PhoneVerification;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @group Verification
 * Class PhoneVerificationsController
 * @package App\Http\Controllers\Api\Verification
 */
class PhoneVerificationsController extends Controller
{

    /**
     * @var PhoneVerification
     */
    private $phoneVerification;

    public function __construct(PhoneVerification $phoneVerification)
    {
        $this->phoneVerification = $phoneVerification;
    }

    /**
     * Send verify sms code to users phone
     *
     * response {
     *   "success": "true"
     * }
     * response {
     *   "success": "true",
     *   "already_verified": "true"
     * }
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        try {
            $result = $this->phoneVerification->sendSms($request->user());
            return response()->json(['success' => $result]);
        } catch (AlreadyVerifiedException $verificationException) {
            return response()->json([
                'success' => true,
                'already_verified' => true
            ]);
        }
    }

    /**
     * Check verification code
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @response {
     *   "success": "false",
     *   "error": "Error message"
     * }
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function verify(SendVerifyRequest $request)
    {
        try {
            $result = $this->phoneVerification->verify(
                $request->user(),
                $request->get('code')
            );
            return response()->json(['success' => $result]);
        } catch (VerificationException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        } catch (ExpiredException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        } catch (LimitException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Verify by Firebase
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @response {
     *   "success": "false",
     *   "error": "Error message"
     * }
     *
     * @param SendVerifyByFirebaseRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function verifyByFirebase(SendVerifyByFirebaseRequest $request)
    {
        try {
            $result = $this->phoneVerification->verifyByFirebaseIdToken(
                $request->user(),
                $request->get('token')
            );
            return response()->json(['success' => $result]);
        } catch (VerificationException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Send call code to users phone
     *
     * response {
     *   "success": "true"
     * }
     * response {
     *   "success": "true",
     *   "already_verified": "true"
     * }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendCall(Request $request)
    {
        try {
            $result = $this->phoneVerification->sendCall($request->user());
            return response()->json(['success' => $result]);
        } catch (AlreadyVerifiedException $verificationException) {
            return response()->json([
                'success' => true,
                'already_verified' => true
            ]);
        }
    }

    /**
     * Verify Call code
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @response {
     *   "success": "false",
     *   "error": "Error message"
     * }
     *
     * @param SendVerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function verifyCall(SendVerifyRequest $request)
    {
        try {
            $result = $this->phoneVerification->verifyCall(
                $request->user(),
                $request->get('code')
            );
            return response()->json(['success' => $result]);
        } catch (VerificationException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        } catch (ExpiredException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        } catch (LimitException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }

}
