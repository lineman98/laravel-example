<?php

namespace App\Http\Controllers\Api\Auth;

use App\Entity\Users\User;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\Users\UserResource;
use App\Services\Users\RegisterService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class AuthController
 * @package App\Http\Controllers\Api\Auth
 * @group Auth
 */
class AuthController extends Controller
{

    /**
     * @var RegisterService
     */
    protected $registerService;

    /**
     * AuthController constructor.
     * @param RegisterService $registerService
     */
    public function __construct(RegisterService $registerService)
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
        $this->registerService = $registerService;
    }

    /**
     * Login
     *
     * @bodyParam phone string required User phone. Example: +789343242424
     * @bodyParam password string required User password. Example: Mefe3fHh8efh
     *
     * @apiResource \App\Http\Resources\Users\UserResource
     * @apiResourceModel \App\Entity\Users\User
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['phone', 'password']);

        if (! Auth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = User::where('phone', request('phone'))->first();

        // генерируем токен
        $token = $user->createToken('Application')->accessToken;
        return new UserResource($user, $token);
    }

    /**
     * Registration
     *
     * @param RegisterRequest $request
     * @apiResource \App\Http\Resources\Users\UserResource
     * @apiResourceModel \App\Entity\Users\User
     * @return UserResource
     * @throws \App\Exceptions\ValidationException
     * @throws \Throwable
     */
    public function register(RegisterRequest $request)
    {
        $user = $this->registerService->register($request->getDto());
        $token = $user->createToken('Application')->accessToken;
        return new UserResource($user, $token);
    }

    /**
     * Get the authenticated User.
     * @apiResource \App\Http\Resources\Users\UserResource
     * @apiResourceModel \App\Entity\Users\User
     * @return UserResource
     *
     */
    public function me()
    {
        return new UserResource(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @response 204 {
     *
     * }
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $accessToken = auth()->user()->token();
        $accessToken->revoke();
        $accessToken->delete();
        return response()->json(null, 204);
    }

}
