<?php

namespace App\Http\Controllers\Api\Users;

use App\Entity\Users\User;
use App\Entity\Users\UserBonusHistory;
use App\Http\Controllers\Controller;
use App\Http\Resources\Users\UserReferralResource;
use App\Services\Users\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

/**
 * @group Referrals
 * Class ReferralsController
 * @package App\Http\Controllers\Api\Users
 */
class ReferralsController extends Controller
{

    private $usersService;

    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * Get referrals
     *
     * @queryParam id int Id of requested user (For admins)
     *
     * @apiResourceCollection App\Http\Resources\Users\UserReferralResource
     * @apiResourceModel App\Entity\Users\User
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $id = $request->get('id', $request->user()->id);
        $user = $this->usersService->find($id);
        $this->authorize('edit-user', $user);
        $users = User::with('avatar')
            ->where('referrer_id', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return UserReferralResource::collection($users);
    }

    /**
     * Get referrals stats
     *
     * @queryParam id int Id of requested user (For admins)
     *
     * @response {
     *   "day_referrals_count": 10,
     *   "all_referrals_count": 11,
     *   "bonus_months_count": 2
     * }
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function stats(Request $request)
    {
        $user_id = $request->get('id', $request->user()->id);
        $user = $this->usersService->find($user_id);
        $this->authorize('edit-user', $user);

        $stats = Cache::remember('referral_stats_user_'.$user_id, 30, function () use ($user_id) {
            // количество приглашенных за 24ч
            $dayReferralsCount = User::where('referrer_id', $user_id)
                ->where('created_at', '>=', now()->subDay())
                ->where('created_at', '<=', now())
                ->count();

            // общее количество приглашенных
            $allReferralsCount = User::where('referrer_id', $user_id)
                ->count();

            // количество полученных бонусных месяцев
            $bonusMonthsCount = UserBonusHistory::where('user_id', $user_id)
                ->where('type', 'OrderPlan')
                ->count();

            return [
                'day_referrals_count' => $dayReferralsCount,
                'all_referrals_count' => $allReferralsCount,
                'bonus_months_count' => $bonusMonthsCount
            ];
        });

        return response()->json($stats);
    }

}
