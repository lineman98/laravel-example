<?php

namespace App\Http\Controllers\Api\Offers;

use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferUser;
use App\Services\Offers\OfferUserService;
use App\Services\Offers\OfferService;
use Illuminate\Http\Request;
use App\Http\Resources\Offers\OfferUserResource;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Requests\Offers\OfferUser\CreateRequest;
use App\Http\Requests\Offers\OfferUser\UpdateRequest;

/**
 * @group Offers
 * Class OfferUserController
 * @package App\Http\Controllers\Api\Offers
 */
class OfferUserController extends Controller
{
    /** @var OfferUserService */
    private $offerUserService;

    public function __construct(OfferUserService $offerUserService, OfferService $offerService)
    {
        $this->offerUserService = $offerUserService;
    }
    /**
     * Get offer applications
     *
     * @urlParam offer_id int Id of offer Example: 1
     *
     * @apiResourceCollection App\Http\Resources\Offers\OfferUserResource
     * @apiResourceModel App\Entity\Offers\OfferUser
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, int $offer_id)
    {
        $offers = $this->offerUserService->paginate($offer_id, 20);

        return OfferUserResource::collection($offers);
    }

    /**
     * Send application or ignore offer
     *
     * @urlParam offer_id int Id of offer Example: 1
     *
     * @apiResourceCollection App\Http\Resources\Offers\OfferUserResource
     * @apiResourceModel App\Entity\Offers\OfferUser
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request, int $offer_id)
    {
        $resource = $this->offerUserService->create(
            $request->user()->id,
            $offer_id,
            $request->get('message'),
            $request->get('status')
		);

        return new OfferUserResource($resource);
    }

    /**
     * Show application
     *
     * @urlParam offer_id int Id of offer Example: 1
     * @urlParam id int Id of application Example: 1
     *
     * @apiResourceCollection App\Http\Resources\Offers\OfferUserResource
     * @apiResourceModel App\Entity\Offers\OfferUser
     * @param  \App\OfferUser  $OfferUser
     * @return \Illuminate\Http\Response
     */
    public function show(OfferUser $OfferUser, int $offer_id, int $id)
    {
        $resource = $this->offerUserService->get($id);

        return new OfferUserResource($resource);
    }

    /**
     * Update application
     *
     * @urlParam offer_id int Id of offer Example: 1
     * @urlParam id int Id of application Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OfferUser  $OfferUser
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, int $offer_id, int $id)
    {
        $offer = $this->offerService->find($offer_id);
        $this->authorize('edit-offer', $offer);
        $result = $this->offerUserService->update($id, $request->getDto());

        return response()->json(compact('result'));
    }

    /**
     * Remove application (dont works)
     *
     * @param  \App\OfferUser  $OfferUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfferUser $OfferUser)
    {

    }

    /**
     * Accept offer application
     *
     * @urlParam offer int Id of offer Example: 1
     * @urlParam userId int Id applicated user Example: 20
     *
     * @response {
     *   "result": "true"
     * }
     *
     * @param Request $request
     * @param Offer $offer
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function agree(Request $request, Offer $offer, $userId) {
        $this->middleware('subscription_active');
        $this->authorize('edit-offer', $offer);

        $offerUserResource = $this->offerUserService->findByUserId($offer, $userId);
        $result = $this->offerUserService->agree($offerUserResource);
        return response()->json(compact('result'));
    }

    /**
     * Refuse offer application
     *
     * @urlParam offer int Id of offer Example: 1
     * @urlParam userId int Id applicated user Example: 20
     *
     * @response {
     *   "result": "true"
     * }
     * @param Request $request
     * @param Offer $offer
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function refuse(Request $request, Offer $offer, $userId) {
        $this->middleware('subscription_active');
        $this->authorize('edit-offer', $offer);

        $offerUserResource = $this->offerUserService->findByUserId($offer, $userId);
        $result = $this->offerUserService->refuse($offerUserResource);
        return response()->json(compact('result'));
    }
}
