<?php

namespace App\Http\Controllers\Api\Offers;

use App\Http\Requests\Offers\UsersCountRequest;
use App\Services\Offers\OfferLinksService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfferLinksController extends Controller
{

    /** @var OfferLinksService */
    protected $linksService;

    /**
     * OfferLinksController constructor.
     * @param OfferLinksService $linksService
     */
    public function __construct(OfferLinksService $linksService)
    {
        $this->linksService = $linksService;
    }

    /**
     * @param UsersCountRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function modelsCount(UsersCountRequest $request)
    {
        $query = $this->linksService->getSearchQueryModels($request->user(), $request->validated());
        $count = $query->count();
        return response()->json(compact('count'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function refuseFromModel(Request $request, $id)
    {
        $offerLink = $this->linksService->getLinkForModel($request->user(), $id);
        $result = $this->linksService->refuseFromModel($offerLink);
        return response()->json(compact('result'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function agreeFromModel(Request $request, $id)
    {
        $offerLink = $this->linksService->getLinkForModel($request->user(), $id);
        $result = $this->linksService->agreeFromModel($offerLink);
        return response()->json(compact('result'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function refuseFromManager(Request $request, $id)
    {
        $offerLink = $this->linksService->getLinkForManager($request->user(), $id);
        $result = $this->linksService->refuseFromManager($offerLink);
        return response()->json(compact('result'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function agreeFromManager(Request $request, $id)
    {
        $offerLink = $this->linksService->getLinkForManager($request->user(), $id);
        $result = $this->linksService->agreeFromManager($offerLink);
        return response()->json(compact('result'));
    }

}
