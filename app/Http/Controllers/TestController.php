<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity\Geo\City\City;

class TestController extends Controller
{
    public function index() {
        $files = scandir('./photos');

        foreach($files as $i => $filename) {
            //if($i <= 101) continue;
            if(preg_match('/.*\.jpg/', $filename)) {
                try {
                    $cityname = str_replace('.jpg', '', $filename);
                    $slug = \Str::slug($cityname);

                    //echo("$filename => $slug\r\n");
                    rename("./photos/$filename", "./photos/$slug.jpg");

                    $city = City::whereHas('translation', function($q) use ($cityname){
                        $q->where('name', $cityname);
                    })->update(['image_path' => "city/$slug.jpg", 'slug' => $slug]);

                }   catch (\Exception $e) {
                    print_r($e->getMessage());
                }

            }

        }
    }
}
