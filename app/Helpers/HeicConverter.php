<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class HeicConverter {

    /**
     * Конвертирование heic в jpeg
     * @param $path
     * @param int $quality
     * @return bool|string
     * @throws \Exception
     */
    public static function convertToJPEG($path) {
        if(!file_exists($path))
            return false;

        $path_parts = pathinfo($path);

        $output_filename = $path_parts['filename'] . '.jpg';

        $new_path = '/tmp/' . $output_filename;
        exec('/usr/local/bin/heif-convert '.$path.' '.$new_path);

        if(!file_exists($new_path)) {
            throw new \Exception('Error generate jpg file');
        }

        return $new_path;
    }

}
