<?php

namespace App\Events;

use App\Entity\Users\User;
use App\Http\Resources\Users\UserResource;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserSendVerifyPhoto implements ShouldBroadcast
{

    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = new UserResource($user);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('Admin'),
        ];
    }

    public function broadcastAs()
    {
        return 'UserSendVerifyPhoto';
    }

}
