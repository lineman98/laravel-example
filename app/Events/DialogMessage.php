<?php

namespace App\Events;

use App\Entity\Messages\Message;
use App\Entity\Messages\Dialog;
use App\Http\Resources\Messages\DialogResource;
use App\Http\Resources\Messages\MessageResource;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class DialogMessage implements ShouldBroadcastNow
{

    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message, $dialog;

    /**
     * PrivateMessage constructor.
     * @param Message $message
     * @param bool $first_message
     */
    public function __construct(Message $message, Dialog $dialog)
    {
        $this->message = new MessageResource($message);
        $this->dialog = new DialogResource($dialog);
        // если это первое сообщение в диалоге
        /*if($first_message) {
            $this->dialog = new DialogResource($message->dialog);
        }*/
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $channels = [
            new Channel('App.Dialog.'.$this->dialog->id)
        ];

        foreach($this->dialog->users as $user) {
            $channels[] = new PrivateChannel('App.User.'.$user->id);
        }

        $admin_user_id = config('support.admin_support_id');
        if ($this->message->user_to_id === $admin_user_id || $this->message->user_from_id === $admin_user_id ) {
            $channels[] = new PrivateChannel('Admin');
        }
        return $channels;
    }

    public function broadcastAs()
    {
        return 'DialogMessage';
    }

}
