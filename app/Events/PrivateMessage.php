<?php

namespace App\Events;

use App\Entity\Messages\Message;
use App\Http\Resources\Messages\DialogResource;
use App\Http\Resources\Messages\MessageResource;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PrivateMessage implements ShouldBroadcastNow
{

    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message, $dialog;

    /**
     * PrivateMessage constructor.
     * @param Message $message
     * @param bool $first_message
     */
    public function __construct(Message $message, $first_message=false)
    {
        $this->message = new MessageResource($message);
        // если это первое сообщение в диалоге
        if($first_message) {
            $this->dialog = new DialogResource($message->dialog);
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $channels = [
            new PrivateChannel('App.User.'.$this->message->user_to_id),
            new PrivateChannel('App.User.'.$this->message->user_from_id),
        ];
        $admin_user_id = config('support.admin_support_id');
        if ($this->message->user_to_id === $admin_user_id || $this->message->user_from_id === $admin_user_id ) {
            $channels[] = new PrivateChannel('Admin');
        }
        return $channels;
    }

    public function broadcastAs()
    {
        return 'PrivateMessage';
    }

}
