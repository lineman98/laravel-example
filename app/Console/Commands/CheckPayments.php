<?php

namespace App\Console\Commands;

use App\Entity\Wallet\Wallet;
use App\Services\Bills\BillService;
use Illuminate\Console\Command;
use YandexMoney\API;

class CheckPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payments:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check yandex payments';

    /** @var BillService */
    private $billService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->billService = app(BillService::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $wallets = Wallet::get();
        $wallets->each(function (Wallet $wallet) {
            $this->processWallet($wallet);
        });
    }

    public function processWallet(Wallet $wallet)
    {
        $api = new API($wallet->token);

        $operations_data = $api->operationHistory(array(
            "records" => 10,
            "type" => "deposition",
        ));

        $operations = $operations_data->operations;

        foreach ($operations as $operation) {
            if(isset($operation->label)) {
                $this->processPayment($operation->label);
            }
        }
    }

    public function processPayment(string $label): void
    {
        $bill_exploded = explode('bill_', $label);
        if (count($bill_exploded) !== 2)
            return;

        $bill_id = $bill_exploded[1];

        $bill = $this->billService->findActiveBill($bill_id);
        if(!$bill)
            return;

        // выставляем оплату
        if (!$bill->isPaid())
            $this->billService->setPaid($bill);
    }
}
