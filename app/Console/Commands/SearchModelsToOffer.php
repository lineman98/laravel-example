<?php

namespace App\Console\Commands;

use App\Entity\Offers\Offer;
use Illuminate\Console\Command;

class SearchModelsToOffer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search models to offers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $offers = Offer::active()->get();

        foreach ($offers as $offer) {
            \App\Jobs\SearchModelsToOffer::dispatch($offer)
                ->onQueue('models_offer_search');
        }

    }
}
