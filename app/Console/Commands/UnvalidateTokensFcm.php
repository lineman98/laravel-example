<?php

namespace App\Console\Commands;

use App\Entity\Offers\Offer;
use App\Entity\Users\UserExpoToken;
use App\Services\Offers\OffersService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Kreait\Firebase\Messaging;

class UnvalidateTokensFcm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:unvalidate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close ended offers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle(Messaging $messaging)
    {
        $all_count_not_working = 0;
        $unworking_tokens = [];

        UserExpoToken::query()
            ->chunkById(500, function (Collection $expoTokens) use (&$all_count_not_working, &$unworking_tokens, $messaging) {
                $deviceTokens = $expoTokens->pluck('token')->toArray();

                $message = Messaging\CloudMessage::new()
                    ->withData(['key' => 'value']);

                $sendReport = $messaging->sendMulticast($message, $deviceTokens);

                foreach ($sendReport->getItems() as $index => $report) {
                    if ($report->isFailure()) {
                        $all_count_not_working++;
                        echo $report->error()->getCode().PHP_EOL;
                        echo $deviceTokens[$index] . PHP_EOL;
                        UserExpoToken::where('token', $deviceTokens[$index])->delete();
                    }
                }
            });

        echo $all_count_not_working . PHP_EOL;
//        dd($unworking_tokens);
    }
}
