<?php

namespace App\Console\Commands;

use App\Entity\Offers\Offer;
use App\Services\Offers\OffersService;
use Illuminate\Console\Command;

class CloseEndedOffers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:close-ended';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close ended offers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offersService = new OffersService;

        $today = now()->setTime(0,0,0);

        // берем завершенные офферы
        $offers = Offer::where('meeting_date', '<', $today)
            ->where('status', Offer::STATUS_ACTIVE)
            ->get();

        // завершаем офферы
        foreach ($offers as $offer) {
            $offersService->setEnded($offer);
        }
    }
}
