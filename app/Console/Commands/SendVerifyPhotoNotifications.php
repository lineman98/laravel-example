<?php

namespace App\Console\Commands;

use App\Entity\Users\User;
use App\Notifications\VerifyPhotoNotification;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class SendVerifyPhotoNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:verify-upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка уведомлений о верификации моделям';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // выбираем моделей, которые не отправили фотку на верификацию, но заполнили анкету
        $girls = User::query()
            ->girl()
            ->active()
            ->where('verified', User::NOT_VERIFIED);

        $girls->chunk(100, function (Collection $girls) {
            $girls->each(function (User $girl) {
                $girl->notify(new VerifyPhotoNotification);
            });
        });
    }
}
