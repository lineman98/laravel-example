<?php

namespace App\Jobs;

use App\Entity\Offers\Offer;
use App\Entity\Users\User;
use App\Notifications\OfferCreateAdminNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;

class OfferCreatedSendAdminNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Offer */
    private $offer;

    /**
     * OfferCreatedSendAdminNotifications constructor.
     * @param Offer $offer
     */
    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $admins = Cache::remember('admins_users', 120, function () {
           return User::where('role', User::ROLE_ADMIN)->get();
        });
        Notification::send($admins, new OfferCreateAdminNotification($this->offer));
    }
}
