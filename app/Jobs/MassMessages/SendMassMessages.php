<?php

namespace App\Jobs\MassMessages;

use App\Entity\MassMessages\MassMessage;
use App\Services\MassMessages\MassMessagesService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMassMessages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var MassMessage */
    private $massMessage;

    /** @var MassMessagesService */
    private $massMessagesService;

    /**
     * Create a new job instance.
     * @param MassMessage $massMessage
     * @return void
     */
    public function __construct(MassMessage $massMessage)
    {
        $this->massMessage = $massMessage;
        $this->massMessagesService = app(MassMessagesService::class);
    }

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        // добавляем пользователей в рассылку
        $this->massMessagesService->addUsersToMassMessage($this->massMessage);
        // отправляем сообщения
        $this->massMessagesService->sendMessages($this->massMessage);
        // завершаем отправку
        $this->massMessagesService->finishSending($this->massMessage);
    }

}
