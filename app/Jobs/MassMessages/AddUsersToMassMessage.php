<?php

namespace App\Jobs\MassMessages;

use App\Entity\MassMessages\MassMessage;
use App\Services\MassMessages\MassMessagesService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddUsersToMassMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var MassMessage */
    private $massMessage;

    /** @var MassMessagesService */
    private $massMessagesService;

    /**
     * Create a new job instance.
     * @param MassMessage $massMessage
     * @return void
     */
    public function __construct(MassMessage $massMessage)
    {
        $this->massMessage = $massMessage;
        $this->massMessagesService = app(MassMessagesService::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // добавляем пользователей в рассылку
        $this->massMessagesService->addUsersToMassMessage($this->massMessage);
    }

}
