<?php

namespace App\Providers;

use App\Entity\Offers\Offer;
use App\Entity\Tours\Tour;
use App\Entity\Users\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('delete-user', function ($user, User $edit_user) {
            return $user->isAdmin() || $user->id == $edit_user->id;
        });

        Gate::define('edit-user', function ($user, User $edit_user) {
            return $user->isAdmin() || $user->id == $edit_user->id;
        });

        Gate::define('edit-offer', function ($user, Offer $offer) {
            return $user->isAdmin() || $user->id == $offer->user_id;
        });

        Gate::define('edit-tour', function ($user, Tour $tour) {
            return $user->isAdmin() || $user->id == $tour->user_id;
        });
    }
}
