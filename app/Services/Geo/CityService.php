<?php

namespace App\Services\Geo;

use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Http\Requests\Cities\CreateRequest;
use App\Http\Requests\Cities\UpdateRequest;
use Illuminate\Support\Facades\Cache;
use App\Filters\CitiesFilter;

class CityService
{

    protected $city;
    protected $countryService;

    public function __construct(City $city, CountryService $countryService)
    {
        $this->city = $city;
        $this->countryService = $countryService;
    }

    public function create($name, $slug, $countryId): City
    {
        $country = $this->countryService->find($countryId);
        $city = $this->city->make([
            'name' => $name,
            'slug' => $slug
        ]);
        $city->country()->associate($country);
        $city->saveOrFail();

        Cache::tags(City::class)->put('city_'.$city->id, $city, 30);
        return $city;
    }

    public function createFromRequest(CreateRequest $request): City
    {
        $city = $this->city->create($request->all());
        Cache::tags(City::class)->put('city_'.$city->id, $city, 30);
        return $city;
    }

    public function updateFromRequest(UpdateRequest $request, $id): bool
    {
        $city = $this->city->findOrFail($id);
        $status = $city->update($request->all());
        if($status) {
            Cache::tags(City::class)->put('city_' . $city->id, $city, 30);
        }
        return $status;
    }

    public function getTopByOffers(array $options) {
        return Cache::tags(City::class)->remember('cities_top', 60, function() use ($options) {
            return \App\Entity\Geo\City\City::withCount(['offers' => function($offers) {
                $offers->where('status', 'active');
            }])
                ->with('translations')
                ->orderBy('offers_count', 'desc')
                ->limit($options['count'])
                ->get();
        });
    }

    public function getCitiesForCountry($countryId)
    {
        return Cache::tags(City::class)->remember('country_cities_'.$countryId, 30, function () use ($countryId) {
            return City::with('translations')
                ->where('country_id', $countryId)
                ->get();
        });
    }

    public function searchCities($params) {
        \DB::enableQueryLog();
        $filter = new CitiesFilter($params);
        $cities = City::with('translations')
            ->filter($filter)
            ->limit(30)
            ->get();
        //print_r(\DB::getQueryLog());
        return $cities;
    }

	public function getCities()
    {
        return Cache::tags(City::class)->remember('cities', 30, function () {
            return City::with('translations')
                ->get();
        });
    }

    public function find($id)
    {
        return Cache::tags(City::class)->remember('city_'.$id, 30, function () use ($id) {
            return City::with('translations')->findOrFail($id);
        });
    }

}
