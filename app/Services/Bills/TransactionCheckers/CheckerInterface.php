<?php

namespace App\Services\Bills\TransactionCheckers;

use App\Entity\Bill\Transaction;

interface CheckerInterface
{

    public function isPaid(Transaction $transaction): bool;

}
