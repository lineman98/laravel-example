<?php

namespace App\Services\Bills\TransactionCheckers\Platforms;

use App\DTO\Transactions\TransactionDto;
use App\Entity\Bill\Transaction;
use App\Services\Bills\TransactionCheckers\CheckerInterface;
use GuzzleHttp\Client;

class IosChecker implements CheckerInterface
{

    public const SANDBOX_URL = 'https://sandbox.itunes.apple.com/verifyReceipt';
    public const PRODUCTION_URL = 'https://buy.itunes.apple.com/verifyReceipt';

    public function isPaid(Transaction $transaction): bool
    {
        $response = $this->request($transaction->receipt);

        if (!isset($response['status'])) {
            return false;
        }

        // если платеж с песочницы
        if ($response['status'] === 21007) {
            $response = $this->request($transaction->receipt, true);
        }

        // записываем ответ
        $transaction->response = $response;
        $transaction->save();

        return isset($response['status']) && $response['status'] == 0;
    }

    public function request(string $receipt, bool $isSandbox=false): array
    {
        $client = new Client;
        $response = $client->post($isSandbox ? self::SANDBOX_URL : self::PRODUCTION_URL, [
            'json' => ['receipt-data' => $receipt]
        ]);
        $content = $response->getBody()->getContents();
        return json_decode($content, true);
    }

}
