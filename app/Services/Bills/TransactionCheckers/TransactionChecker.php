<?php

namespace App\Services\Bills\TransactionCheckers;

use App\Entity\Bill\Transaction;
use App\Services\Bills\TransactionCheckers\Platforms\AndroidChecker;
use App\Services\Bills\TransactionCheckers\Platforms\IosChecker;

class TransactionChecker
{

    public function isPaid(Transaction $transaction): bool
    {
        $platform = $transaction->platform;
        $checker = $this->createChecker($platform);
        return $checker->isPaid($transaction);
    }

    public function createChecker(string $platform): CheckerInterface
    {
        if ($platform === 'ios')
            return new IosChecker;
        return new AndroidChecker;
    }

}
