<?php

namespace App\Services\Bills;

use App\DTO\Transactions\TransactionDto;
use App\Entity\Bill\Bill;
use App\Entity\Bill\Transaction;
use App\Services\Bills\TransactionCheckers\TransactionChecker;

class TransactionService
{

    private $billService;

    private $transactionChecker;

    public function __construct(BillService $billService, TransactionChecker $transactionChecker)
    {
        $this->billService = $billService;
        $this->transactionChecker = $transactionChecker;
    }

    /**
     * @param TransactionDto $dto
     * @return Transaction
     */
    public function create(TransactionDto $dto): Transaction
    {
        $bill = $dto->getBill();
        $transaction = new Transaction;
        $transaction->receipt = $dto->getReceipt();
        $transaction->platform = $dto->getPlatform();
        $transaction->bill()->associate($bill);
        $transaction->save();

        if ($this->transactionChecker->isPaid($transaction)) {
            $transaction->setPaid();
            $this->billService->setPaid($bill);
        }

        return $transaction;
    }

}
