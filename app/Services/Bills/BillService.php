<?php

namespace App\Services\Bills;

use App\Entity\Bill\Bill;
use App\Entity\Offers\Offer;
use App\Entity\Orders\OrderOfferPublication;
use App\Entity\Orders\OrderPlan;
use App\Entity\Orders\OrderTourPublication;
use App\Entity\Tours\Tour;
use App\Entity\Users\User;
use App\Entity\Users\UserBonusHistory;
use App\Notifications\SuccessPaymentBill;
use Illuminate\Support\Facades\DB;
use Rinvex\Subscriptions\Models\Plan;

class BillService
{

    private $walletService;

    public function __construct(WalletService $walletService)
    {
        $this->walletService = $walletService;
    }

    /**
     * @param $id
     * @return Bill
     */
    public function findBill($id): Bill
    {
        return Bill::findOrFail($id);
    }

    /**
     * @param string $id
     * @return Bill|null
     */
    public function findActiveBill(string $id): ?Bill
    {
        return Bill::active()->find($id);
    }

    /**
     * Оплатить счет
     * @param Bill $bill
     */
    public function setPaid(Bill $bill): void
    {
        if(!$bill->setPaid())
            return;

        // выдаем доступ к заказу
        if($bill->order)
            $bill->order->afterPay();

        // высылаем оповещение об успешной оплате счета
        if($bill->user)
            $bill->user->notify(new SuccessPaymentBill($bill));
    }

    /**
     * Создание счета на оплату тарифного плана
     * @param User $user
     * @param Plan $plan
     * @return Bill
     * @throws \Throwable
     */
    public function createPlanBill(User $user, Plan $plan): Bill
    {
        // если есть активный счет к этому тарифу, то возвращаем его
        $order = OrderPlan::where([
            'user_id' => $user->id,
            'plan_id' => $plan->id
        ])->whereHas('bill', function ($query) {
            $query->where('status', Bill::STATUS_ACTIVE);
        })->first();

        // если нет ордера, то создаем новый
        if(!$order) {
            $order = OrderPlan::create([
                'user_id' => $user->id,
                'plan_id' => $plan->id
            ]);
        }

        // если уже есть счет, то отдаем его на оплату
        if($order->bill)
            return $order->bill;

        $bill = new Bill;
        $bill->amount = $plan->price;
        $bill->currency = $plan->currency;
        $bill->order()->associate($order);
        $bill->user()->associate($user);
        $bill->status = Bill::STATUS_ACTIVE;
        $bill->saveOrFail();

        return $bill;
    }

    /**
     * Создание счета на оплату тарифного плана
     * @param User $user
     * @param Tour $tour
     * @return Bill
     * @throws \Throwable
     */
    public function createTourPublicationBill(User $user, Tour $tour): Bill
    {
        // если есть активный счет к этому туру, то возвращаем его
        $order = OrderTourPublication::firstOrCreate([
            'user_id' => $user->id,
            'tour_id' => $tour->id
        ]);

        // если уже есть счет, то отдаем его на оплату
        if($order->bill)
            return $order->bill;

        $bill = new Bill;
        $bill->amount = config('plans.tour_publication_cost');
        $bill->currency = 'RUB';
        $bill->order()->associate($order);
        $bill->user()->associate($user);
        $bill->status = Bill::STATUS_ACTIVE;
        $bill->saveOrFail();

        return $bill;
    }

    /**
     * Создание счета на оплату публикации предложения
     * @param User $user
     * @param Offer $offer
     * @return Bill
     * @throws \Throwable
     */
    public function createOfferPublicationBill(User $user, Offer $offer): Bill
    {
        // если есть активный счет к этому туру, то возвращаем его
        $order = OrderOfferPublication::firstOrCreate([
            'user_id' => $user->id,
            'offer_id' => $offer->id
        ]);

        // если уже есть счет, то отдаем его на оплату
        if($order->bill)
            return $order->bill;

        $bill = new Bill;
        $bill->amount = config('plans.offer_publication_cost');
        $bill->currency = 'RUB';
        $bill->order()->associate($order);
        $bill->user()->associate($user);
        $bill->status = Bill::STATUS_ACTIVE;
        $bill->saveOrFail();

        return $bill;
    }

    /**
     * Оплата счета с реферального баланса
     * @param Bill $bill
     * @return bool
     * @throws \Throwable
     */
    public function payFromReferralBalance(Bill $bill): bool
    {
        if (!$bill->canPayFromReferralBalance())
            throw new \Exception('Impossible to pay this bill from the referral balance');

        /** @var User $user */
        $user = $bill->user;

        $billReferralCost = $bill->getReferralBalanceCost();
        if ($user->referral_balance < $billReferralCost)
            throw new \Exception('Not enough balance');

        DB::beginTransaction();

        try {
            $newBalance = $user->referral_balance - $billReferralCost;

            // сохраняем бонус в историю
            UserBonusHistory::create([
                'user_id' => $user->id,
                'type' => $bill->getType(),
                'balance_before' => $user->referral_balance,
                'balance_after' => $newBalance
            ]);

            $user->referral_balance = $newBalance;
            $user->saveOrFail();
            $this->setPaid($bill);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
        return true;
    }

    public function generateUrlForPay(Bill $bill)
    {
        $url = 'https://money.yandex.ru/transfer?';
        $name = $bill->getName();

        $params = [
            'receiver' => $this->walletService->getActiveWallet(),
            'label' => 'bill_'.$bill->id,
            'targets' => $name,
            'paymentType' => 'AC',
            'sum' => $bill->amount,
            'successURL' => 'http://web.laravel-example/',
            'quickpay-back-url' => 'http://web.laravel-example/',
            'shop-host' => '',
            'comment' => '',
            'origin' => 'form',
            'selectedPaymentType' => 'AC',
            'destination' => $name,
            'form-comment' => $name,
            'short-dest' => '',
            'quickpay-form' => 'shop',
        ];

        $query = http_build_query($params);
        return $url . $query;
    }
}
