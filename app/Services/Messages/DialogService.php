<?php

namespace App\Services\Messages;

use App\Entity\Messages\Dialog;
use App\Entity\Messages\Message;
use App\Entity\Users\User;
use App\Entity\Messages\DialogUser;
use Illuminate\Support\Facades\Cache;

class DialogService
{

    /**
     * Create or find exists dialog between users
     * @param int $user_first
     * @param int $user_second
     * @return Dialog
     */
    public function createDialog(int $user_first, int $user_second): Dialog
    {
        $users = [ $user_first, $user_second ];

        $dialog = Dialog::where('type', 'direct')
                   ->withCount('users', function($q) use($users) {
                       $q->whereIn('id', $users);
                   })
                   ->having('users', '2')->firstOrCreate([
                       'type' => 'direct'
                   ])->sync('users', $users);

        /*Cache::rememberForever('dialog_'.$user_first_id.'_'.$user_second_id,
            function () use ($user_first_id, $user_second_id) {
               return
        });*/

        return $dialog;
    }

    public function addUser(User $user, int $dialog_id)
    {
        return DialogUser::firstOrCreate([
            'user_id' => $user->id,
            'dialog_id' => $dialog_id
        ], [
            'user_id' => $user->id,
            'dialog_id' => $dialog_id
        ]);
    }

    /**
     * Get last user dialogs
     * @param User $user
     * @return mixed
     */
    public function getUserDialogs(User $user)
    {
        return $user->dialogs()->with('lastMessage', 'users')
            ->whereHas('messages', function($q) use($user) {
                $q->where('user_from_id', $user->id);
            })
            ->orderBy('last_message_time', 'desc')
            ->limit(50)
            ->withPivot('readed')
            ->get();
    }


    public function markDialogAsRead(Dialog $dialog, User $user): bool
    {
        $status = $dialog->users()->updateExistingPivot($user->id, ['readed' => 1]);
        return $status;
    }

}
