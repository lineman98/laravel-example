<?php

namespace App\Services\Messages;

use App\DTO\Messages\MessageDto;
use App\DTO\Messages\DialogMessageDto;
use App\Entity\Messages\Dialog;
use App\Entity\Messages\DialogUser;
use App\Entity\Messages\Message;
use App\Entity\Users\User;
use App\Entity\Offers\OfferUser;
use App\Events\PrivateMessage;
use App\Events\DialogMessage;
use App\Exceptions\BlackListException;
use App\Notifications\NewMessageNotification;
use App\Services\BlackList\BlackListService;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\DB;

class MessageService
{

    /** @var DialogService */
    private $dialogService;
    /** @var BlackListService */
    private $blackListService;

    public function __construct(DialogService $dialogService, BlackListService $blackListService)
    {
        $this->dialogService = $dialogService;
        $this->blackListService = $blackListService;
    }

    /**
     * @param MessageDto $dto
     * @return Message
     * @throws BlackListException
     * @throws \Throwable
     */
    public function createMessage(MessageDto $dto): Message
    {
        // проверка, есть ли пользователь в чс
        $inBlackList = $this->blackListService->inBlackList(
            $dto->getUserTo(),
            $dto->getUserFrom()
        );

        if($inBlackList) {
            throw new BlackListException();
        }

        DB::beginTransaction();
        try {
            $dialog = $this->dialogService->createDialog(
                $dto->getUserFrom()->id,
                $dto->getUserTo()->id
            );

            $message = new Message;
            $message->content = $dto->getContent();
            $message->read = false;
            $message->userFrom()->associate($dto->getUserFrom());
            $message->dialog()->associate($dialog);
            $message->saveOrFail();

            // отправляем сообщение на сокет
            event(new PrivateMessage($message, $dialog->wasRecentlyCreated));
            // отправляем оповещения на другие каналы
            $dto->getUserTo()->notify(new NewMessageNotification($message));

            // обновляем дату последнего сообщения в диалоге
            $dialog->last_message_time = $message->created_at;
            $dialog->lastMessage()->associate($message);
            $dialog->saveOrFail();


        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();

        return $message;
    }

    /**
     * @param DialogMessageDto $dto
     * @return Message
     */
    public function createDialogMessage(DialogMessageDto $dto): Message
    {
        DB::beginTransaction();
        try {
            $dialog = $dto->getDialog();

            $dialog->users()->syncWithoutDetaching($dto->getUserFrom());

            $message = new Message;
            $message->content = $dto->getContent();
            $message->read = false;
            $message->userFrom()->associate($dto->getUserFrom());
            $message->dialog()->associate($dialog);
            $message->saveOrFail();

            // отправляем сообщение на сокет
            event(new DialogMessage($message, $dialog));
            // отправляем оповещения на другие каналы
            Notification::send($dialog->users, new NewMessageNotification($message, $dialog));

            // обновляем дату последнего сообщения в диалоге
            $dialog->last_message_time = $message->created_at;
            $dialog->lastMessage()->associate($message);
            $dialog->saveOrFail();

            DialogUser::where('user_id', '<>', $dto->getUserFrom()->id)
                ->where('dialog_id', $dialog->id)
                ->update(['readed' => 0]);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
        return $message;
    }

    /**
     * @param User $user
     * @param string $content
     * @return Message
     * @throws BlackListException
     * @throws \Throwable
     */
    public function sendMessageFromAdmin(User $user, string $content): Message
    {
        // получаем админа
        $adminUser = User::findOrFail(config('support.admin_support_id'));
        $messageDto = new MessageDto([
            'user_from' => $adminUser,
            'user_to' => $user,
            'content' => $content
        ]);
        return $this->createMessage($messageDto);
    }

    public function markMessageAsRead(Message $message, User $user): bool
    {
        // проставляем статус прочитано у всех сообщений, которые получил этот пользователь в рамках диалога
        $status = Message::where('id', '<=', $message->id)
            ->where('dialog_id', $message->dialog_id)
            ->where('user_to_id', $user->id)
            ->update(['read' => 1]);
        return $status;
    }

    public function listDialogMessages($dialog_id)
    {
        return Message::where('dialog_id', $dialog_id)
            ->orderBy('created_at', 'desc')
            ->limit(50)
            ->get();
    }

    /**
     * Get user's unread messages count
     * @param User $user
     * @return int
     */
    public function getUnreadCount(User $user): int
    {
        return Message::where('user_to_id', $user->id)
            ->where('read', 0)
            ->count();
    }

}
