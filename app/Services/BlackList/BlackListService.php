<?php

namespace App\Services\BlackList;

use App\Entity\BlackList\BlackListUser;
use App\Entity\Users\User;
use Illuminate\Support\Facades\Cache;

class BlackListService {

    /**
     * Проверка, находится ли userTo в блэклисте к userFrom
     * @param User $userFrom
     * @param User $userTo
     * @return bool
     */
    public function inBlackList(User $userFrom, User $userTo): bool
    {
        return Cache::rememberForever('black_list_'.$userFrom->id.'_'.$userTo->id,
            function () use ($userFrom, $userTo) {
                $entry = BlackListUser::where([
                    'user_from_id' => $userFrom->id,
                    'user_to_id' => $userTo->id,
                ])->first();
                return !!$entry;
            });
    }

    /**
     * @param User $userFrom
     * @param User $userTo
     * @return BlackListUser
     * @throws \Throwable
     */
    public function addToBlackList(User $userFrom, User $userTo): BlackListUser
    {
        $entry = BlackListUser::firstOrCreate([
            'user_from_id' => $userFrom->id,
            'user_to_id' => $userTo->id,
        ]);
        // запоминаем в кэше
        Cache::forever('black_list_'.$userFrom->id.'_'.$userTo->id, true);
        return $entry;
    }

    /**
     * @param User $userFrom
     * @param User $userTo
     * @return bool
     */
    public function removeFromBlackList(User $userFrom, User $userTo): bool
    {
        $entry = BlackListUser::where([
            'user_from_id' => $userFrom->id,
            'user_to_id' => $userTo->id,
        ])->firstOrFail();
        $result = $entry->delete();

        // удаляем из кэша
        if($result) {
            Cache::pull('black_list_'.$userFrom->id.'_'.$userTo->id);
        }

        return $result;
    }

}
