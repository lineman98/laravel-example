<?php

namespace App\Services\Offers;

use App\DTO\Offers\OfferCreateDto;
use App\DTO\Offers\OfferUpdateDto;
use App\Entity\Messages\Dialog;
use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferLink;
use App\Entity\Users\User;
use App\Events\OfferCreated;
use App\Filters\OffersFilter;
use App\Jobs\OfferCreatedSendAdminNotifications;
use App\Jobs\OfferEndedSendNotifications;
use App\Notifications\OfferActivated;
use App\Notifications\OfferBlocked;
use App\Notifications\OfferEnded;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OfferDialogService {
    public function createOfferDialog(Offer $offer) {
        $dialog = $offer->dialog()->create([
            'type' => 'chat',
            'name' => $offer->name
        ]);

        return $dialog;
    }
}
