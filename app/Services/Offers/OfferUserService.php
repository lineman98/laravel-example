<?php

namespace App\Services\Offers;

use Carbon\Carbon;
use App\Entity\Offers\Offer;
use App\Entity\Users\User;
use App\Entity\Offers\OfferUser;
use App\Notifications\NewOfferApplication;
use Illuminate\Broadcasting\PrivateChannel;
use App\Notifications\OfferApplicationAgreed;
use App\Notifications\OfferApplicationRefused;

class OfferUserService
{
    /**
     * @param int $offer_id
	 * @param int $count
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(int $offer_id, int $count=20)
    {
		$resources = OfferUser::with('user', 'offer')
			->where('offers_user.id', $offer_id)
			->paginate($count);

        return $resources;
    }

	public function create(int $user_id, int $offer_id, $message, string $status)
	{
        $resource = OfferUser::firstOrCreate([
			'user_id' => $user_id,
			'offer_id' => $offer_id
		], [
            'user_id' => $user_id,
            'offer_id' => $offer_id,
            'message' => $message,
            'status' => $status,
            'created_at' => Carbon::now()
		]);

		if($resource->status == OfferUser::STATUS_SENDED)
		{
			$offer = Offer::findOrFail($offer_id);
			$user = User::findOrFail($offer->user_id);

			$resource->user()->associate($user);
			$resource->offer()->associate($offer);

			$user->notify((new NewOfferApplication($resource)));//->delay(random_int(1, 1)));
		}
		return $this->get($resource->id);
	}

	public function findByUserId(Offer $offer, $userId) {
        return OfferUser::where('offer_id', $offer->id)->where('user_id', $userId)->first();
    }

    public function agree(OfferUser $offerUser)
    {
        $offerUser->status = OfferUser::STATUS_ACCEPT;
        $offerUser->save();

        $offerUser->user->notify(new OfferApplicationAgreed($offerUser));

        return true;
    }

    public function refuse(OfferUser $offerUser)
    {
        $offerUser->status = OfferUser::STATUS_REJECT;
        $offerUser->save();

        $offerUser->user->notify(new OfferApplicationRefused($offerUser));

        return true;
    }

    public function update($id, $data) {
        $offerUser = $this->get($id);
        foreach($data as $key => $value) {
            $offerUser->{$key} = $value;
        }
        $offerUser->save();
        return 1;
    }

	public function get($id)
	{
        $resource = OfferUser::with('user', 'offer')
            ->findOrFail($id);

		return $resource;
	}
}
