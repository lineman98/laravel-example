<?php

namespace App\Services\Offers;

use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferLink;
use App\Entity\Users\User;
use App\Filters\UsersCountFilter;
use App\Notifications\ManagerAgreeOffer;
use App\Notifications\ManagerRefuseOffer;
use App\Notifications\ModelAgreedOffer;
use App\Notifications\NewOfferInvite;
use Illuminate\Database\Eloquent\Builder;

class OfferLinksService {

    /**
     * Invite user to offer
     * @param User $user
     * @param Offer $offer
     * @return OfferLink
     * @throws \Throwable
     */
    public function create(User $user, Offer $offer)
    {
        $offerLink = OfferLink::where([
            'user_id' => $user->id,
            'offer_id' => $offer->id,
        ])->first();

        if($offerLink)
            return $offerLink;

        $offerLink = new OfferLink;
        $offerLink->user()->associate($user);
        $offerLink->offer()->associate($offer);
        $offerLink->saveOrFail();

        // send notify to user
        $user->notify((new NewOfferInvite($offerLink))->delay(random_int(1, 180)));

        return $offerLink;
    }

    /**
     * @param User $manager
     * @param array $params
     * @return Builder
     */
    public function getSearchQueryModels(User $manager, array $params=[]): Builder
    {
        $manager_id = $manager->id;

        // статусы, к которым менеджер имеет доступ
        $userAccessStatuses = $manager->accessStatuses->pluck('id');

        $usersFilter = new UsersCountFilter($params);

        $query = User::filter($usersFilter)
            ->girl()
            ->active()
            ->verified()
            ->phoneVerified()
            ->where('gender', 'female')
            ->where('subscription_payed', 1)
            ->where('users.id', '!=', $manager_id);

        // статусы моделей, к которым менеджер имеет доступ
        $query = $query->whereHas('modelStatuses', function ($subQuery) use ($userAccessStatuses) {
            $subQuery->whereIn('status_id', $userAccessStatuses);
        });

        // проверка на черный список
        // 1. проверка, есть ли менеджер в чс у модели
        // 2. проверка, есть ли модель в чс у менеджера
        $query = $query->whereNotExists(function ($subQuery) use ($manager_id) {
            $subQuery->selectRaw('id')
                ->from('black_list_users')
                ->where(function($query) use ($manager_id) {
                    $query->where('user_from_id', $manager_id)
                        ->whereRaw('user_to_id = users.id');
                })->orWhere(function($query) use ($manager_id) {
                    $query->where('user_to_id', $manager_id)
                        ->whereRaw('user_from_id = users.id');
                });
        });

        // категории, из которых модель хочет получать предложения
        if (isset($params['category'])) {
            $category_id = $params['category'];
            $query->where(function ($subQuery) use ($category_id) {
                $subQuery->whereDoesntHave('offerCategories')
                    ->orWhereHas('offerCategories', function ($query) use ($category_id) {
                        $query->where('offer_category_id', $category_id);
                    });
            });
        }

        return $query;
    }

    /**
     * @param User $user
     * @param $offer_link_id
     * @return mixed
     */
    public function getLinkForManager(User $user, $offer_link_id)
    {
        return OfferLink::join('offers', 'offers.id', '=', 'offer_links.offer_id')
            ->where('offer_links.id', $offer_link_id)
            ->where('offers.user_id', $user->id)
            ->selectRaw('offer_links.*')
            ->firstOrFail();
    }

    /**
     * @param User $user
     * @param $offer_link_id
     * @return mixed
     */
    public function getLinkForModel(User $user, $offer_link_id)
    {
        return OfferLink::where('id', $offer_link_id)
            ->where('user_id', $user->id)
            ->firstOrFail();
    }

    /**
     * Отказ от предложения моделью
     * @param OfferLink $offerLink
     * @return bool
     * @throws \Throwable
     */
    public function refuseFromModel(OfferLink $offerLink)
    {
        $changeStatusResult = $offerLink->setModelRefused();
        return $changeStatusResult;
    }

    /**
     * Модель согласилась на предложение
     * @param OfferLink $offerLink
     * @return bool
     * @throws \Throwable
     */
    public function agreeFromModel(OfferLink $offerLink): bool
    {
        $changeStatusResult = $offerLink->setModelAgreed();

        // если статус изменился, то оповещаем менеджера
        if($changeStatusResult) {
            $offerLink->offer->user->notify(new ModelAgreedOffer($offerLink));
        }

        return $changeStatusResult;
    }

    /**
     * Менеджер отказал модели
     * @param OfferLink $offerLink
     * @return bool
     * @throws \Throwable
     */
    public function refuseFromManager(OfferLink $offerLink): bool
    {
        $changeStatusResult = $offerLink->setManagerRefused();

        // если статус изменился, то оповещаем модель
        if($changeStatusResult) {
            $offerLink->user->notify(new ManagerRefuseOffer($offerLink));
        }

        return $changeStatusResult;
    }

    /**
     * Менеджер дал согласие модели
     * @param OfferLink $offerLink
     * @return bool
     * @throws \Throwable
     */
    public function agreeFromManager(OfferLink $offerLink): bool
    {
        $changeStatusResult = $offerLink->setManagerAgreed();

        // если статус изменился, то оповещаем модель
        if($changeStatusResult) {
            $offerLink->user->notify(new ManagerAgreeOffer($offerLink));
        }

        return $changeStatusResult;
    }

}
