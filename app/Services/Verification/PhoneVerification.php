<?php

namespace App\Services\Verification;

use App\Entity\Users\User;
use App\Entity\Verification\PhoneActivate;
use App\Services\Sms\SmsSender;
use App\Services\Verification\Exceptions\AlreadyVerifiedException;
use App\Services\Verification\Exceptions\ExpiredException;
use App\Services\Verification\Exceptions\LimitException;
use App\Services\Verification\Exceptions\VerificationException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\TwilioException;

class PhoneVerification
{

    private $sender;

    public function __construct(SmsSender $sender)
    {
        $this->sender = $sender;
    }

    /**
     * @param User $user
     * @param string $phone
     * @return bool
     * @throws AlreadyVerifiedException
     */
    public function sendSms(User $user, $type=null)
    {
        $activate_model = PhoneActivate::where('user_id', $user->id)
            ->where('type', $type)
            ->where('method', 'sms')
            ->first();

        if (!$activate_model) {
            $activate_model = new PhoneActivate;
            $activate_model->user_id = $user->id;
            $activate_model->type = $type;
            $activate_model->method = 'sms';
        }

        if($activate_model->success)
            throw new AlreadyVerifiedException();

        $code = random_int(100000, 999999);
        $activate_model->tries = 0;
        $activate_model->phone = $user->phone;
        $activate_model->code = $code;
        $activate_model->expires_at = time() + 60*10;
        $activate_model->save();

        try {
            $this->sender->send($user->phone, config('app.name') . '. Your code: ' . $code);
            return true;
        } catch (TwilioException $exception) {
            Log::error('Send sms exception: ' . $exception->getMessage());
            return false;
        }
    }

    /**
     * @param User $user
     * @param string $code
     * @return bool
     * @throws ExpiredException
     * @throws VerificationException
     * @throws LimitException
     * @throws \Throwable
     */
    public function verify(User $user, string $code, $type=null, $with_delete=false)
    {
        $activate_model = PhoneActivate::where('user_id', $user->id)
            ->where('type', $type)
            ->where('method', 'sms')
            ->first();
        if (!$activate_model)
            throw new VerificationException("Код не найден");
        if($activate_model->expires_at < time())
            throw new ExpiredException("Код устарел. Отправьте новый код");
        if($activate_model->tries > 3)
            throw new LimitException("Лимит попыток. Отправьте новый код на телефон");
        if ($activate_model->code !== $code) {
            $activate_model->tries++;
            $activate_model->save();
            return false;
        }
        $activate_model->success = true;
        $activate_model->save();

        // подтверждаем телефон пользователя
        if(!$type)
            $user->setVerifiedPhone();

        // удаляем историю смски
        if($with_delete)
            $activate_model->delete();

        return true;
    }

    /**
     * Проверка телефона через Firebase ID token
     * @param User $user
     * @param string $idToken
     * @param null $type
     * @return bool
     * @throws VerificationException
     * @throws \Throwable
     */
    public function verifyByFirebaseIdToken(User $user, string $idToken, $type=null)
    {

        $auth = (new \Kreait\Firebase\Factory())
            ->withServiceAccount(storage_path('firebase_credentials.json'))
           ->withDisabledAutoDiscovery()
            ->createAuth();

        try {
            $verificationIdToken = $auth->verifyIdToken($idToken);
        } catch (\Throwable $e) {
            throw new VerificationException("Передан некорректный токен");
        }

        $uid = $verificationIdToken->getClaim('sub');

        try {
            $firebaseUser = $auth->getUser($uid);
        } catch (\Throwable $e) {
            throw new VerificationException("Ошибка проверки кода. Попробуйте еще раз");
        }

        // если токен не подходит для этого юзера (различаются номера телефонов)
        if ($firebaseUser->phoneNumber !== $user->phone) {
            throw new VerificationException("Некорректный номер телефона");
        }

        // подтверждаем телефон пользователя
        if(!$type)
            $user->setVerifiedPhone();

        return true;
    }


    /**
     * @param User $user
     * @param string $phone
     * @return bool
     * @throws AlreadyVerifiedException
     */
    public function sendCall(User $user, $type=null)
    {
        $activate_model = PhoneActivate::where('user_id', $user->id)
            ->where('type', $type)
            ->where('method', 'call')
            ->first();

        if (!$activate_model) {
            $activate_model = new PhoneActivate;
            $activate_model->user_id = $user->id;
            $activate_model->type = $type;
        }

        if($activate_model->success)
            throw new AlreadyVerifiedException();

        $code = random_int(1000, 9999);
        $activate_model->tries = 0;
        $activate_model->phone = $user->phone;
        $activate_model->code = $code;
        $activate_model->expires_at = time() + 60*10;
        $activate_model->method = 'call';

        // отправляем звонок
        $client = new Client;
        $response = $client->post('https://api.checkmobi.com/v1/validation/request', [
            'json' => [
                'number' => $user->phone,
                'type' => 'reverse_cli'
            ],
            'headers' => [
                'Authorization' => '35475A5E-0B9B-4ABE-861F-D3DCCB7434E1'
            ]
        ]);
        $response = $response->getBody()->getContents();
        $response = json_decode($response, true);

        // записываем id звонка
        $activate_model->info = $response['id'];
        $activate_model->save();

        return true;
    }

    /**
     * @param User $user
     * @param string $code
     * @return bool
     * @throws ExpiredException
     * @throws VerificationException
     * @throws LimitException
     * @throws \Throwable
     */
    public function verifyCall(User $user, string $code, $type=null)
    {
        $activate_model = PhoneActivate::where('user_id', $user->id)
            ->where('type', $type)
            ->where('method', 'call')
            ->first();
        if (!$activate_model)
            throw new VerificationException("Код не найден");
        if($activate_model->expires_at < time())
            throw new ExpiredException("Код устарел. Отправьте новый код");
        if($activate_model->tries > 3)
            throw new LimitException("Лимит попыток. Отправьте новый код на телефон");


        $client = new Client;
        $response = $client->post('https://api.checkmobi.com/v1/validation/verify', [
            'json' => [
                'id' => $activate_model->info,
                'pin' => $code
            ],
            'headers' => [
                'Authorization' => '35475A5E-0B9B-4ABE-861F-D3DCCB7434E1'
            ]
        ]);
        $response = $response->getBody()->getContents();
        $response = json_decode($response, true);

        if (!$response['validated']) {
            $activate_model->tries++;
            $activate_model->save();
            return false;
        }
        $activate_model->success = true;
        $activate_model->save();

        // подтверждаем телефон пользователя
        if(!$type)
            $user->setVerifiedPhone();

        return true;
    }

}
