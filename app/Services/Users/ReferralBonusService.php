<?php

namespace App\Services\Users;

use App\Entity\Users\User;
use App\Entity\Users\UserBonusHistory;
use App\Entity\Users\UserReferralBonus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReferralBonusService
{

    /**
     * Начисление бонусов за оплату пользователем
     * @param User $referrer
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function incrementBonuses(User $referrer, User $user): bool
    {
        DB::beginTransaction();

        try {
            // проверяем, была ли уже засчитана оплата от этого пользователя
            $bonus = UserReferralBonus::where([
                'user_id' => $referrer->id,
                'user_from_id' => $user->id
            ])->first();

            // выходим, если бонус уже был начислен
            if ($bonus)
                return false;

            // пополняем баланс
            UserReferralBonus::create([
                'user_id' => $referrer->id,
                'user_from_id' => $user->id
            ]);

            // увеличиваем баланс
            $referrer->referral_balance = $referrer->referral_balance + 1;
            $referrer->save();

            // количество оплат, необходимых для активации месяца доступа
            $referralsCountForBonus = $referrer->isManager()
                ? config('plans.manager_free_month_referrals')
                : config('plans.model_free_month_referrals');

            // если хватает баланса для активации бонуса
            if ($referrer->isManager() && $referrer->referral_balance >= $referralsCountForBonus) {
                $currentUserSubscription = $referrer->getCurrentSubscription();
                if ($currentUserSubscription->active()) {
                    $currentUserSubscription->ends_at = $currentUserSubscription->ends_at->addMonth();
                } else {
                    $currentUserSubscription->ends_at = now()->addMonth();
                }
                $currentUserSubscription->save();

                $newBalance = $referrer->referral_balance - $referralsCountForBonus;

                // сохраняем бонус в историю
                UserBonusHistory::create([
                    'user_id' => $referrer->id,
                    'type' => 'bonus_month',
                    'balance_before' => $referrer->referral_balance,
                    'balance_after' => $newBalance
                ]);

                $referrer->subscription_payed = true;
                $referrer->referral_balance = $newBalance;
                $referrer->save();
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Error bonus referrer ' . $exception->getMessage());
            throw $exception;
        }

        DB::commit();
        return true;
    }

}
