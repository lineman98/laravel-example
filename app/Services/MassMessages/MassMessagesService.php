<?php

namespace App\Services\MassMessages;

use App\DTO\MassMessages\MassMessageDto;
use App\DTO\Messages\MessageDto;
use App\Entity\MassMessages\MassMessage;
use App\Entity\MassMessages\MassMessageUser;
use App\Entity\Users\User;
use App\Jobs\MassMessages\AddUsersToMassMessage;
use App\Jobs\MassMessages\SendMassMessages;
use App\Services\Messages\MessageService;
use App\Services\Users\UsersService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class MassMessagesService
{

    /** @var MessageService */
    private $messageService;

    /** @var UsersService */
    private $usersService;

    public function __construct(MessageService $messageService, UsersService $usersService)
    {
        $this->messageService = $messageService;
        $this->usersService = $usersService;
    }

    /**
     * @param MassMessageDto $dto
     * @return MassMessage
     * @throws \Throwable
     */
    public function create(MassMessageDto $dto): MassMessage
    {
        $massMessage = new MassMessage;
        $massMessage->content = $dto->getContent();
        $massMessage->filter = $dto->getFilter();
        $massMessage->status = MassMessage::STATUS_WAIT;
        $massMessage->saveOrFail();

        // добавляем пользователей в рассылку
        AddUsersToMassMessage::dispatch($massMessage);

        return $massMessage;
    }

    public function find($id)
    {
        return MassMessage::findOrFail($id);
    }

    public function paginate(int $count)
    {
        return MassMessage::withCount('users')
            ->orderBy('created_at', 'desc')
            ->paginate($count);
    }

    /**
     * @param MassMessage $massMessage
     * @return bool
     * @throws \Throwable
     */
    public function startSending(MassMessage $massMessage): bool
    {
        $massMessage->status = MassMessage::STATUS_SENDING;
        $status = $massMessage->saveOrFail();
        if ($status) {
            // отправляем рассылку в очередь
            SendMassMessages::dispatch($massMessage);
        }
        return $status;
    }

    /**
     * @param MassMessage $massMessage
     * @return bool
     * @throws \Throwable
     */
    public function finishSending(MassMessage $massMessage): bool
    {
        $massMessage->status = MassMessage::STATUS_COMPLETED;
        $status = $massMessage->saveOrFail();
        return $status;
    }

    /**
     * Добавление пользователей в рассылку по результатам фильтра
     * @param MassMessage $massMessage
     */
    public function addUsersToMassMessage(MassMessage $massMessage)
    {
        $filterParams = $massMessage->getFilterParams();
        $this->usersService->chunkWithFilter(100, $filterParams, function ($users) use ($massMessage) {
            $users->each(function (User $user) use ($massMessage) {
                $massMessage->users()->firstOrCreate(['user_id' => $user->id]);
            });
        });
    }

    /**
     * Отправка сообщений
     * @param MassMessage $massMessage
     */
    public function sendMessages(MassMessage $massMessage)
    {
        // получаем админа
        $adminUser = User::findOrFail(config('support.admin_support_id'));

        $massMessage->users()->where('sended', false)
            ->chunkById(100, function (Collection $massMessageUsers) use ($massMessage, $adminUser) {
                $massMessageUsers->each(function (MassMessageUser $massMessageUser) use ($massMessage, $adminUser) {
                    $messageDto = $this->createMessageDto($massMessage, $adminUser, $massMessageUser->user);
                    try {
                        $message = $this->messageService->createMessage($messageDto);
                        $massMessageUser->sended = true;
                        $massMessageUser->message()->associate($message);
                        $massMessageUser->saveOrFail();
                    } catch (\Throwable $exception) {

                    }
                });
            });
    }

    /**
     * @param MassMessage $massMessage
     * @param User $userFrom
     * @param User $userTo
     * @return MessageDto
     */
    public function createMessageDto(MassMessage $massMessage, User $userFrom, User $userTo): MessageDto
    {
        return new MessageDto([
            'user_from' => $userFrom,
            'user_to' => $userTo,
            'content' => $massMessage->content,
        ]);
    }

    /**
     * @param MassMessage $massMessage
     * @return bool
     * @throws \Exception
     */
    public function destroy(MassMessage $massMessage): bool
    {
        return $massMessage->delete();
    }

}
