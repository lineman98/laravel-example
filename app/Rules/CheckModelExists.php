<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckModelExists implements Rule
{

    protected $claimable_type;

    /**
     * CheckModelExists constructor.
     * @param string $claimable_type
     */
    public function __construct(string $claimable_type)
    {
        $this->claimable_type = $claimable_type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $model_class = config('relations.map_relations.'.$this->claimable_type);
        try {
            $model = app($model_class);
            return !!$model->find($value);
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Incorrect parameters';
    }
}
