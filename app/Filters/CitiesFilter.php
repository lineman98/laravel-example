<?php

namespace App\Filters;

class CitiesFilter extends BaseFilter
{

    public function query($value)
    {
        $this->builder->where('name', 'like', '%'.$value.'%');
    }

    public function country_id($value)
    {
        if(!is_array($value)) {
            $this->builder->where('cities.country_id', $value);
        } else {
            $this->builder->whereIn('cities.country_id', $value);
        }
    }

    public function search($value) {
        $this->builder->whereHas('translations', function($query) use ($value) {
            $query->where('name', 'like', "%$value%");
        });
    }


}
