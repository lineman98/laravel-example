<?php

namespace App\Filters;

class ToursFilter extends BaseFilter
{

    public function query($value)
    {
        $this->builder->where('description', 'like', '%'.$value.'%');
    }

    public function status($value)
    {
        if(!is_array($value)) {
            $this->builder->where('tours.status', $value);
        } else {
            $this->builder->whereIn('tours.status', $value);
        }
    }

    public function users($value)
    {
        if(!is_array($value)) {
            $this->builder->where('tours.user_id', $value);
        } else {
            $this->builder->whereIn('tours.user_id', $value);
        }
    }

    public function country($value)
    {
        if(!is_array($value)) {
            $this->builder->where('tours.country_id', $value);
        } else {
            $this->builder->whereIn('tours.country_id', $value);
        }
    }

    public function sort($value)
    {
        if (!$this->hasParameter('order'))
            return;

        $order = $this->getParameter('order');
        if (!in_array($order, ['asc', 'desc']))
            return;

        $this->builder->orderBy($value, $order);
    }

}
