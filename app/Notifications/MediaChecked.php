<?php

namespace App\Notifications;

use App\Entity\Users\User;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidMessagePriority;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;
use NotificationChannels\Fcm\Resources\WebpushConfig;
use Spatie\MediaLibrary\Models\Media;

class MediaChecked extends Notification implements ShouldQueue
{
    use Queueable;

    private $user_id;

    /** @var Media */
    private $media;

    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->user_id = $notifiable->id;
        $channels = ['broadcast'];
        if(count($notifiable->pushTokens())) {
            $channels[] = FcmChannel::class;
        }
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'media_id' => $this->media->id,
            'media_type' => $this->media->collection_name === User::PUBLIC_VIDEOS ? 'video' : 'photo'
        ];
    }

    public function toFcm($notifiable)
    {
        $title = $this->media->collection_name === User::PUBLIC_VIDEOS
            ? '✅ Ваше видео проверено'
            : '✅ Ваша фотография проверена';

        $body = $this->media->collection_name === User::PUBLIC_VIDEOS
            ? 'Теперь его видят посетители вашей страницы'
            : 'Теперь ее видят посетители вашей страницы';

        return FcmMessage::create()
            ->setData([
                'type' => 'MediaChecked',
                'media_id' => $this->media->id,
                'media_type' => $this->media->collection_name === User::PUBLIC_VIDEOS ? 'video' : 'photo'
            ])
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setPriority(AndroidMessagePriority::HIGH())
            )
            ->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
                    ->setHeaders([
                        "apns-priority" => "5"
                    ])
            )
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle($title)
                    ->setBody($body)
                    ->setImage('https://web.laravel-example/static/img/logo.png')
            )
            ->setWebpush(
                WebpushConfig::create()
                    ->setNotification([
                        "title" => $title,
                        "body" => $body,
                        "click_action" => "https://web.laravel-example/",
                        "icon" => "https://web.laravel-example/static/img/logo.png"
                    ])
            );
    }

    public function broadcastType()
    {
        return 'MediaChecked';
    }

    public function broadcastOn()
    {
        return [new PrivateChannel('App.User.'.$this->user_id)];
    }

}
