<?php

namespace App\Notifications;

use App\Entity\Offers\Offer;
use App\Entity\Users\User;
use App\Http\Resources\Users\UserResource;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidMessagePriority;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;
use NotificationChannels\Fcm\Resources\WebpushConfig;

class UserRegisteredAdminNotification extends Notification implements ShouldQueue
{

    use Queueable;

    /** @var User */
    public $user;

    private $user_id;

    /**
     * UserRegisteredAdminNotification constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->user_id = $notifiable->id;
        $channels = ['broadcast'];
        if($notifiable->send_offers_notifications && count($notifiable->pushTokens())) {
            $channels[] = FcmChannel::class;
        }
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user' => new UserResource($this->user),
        ];
    }

    public function toFcm($notifiable)
    {
        $title = "Новая регистрация";
        $message = ('Пользователь') . '. Телефон ' . $this->user->phone;

        return FcmMessage::create()
            ->setData([
                'type' => 'UserRegistered'
            ])
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setPriority(AndroidMessagePriority::HIGH())
            )
            ->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
                    ->setHeaders([
                        "apns-priority" => "5"
                    ])
            )
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle($title)
                    ->setBody($message)
                    ->setImage('https://web.laravel-example/static/img/logo.png')
            )
            ->setWebpush(
                WebpushConfig::create()
                    ->setNotification([
                        "title" => $title,
                        "body" => $message,
                        "click_action" => "http://awwadmin.rusgirls.vip/#/users/".$this->user->id,
                        "icon" => "https://web.laravel-example/static/img/logo.png"
                    ])
            );
    }

    public function broadcastType()
    {
        return 'UserRegistered';
    }

    public function broadcastOn()
    {
        return [new PrivateChannel('App.User.'.$this->user_id)];
    }

}
