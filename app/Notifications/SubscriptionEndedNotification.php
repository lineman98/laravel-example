<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\WebpushConfig;

class SubscriptionEndedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $isTrial;

    /**
     * Create a new notification instance.
     * @param bool $isTrial
     * @return void
     */
    public function __construct(bool $isTrial)
    {
        $this->isTrial = $isTrial;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = ['broadcast'];
        if(count($notifiable->pushTokens())) {
            $channels[] = FcmChannel::class;
        }
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toFcm($notifiable)
    {
        return FcmMessage::create()
            ->setData([
                'param1' => 'baz' // Optional
            ])
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle($this->isTrial ? "Пробный доступ звершен" : "Закончился доступ к приложению")
                    ->setBody("Продлите доступ и продолжайте пользоваться приложением :)")
                    ->setImage('https://web.laravel-example/static/img/logo.png')
            )
            ->setWebpush(
                WebpushConfig::create()
                    ->setNotification([
                        "title" => $this->isTrial ? "Пробный доступ звершен" : "Закончился доступ к приложению",
                        "body" => "Продлите доступ и продолжайте пользоваться приложением :)",
                        "click_action" => "https://web.laravel-example/",
                        "icon" => "https://web.laravel-example/static/img/logo.png"
                    ])
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
