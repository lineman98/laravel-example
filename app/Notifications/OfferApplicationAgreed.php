<?php

namespace App\Notifications;

use App\Entity\Offers\OfferLink;
use App\Entity\Offers\OfferUser;
use App\Http\Resources\Offers\OfferForModelResource;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidMessagePriority;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;
use NotificationChannels\Fcm\Resources\WebpushConfig;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\Http\Resources\Offers\OfferUserResource;
use App\Http\Resources\Offers\OfferShortResource;

class OfferApplicationAgreed extends Notification implements ShouldQueue
{

    use Queueable;

    /** @var OfferLink */
    private $offerLink;

    /**
     * NewOfferInvite constructor.
     * @param OfferLink $offerLink
     */
    public function __construct(OfferUser $offerLink)
    {
        $this->offerLink = $offerLink;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = ['broadcast'];
        if(count($notifiable->pushTokens())) {
            $channels[] = FcmChannel::class;
        }
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'offer' => new OfferForModelResource($this->offerLink->offer),
            'offerLink' => $this->offerLink
        ];
    }

    public function toFcm($notifiable)
    {
        return FcmMessage::create()
            ->setData([
//                'offer_id' => $this->offerLink->offer_id,
//                'offer_link_id' => $this->offerLink->id,
                'type' => 'NewOfferInvite2'
            ])
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle("🔥 Вы получили новую заявку на вписку!")
                    ->setBody("Узнайте подробности в предложении " . $this->offerLink->offer->name)
                    ->setImage('https://web.laravel-example/static/img/logo.png')
            )
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setPriority(AndroidMessagePriority::HIGH())
            )
            ->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
                    ->setHeaders([
                        "apns-priority" => "5"
                    ])
            )
            ->setWebpush(
                WebpushConfig::create()
                    ->setHeaders([
                        "Urgency" => "high"
                    ])
                    ->setNotification([
                        "title" => "🔥 Вы получили новую заявку на вписку!",
                        "body" => "Узнайте подробности в приложении ".$this->offerLink->offer->name,
                        "click_action" => "https://web.laravel-example/",
                        "icon" => "https://web.laravel-example/static/img/logo.png"
                    ])
            );
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'application' => new OfferUserResource($this->offerLink),
            'offer' => new OfferShortResource($this->offerLink->offer)
        ]);
    }

    public function broadcastType()
    {
        return 'offerApplicationAgreed';
    }

    /*public function routeNotificationForBroadcast($notifiable) {
        return [new PrivateChannel('App.User.'.$notifiable->user_id)];
    }*/

    public function broadcastOn()
    {
        return [new PrivateChannel('App.User.'.$this->offerLink->user_id)];
    }

}
