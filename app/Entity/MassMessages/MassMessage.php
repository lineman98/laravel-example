<?php

namespace App\Entity\MassMessages;

use Illuminate\Database\Eloquent\Model;

class MassMessage extends Model
{

    public const STATUS_WAIT = 'wait';
    public const STATUS_SENDING = 'sending';
    public const STATUS_COMPLETED = 'completed';

    public function getFilterParams(): array
    {
        return $this->filter ? json_decode($this->filter, true) : [];
    }

    public function users()
    {
        return $this->hasMany(MassMessageUser::class)
            ->with('user');
    }

}
