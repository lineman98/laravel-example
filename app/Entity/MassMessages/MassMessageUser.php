<?php

namespace App\Entity\MassMessages;

use App\Entity\Messages\Message;
use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class MassMessageUser extends Model
{

    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function massMessage()
    {
        return $this->belongsTo(MassMessage::class, 'mass_message_id');
    }

    public function message()
    {
        return $this->belongsTo(Message::class, 'message_id');
    }

}
