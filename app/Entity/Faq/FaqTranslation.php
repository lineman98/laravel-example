<?php

namespace App\Entity\Faq;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class FaqTranslation extends Model
{

    protected $fillable = ['question', 'answer'];
    public $timestamps = false;

}
