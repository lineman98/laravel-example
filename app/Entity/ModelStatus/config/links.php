<?php

return [
    'short_link_domain' => env('SHORT_LINK_DOMAIN'),
    'short_link_domain_two' => env('SHORT_LINK_DOMAIN_TWO'),
    'short_link_host' => env('SHORT_LINK_HOST'),
];
