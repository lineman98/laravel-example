<?php

return [
    'model_plan_id' => 1,
    'manager_plan_id' => 2,

    'tour_publication_cost' => 990,
    'offer_publication_cost' => 990,

    // количество пользователей, которые должны оплатить чтобы менеджер получил бесплатный месяц
    'manager_free_month_referrals' => 5,
    // количество пользователей, которые должны оплатить чтобы модель получила бесплатный месяц
    'model_free_month_referrals' => 2,
];
