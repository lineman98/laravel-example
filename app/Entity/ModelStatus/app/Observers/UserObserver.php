<?php

namespace App\Observers;

use App\Entity\Messages\Dialog;
use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferLink;
use App\Entity\Tours\Tour;
use App\Entity\Users\User;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Entity\Users\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Entity\Users\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Entity\Users\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        // удаляем все диалоги с этим пользователем
        Dialog::where('user_first', $user->id)
            ->orWhere('user_second', $user->id)
            ->delete();

        // удаляем все отклики пользователя на предложения
        OfferLink::where('user_id', $user->id)->delete();

        // удаляем все предложения пользователя
        Offer::where('user_id', $user->id)->delete();

        // удаляем все туры пользователя
        Tour::where('user_id', $user->id)->delete();
    }
}
