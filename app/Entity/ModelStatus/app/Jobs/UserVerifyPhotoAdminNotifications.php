<?php

namespace App\Jobs;

use App\Entity\Users\User;
use App\Notifications\UserSendVerifyPhotoAdminNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;

class UserVerifyPhotoAdminNotifications implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var User */
    private $user;

    /**
     * UserRegisteredSendAdminNotifications constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $admins = Cache::remember('admins_users', 120, function () {
           return User::where('role', User::ROLE_ADMIN)->get();
        });
        Notification::send($admins, new UserSendVerifyPhotoAdminNotification($this->user));
    }
}
