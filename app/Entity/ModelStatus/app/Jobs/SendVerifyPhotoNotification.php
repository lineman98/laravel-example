<?php

namespace App\Jobs;

use App\Entity\Offers\Offer;
use App\Entity\Users\User;
use App\Notifications\OfferCreateAdminNotification;
use App\Notifications\VerifyPhotoNotification;
use App\Services\Messages\MessageService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;

class SendVerifyPhotoNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var User */
    private $user;

    /** @var MessageService */
    private $message_service;

    /**
     * SendVerifyPhotoNotification constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->message_service = app(MessageService::class);
    }

    public function handle()
    {
        // если еще не отправил заявку на проверку
        if ($this->user->isNotVerified()) {
            $this->user->notify(new VerifyPhotoNotification);

            // отправляем сообщение
            $message = '🆘 Пожалуйста, пройдите проверку анкеты чтобы начать получать предложения.'.PHP_EOL.PHP_EOL.'❗ Без проверки вы не получите предложения!'.PHP_EOL.PHP_EOL.'🆓 Проверка бесплатная! Она нужна чтобы подтвердить достоверность Ваших фотографий.'.PHP_EOL.PHP_EOL.'Для проверки анкеты откройте левое меню (нажмите на иконку "три полоски") и нажмите на кнопку "пройти проверку".'.PHP_EOL.PHP_EOL.'💥 Любой вопрос вы можете задать в этом чате.';
            if ($this->user->isManager())
                $message = 'Здравствуйте! Чтобы пройти проверку и получить доступ к публикации предложений вам нужно добавить 5 моделей, которые от вас работали. Модели должны пройти регистрацию с Вашим пригласительным кодом!'.PHP_EOL.PHP_EOL.'Также есть вариант с оплатой доступа. Месяц доступа стоит 4990 рублей.'.PHP_EOL.PHP_EOL.'Какой вариант вам интересен?';
            $this->message_service->sendMessageFromAdmin($this->user, $message);
        }
    }
}
