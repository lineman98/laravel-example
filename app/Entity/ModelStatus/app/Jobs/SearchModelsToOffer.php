<?php

namespace App\Jobs;

use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferLink;
use App\Entity\Users\User;
use App\Filters\UsersCountFilter;
use App\Services\Offers\OfferLinksService;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SearchModelsToOffer implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Offer */
    public $offer;

    /** @var OfferLinksService */
    private $offerLinkService;

    /**
     * SearchModelsToOffer constructor.
     * @param Offer $offer
     */
    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
        $this->offerLinkService = new OfferLinksService;
    }

    public function handle()
    {
        $offer = $this->offer;

        // если предложение уже неактивно, выходим из джобы
        if ($offer->deleted_at || !$offer->isActive())
            return;

        $params = [
            'age_from' => $offer->age_from,
            'age_to' => $offer->age_to,
            'weight_from' => $offer->weight_from,
            'weight_to' => $offer->weight_to,
            'height_from' => $offer->height_from,
            'height_to' => $offer->height_to,
            'boobs_from' => $offer->boobs_from,
            'boobs_to' => $offer->boobs_to,
            'boobs_type' => $offer->boobs_type,
            'only_with_reviews' => $offer->only_with_reviews,
            'eyes' => $offer->eyes->pluck('id'),
            'nationalities' => $offer->nationalities->pluck('id'),
            'hairs' => $offer->hairs->pluck('id'),
            'visas' => $offer->visas->pluck('id'),
            'country_from' => $offer->country_from_id,
            'city_from' => $offer->city_from_id,
            'search_from_same_city' => $offer->search_from_same_city,
            'nearest_city_range' => $offer->nearest_city_range,
            'category' => $offer->category_id,
        ];

        $query = $this->offerLinkService->getSearchQueryModels($offer->user, $params);

        // проверка, приглашена ли уже в предложение
        $query = $query->whereNotExists(function ($subQuery) use ($offer) {
            $subQuery->selectRaw('id')
                ->from('offer_links')
                ->where('offer_id', $offer->id)
                ->whereRaw('user_id = users.id');
        });

        // добавляем найденных моделей в предложение
        $query->chunkById(50, function (Collection $users) use (&$offer) {
            $users->each(function (User $user) use (&$offer) {
                $this->offerLinkService->create($user, $offer);
            });
        });
    }
}
