<?php

namespace App\DTO\MassMessages;

use App\DTO\BaseDto;

class MassMessageDto extends BaseDto
{

    /** @var string */
    private $content;

    /** @var string|null */
    private $filter;

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string|null
     */
    public function getFilter(): ?string
    {
        return $this->filter;
    }

    /**
     * @param string|null $filter
     */
    public function setFilter(?string $filter): void
    {
        $this->filter = $filter;
    }

}
