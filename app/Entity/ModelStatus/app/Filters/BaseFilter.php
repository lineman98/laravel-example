<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

abstract class BaseFilter
{

    protected $params;

    /** @var Builder */
    protected $builder;

    public function __construct(array $params = [])
    {
        $this->params = $params;
    }

    public function __get(string $name)
    {
        return $this->getParameter($name);
    }

    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        $this->beforeApplyFilters();

        foreach ($this->filters() as $filter => $value) {
            if($value === null || $value === 'null' || (is_array($value) && !count($value)))
                continue;
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        $this->afterApplyFilters();

        return $this->builder;
    }

    public function hasParameter(string $key): bool
    {
        return array_key_exists($key, $this->params);
    }

    public function getParameter(string $key)
    {
        if (!$this->hasParameter($key))
            return null;
        return $this->params[$key];
    }

    public function filters(): array
    {
        return $this->params;
    }

    public function beforeApplyFilters(): void
    {
        return;
    }

    public function afterApplyFilters(): void
    {
        return;
    }

}
