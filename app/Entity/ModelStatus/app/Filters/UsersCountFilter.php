<?php

namespace App\Filters;

use App\Entity\Geo\City\City;
use Illuminate\Database\Query\Builder;

class UsersCountFilter extends BaseFilter
{

    /**
     * Страна, из которой ищем моделей
     * @param $value
     */
    public function country_from($value)
    {
        if ($this->nearest_city_range)
            return;

        if ($this->search_from_same_city)
            return;

        // если выбрана страна, из которой искать
        $this->builder->where('country_id', $value);
    }

    /**
     * Город, из которого ищем моделей
     * @param $value
     */
    public function city_from($value)
    {
        if ($this->nearest_city_range)
            return;

        // поиск из города предложения
        if ($this->search_from_same_city)
            return;

        // если выбран город, из которого искать
        $this->builder->where('city_id', $value);
    }

    /**
     * Соседние города, из которых ищем моделей
     * @param $distance_in_km
     */
    public function nearest_city_range($distance_in_km)
    {
        if (!$distance_in_km)
            return;

        // выбираем город, из которого должен идти поиск
        if ($this->search_from_same_city) {
            $main_city = $this->city;
        } else if ($this->city_from) {
            $main_city = $this->city_from;
        } else {
            return;
        }

        $main_city = City::find($main_city);
        if (!$main_city)
            return;

        $lat = $main_city->lat;
        $lng = $main_city->lng;

        // если не заданы координаты города, то ищем в самом городе
        if (!$lat || !$lng) {
            $this->builder->where('city_id', $main_city->id);
            return;
        }

        $this->builder->whereIn('city_id',
            function (Builder $query) use ($distance_in_km, $lat, $lng) {
                $query->selectRaw('cities.id')
                    ->from('cities')
                    ->joinSub(function (Builder $query) use ($distance_in_km, $lat, $lng) {
                        $query->from('cities')->selectRaw(
                            "111.045 * DEGREES(ACOS(COS(RADIANS(?))
                                 * COS(RADIANS(lat))
                                 * COS(RADIANS(lng) - RADIANS(?))
                                 + SIN(RADIANS(?))
                                 * SIN(RADIANS(lat))))
                                 AS distance_in_km, id",
                            [$lat, $lng, $lat]
                        )->having('distance_in_km', '<=', $distance_in_km);
                    }, 'table_2', 'table_2.id', '=', 'cities.id');
            });
    }

    /**
     * Поиск моделей из города проведения предложения
     * @param $value
     */
    public function search_from_same_city($value)
    {
        if (!$value)
            return;

        if ($this->nearest_city_range)
            return;

        $this->builder->where('city_id', $this->city)
            ->where('country_id', $this->country);
    }

    public function age_from($value)
    {
        $this->builder->where('age', '>=', $value);
    }

    public function age_to($value)
    {
        $this->builder->where('age', '<=', $value);
    }

    public function weight_from($value)
    {
        $this->builder->where('weight', '>=', $value);
    }

    public function weight_to($value)
    {
        $this->builder->where('weight', '<=', $value);
    }

    public function height_from($value)
    {
        $this->builder->where('height', '>=', $value);
    }

    public function height_to($value)
    {
        $this->builder->where('height', '<=', $value);
    }

    public function boobs_from($value)
    {
        $this->builder->where('boobs_size', '>=', $value);
    }

    public function boobs_to($value)
    {
        $this->builder->where('boobs_size', '<=', $value);
    }

    public function boobs_type($value)
    {
        if ($value === 'all')
            return;
        if ($value === 'natural') {
            $this->builder->where('boobs_type', null);
        } else {
            $this->builder->where('boobs_type', $value);
        }
    }

    public function only_with_reviews($value)
    {
        if ($value)
            $this->builder->has('reviews');
    }

    public function only_verified($value)
    {
        if ($value)
            $this->builder->verified('yes');
    }

    public function eyes($value)
    {
        if (!is_array($value) || !count($value))
            return;
        $this->builder->whereIn('eye_id', $value);
    }

    public function nationalities($value)
    {
        if (!is_array($value) || !count($value))
            return;
        $this->builder->whereIn('nationality_id', $value);
    }

    public function hairs($value)
    {
        if (!is_array($value) || !count($value))
            return;
        $this->builder->whereIn('hair_id', $value);
    }

    public function visas($value)
    {
        if (!is_array($value) || !count($value))
            return;
        $this->builder->whereHas('visas', function ($subQuery) use ($value) {
            $subQuery->whereIn('visa_id', $value);
        });
    }

}
