<?php

namespace App\Filters;

class OffersFilter extends BaseFilter
{

    public function query($value)
    {
        $this->builder->where('name', 'like', '%'.$value.'%');
    }

    public function status($value)
    {
        if(!is_array($value)) {
            $this->builder->where('offers.status', $value);
        } else {
            $this->builder->whereIn('offers.status', $value);
        }
    }

    public function userStatus($value) {
        if($value == 'viewed') {
            $this->builder->whereHas('views', function($q) {
                $q->where('users.id', \Auth::user()->id);
            });
        } elseif($value == 'not_viewed') {
            $this->builder->whereDoesntHave('views', function($q) {
                $q->where('users.id', \Auth::user()->id);
            });
        } elseif($value == 'applicated') {
            $this->builder->whereHas('applications', function($q) {
                $q->where('users.id', \Auth::user()->id);
            });
        }
    }

    public function users($value)
    {
        if(!is_array($value)) {
            $this->builder->where('offers.user_id', $value);
        } else {
            $this->builder->whereIn('offers.user_id', $value);
        }
    }

    public function country($value)
    {
        if(!is_array($value)) {
            $this->builder->where('offers.country_id', $value);
        } else {
            $this->builder->whereIn('offers.country_id', $value);
        }
    }

    public function city($value)
    {
        if(!is_array($value)) {
            $this->builder->where('offers.city_id', $value);
        } else {
            $this->builder->whereIn('offers.city_id', $value);
        }
    }

    public function category($value)
    {
        if(!is_array($value)) {
            $this->builder->where('offers.category_id', $value);
        } else {
            $this->builder->whereIn('offers.category_id', $value);
        }
    }

    public function sort($value)
    {
        if (!$this->hasParameter('order'))
            return;

        $order = $this->getParameter('order');
        if (!in_array($order, ['asc', 'desc']))
            return;

        $this->builder->orderBy($value, $order);
    }

}
