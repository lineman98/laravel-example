<?php

namespace App\Filters;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsersFilter extends BaseFilter
{

    public function query($value)
    {
        if (!$value || !strlen($value))
            return;
        $this->builder->where(function($subQuery) use ($value) {
            $this->builder->where('name', 'like', '%'.$value.'%')
                ->orWhere('phone', 'like', '%'.$value.'%')
                ->orWhere('whatsapp', 'like', '%'.$value.'%')
                ->orWhere('id', $value)
                ->orWhereHas('invite', function ($query) use ($value) {
                    $query->where('code', 'like', '%'.$value.'%');
                })
                ->orWhereHas('inviteAtRegistration', function ($query) use ($value) {
                    $query->where('code', 'like', '%'.$value.'%');
                });
        });
    }

    public function unchecked_media($value)
    {
        if (!$value)
            return;
        $this->builder->where(function($query) {
            $query->whereHas('uncheckedPhotos')
                ->orWhereHas('uncheckedVideos');
        });
    }

    public function model_statuses($value)
    {
        $this->builder->whereHas('modelStatuses', function ($query) use ($value) {
            $query->select(DB::raw('count(distinct status_id)'))
                ->whereIn('status_id', $value);
        }, '=', count($value));
    }

    public function has_no_model_statuses($value)
    {
        if (!$value)
            return;
        $this->builder->whereDoesntHave('modelStatuses');
    }

    public function has_no_manager_statuses($value)
    {
        if (!$value)
            return;
        $this->builder->whereDoesntHave('accessStatuses');
    }

    public function online($value)
    {
        if (!$value)
            return;
        $this->builder->where('online', 1);
    }

    public function roles($value)
    {
        if(is_array($value)) {
            $this->builder->whereIn('role', $value);
        } else {
            $this->builder->where('role', $value);
        }
    }

    public function type($value)
    {
        if(is_array($value)) {
            $this->builder->whereIn('type', $value);
        } else {
            $this->builder->where('type', $value);
        }
    }

    public function status($value)
    {
        if(is_array($value)) {
            $this->builder->whereIn('status', $value);
        } else {
            $this->builder->where('status', $value);
        }
    }

    public function verified($value)
    {
        $this->builder->where('verified', $value);
    }

    public function phone_verified($value)
    {
        $this->builder->where('phone_verified', $value);
    }

    public function subscription_payed($value)
    {
        $this->builder->where('subscription_payed', $value);
    }

    public function country($value)
    {
        if(is_array($value)) {
            $this->builder->whereIn('country_id', $value);
        } else {
            $this->builder->where('country_id', $value);
        }
    }

    public function city($value)
    {
        if(is_array($value)) {
            $this->builder->whereIn('city_id', $value);
        } else {
            $this->builder->where('city_id', $value);
        }
    }

    public function age_from($value)
    {
        $this->builder->where('age', '>=', $value);
    }

    public function age_to($value)
    {
        $this->builder->where('age', '<=', $value);
    }

    public function weight_from($value)
    {
        $this->builder->where('weight', '>=', $value);
    }

    public function weight_to($value)
    {
        $this->builder->where('weight', '<=', $value);
    }

    public function height_from($value)
    {
        $this->builder->where('height', '>=', $value);
    }

    public function height_to($value)
    {
        $this->builder->where('height', '<=', $value);
    }

    public function boobs_from($value)
    {
        $this->builder->where('boobs_size', '>=', $value);
    }

    public function boobs_to($value)
    {
        $this->builder->where('boobs_size', '<=', $value);
    }

    public function boobs_type($value)
    {
        if ($value === 'natural')
            $this->builder->where('boobs_type', 'natural');
    }

    public function only_with_reviews($value)
    {
        if ($value)
            $this->builder->has('reviews');
    }

    public function only_verified($value)
    {
        if ($value)
            $this->builder->verified('yes');
    }

    public function eyes($value)
    {
        $this->builder->whereIn('eye_id', $value);
    }

    public function nationalities($value)
    {
        $this->builder->whereIn('nationality_id', $value);
    }

    public function hairs($value)
    {
        $this->builder->whereIn('hair_id', $value);
    }

    public function visas($value)
    {
        $this->builder->whereHas('visas', function ($subQuery) use ($value) {
            $subQuery->whereIn('visa_id', $value);
        });
    }

    public function cost($value)
    {
        $this->builder->where(function ($subQuery) use ($value) {
            $subQuery->where('users.cost', null)
                ->orWhere('users.cost', '<=', $value);
        });
    }

    public function sort($value)
    {
        if (!$this->hasParameter('order'))
            return;

        $order = $this->getParameter('order');
        if (!in_array($order, ['asc', 'desc']))
            return;

        $this->builder->orderBy($value, $order);
    }

}
