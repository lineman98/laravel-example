<?php

namespace App\Http\Resources\Users;

use App\Entity\Users\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\MediaLibrary\Models\Media;

class UserResource extends JsonResource
{

    /** @var string|null */
    protected $token;

    public function __construct($resource, string $token=null)
    {
        parent::__construct($resource);
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $countryName = $this->country ? $this->country->name : null;
        $cityName = $this->city ? $this->city->name : null;
        $hairName = $this->hair ? $this->hair->name : null;
        $eyeName = $this->eye ? $this->eye->name : null;
        $nationalityName = $this->nationality ? $this->nationality->name : null;
        // есть ли этот пользователь в блеклисте у залогиненного пользователя
        $inBlackList = auth()->user() ? auth()->user()->hasInBlackList($this->resource) : false;
        // если ли залогиненный пользователь в блеклисте у запрашиваемого пользователя
        $inUsersBlackList = auth()->user() ? $this->resource->hasInBlackList(auth()->user()) : false;
        $isAdmin = auth()->user() && auth()->user()->isAdmin();
        $subscription = $this->getCurrentSubscription();
        $subscriptionEndsAt = $subscription ? $subscription->ends_at : null;
        // проверочная фотка
        $verifyPhoto = $isAdmin && $this->verifyPhoto ? new UserImageResource($this->verifyPhoto) : null;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'role' => $this->role,
            'email' => $this->email,
            'phone' => $this->phone,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'country' => $this->country_id,
            'countryName' => $countryName,
            'city' => $this->city_id,
            'cityName' => $cityName,
            'token' => $this->token,
            'photos' => UserImageResource::collection($this->getMedia(User::PUBLIC_PHOTOS)),
            'videos' => UserVideoResource::collection($this->getMedia(User::PUBLIC_VIDEOS)),
            'hair' => $this->hair_id,
            'hairName' => $hairName,
            'eye' => $this->eye_id,
            'eyeName' => $eyeName,
            'nationality' => $this->nationality_id,
            'nationalityName' => $nationalityName,
            'age' => (string) $this->age,
            'hip' => (string) $this->hip,
            'waist' => (string) $this->waist,
            'bust' => (string) $this->bust,
            'boobs_size' => (string) $this->boobs_size,
            'boobs_type' => $this->boobs_type,
            'weight' => (string) $this->weight,
            'height' => (string) $this->height,
            'avatar' => $this->avatar ? new UserImageResource($this->avatar) : null,
            'verified' => $this->verified,
            'phone_verified' => $this->phone_verified,
            'minimum_cost' => $this->minimum_cost,
            'whatsapp' => $this->whatsapp,
            'invite' => $this->invite ? $this->invite->code : null,
            'status_id' => $this->status_id,
            'model_statuses' => $this->modelStatuses->pluck('id'),
            'manager_statuses' => $this->accessStatuses->pluck('id'),
            'modelStatusesName' => $this->modelStatuses->pluck('name')->join(','),
            'managerStatusesName' => $this->accessStatuses->pluck('name')->join(','),
            'has_foreign_passport' => $this->has_foreign_passport,
            'visas' =>  $this->visas->pluck('id'),
            'visasName' => $this->visas->pluck('name')->join(','),
            'subscription_payed' => $this->subscription_payed,
            'subscription_ends_at' => $subscriptionEndsAt,
            'receive_messages_notifications' => $this->receive_messages_notifications,
            'receive_offers_notifications' => $this->receive_offers_notifications,
            'in_blacklist' => $inBlackList,
            'in_users_blacklist' => $inUsersBlackList,
            'referrer' => $isAdmin && $this->referrer ? new UserShortResource($this->referrer) : null,
            'invite_at_registration' => $isAdmin && $this->inviteAtRegistration ? $this->inviteAtRegistration->code : null,
            'referral_balance' => $this->referral_balance,
            'subscription_trial' => $this->subscription_trial,
            'unverified_reason' => $this->unverified_reason,
            'verifyPhoto' => $verifyPhoto,
            'last_seen' => $this->last_seen,
            'online' => $this->online,
            'can_create_offers' => $this->can_create_offers,
            'can_create_tours' => $this->can_create_tours,
            'gender' => $this->gender,
            'website' => $this->website,
            'aboutme' => $this->aboutme
            //'offer_categories' =>  $this->offerCategories->pluck('id'),
            //'offerCategoriesName' => $this->offerCategories->pluck('name')->join(','),
        ];
    }
}
