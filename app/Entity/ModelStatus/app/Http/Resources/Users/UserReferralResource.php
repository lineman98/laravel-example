<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UserReferralResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $phone = substr($this->phone, 0, 3) . '*****' . substr($this->phone, -2);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $phone,
            'created_at' => $this->created_at,
            'avatar' => $this->avatar ? new UserImageResource($this->avatar) : null,
        ];
    }

}
