<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;

class UserChatCached extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return Cache::tags('user_'.$this->id)
            ->remember('user_meta_'.$this->id, 60*60*120, function() {
                return [
                    'type' => $this->type,
                    'id' => $this->id,
                    'name' => $this->name,
                    'avatar' => $this->avatar ? new UserImageResource($this->avatar) : null
                ];
            });
    }
}
