<?php

namespace App\Http\Resources\Users;

use App\Entity\Users\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ManagerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // есть ли этот пользователь в блеклисте у залогиненного пользователя
        $inBlackList = auth()->user() ? auth()->user()->hasInBlackList($this->resource) : false;
        // если ли залогиненный пользователь в блеклисте у запрашиваемого пользователя
        $inUsersBlackList = auth()->user() ? $this->resource->hasInBlackList(auth()->user()) : false;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => $this->created_at,
            'photos' => UserImageResource::collection($this->getMedia(User::PUBLIC_PHOTOS)),
            'avatar' => $this->avatar ? new UserImageResource($this->avatar) : null,
            'verified' => $this->verified,
            'whatsapp' => !$inBlackList ? $this->whatsapp : null,
            'in_blacklist' => $inBlackList,
            'in_users_blacklist' => $inUsersBlackList,
            'last_seen' => $this->last_seen,
            'online' => $this->online,
            'gender' => $this->gender,
            'website' => $this->website,
        ];
    }
}
