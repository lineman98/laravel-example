<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UserVideoResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'thumb' => $this->getUrl('thumb'),
            'url' => $this->getUrl(),
            'checked' => isset($this->custom_properties['checked'])
                ? $this->custom_properties['checked']
                : false,
        ];
    }
}
