<?php

namespace App\Http\Resources\Offers;

use App\Http\Resources\Geo\CityResource;
use App\Http\Resources\Geo\CountryResource;
use App\Http\Resources\Users\UserShortResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Entity\Offers\OfferUser;
use App\Http\Resources\Messages\DialogResource;

class OfferShortResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category' => $this->category ? new OfferCategoryResource($this->category) : null,
            'city' => $this->city ? new CityResource($this->city) : null,
            'country' => $this->country ? new CountryResource($this->country) : null
        ];
    }
}
