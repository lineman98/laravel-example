<?php

namespace App\Http\Resources\Offers;

use App\Http\Resources\Geo\CityResource;
use App\Http\Resources\Geo\CountryResource;
use App\Http\Resources\Messages\DialogResource;
use App\Http\Resources\Users\UserShortResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Entity\Offers\OfferUser;
use Auth;

class OfferForModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->application && $this->application->status == OfferUser::STATUS_IGNORE) return [];
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category' => $this->category ? new OfferCategoryResource($this->category) : null,
            'city' => $this->city ? new CityResource($this->city) : null,
            'country' => $this->country ? new CountryResource($this->country) : null,
            'manager' => $this->user ? new UserShortResource($this->user) : null,
            'age_from' => $this->age_from,
            'age_to' => $this->age_to,
            'only_verified' => $this->only_verified,
            'offer_model_status' => $this->status,
            'application' =>  new OfferUserResource($this->application),
            'users_going' => $this->usersGoing ? UserShortResource::collection($this->usersGoing) : null,
            'users_requesting' => $this->usersRequesting && !empty(\Auth::user()) && $this->user_id == Auth::user()->id ? UserShortResource::collection($this->usersRequesting) : null,
            'dialog' => new DialogResource($this->dialog),
            'search_male' => $this->search_male,
            'search_female' => $this->search_female,
            'viewed' => !empty(\Auth::user()) ? $this->views()->where('users.id', \Auth::user()->id)->count() > 0 : null
        ];
    }
}
