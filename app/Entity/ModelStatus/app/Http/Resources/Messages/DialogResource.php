<?php

namespace App\Http\Resources\Messages;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Users\UserChatCached;

class DialogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            //'userFirst' => $this->userFirst ? $this->userFirst->getMetaInfo() : null,
            //'userSecond' => $this->userSecond ? $this->userSecond->getMetaInfo() : null,
            'lastMessage' => $this->lastMessage ? new MessageResource($this->lastMessage) : null,
            'last_message_time' => $this->last_message_time,
            'users' => $this->users,
            'name' => $this->name ? $this->name : null,
            'type' => $this->type,
            'offer_id' => $this->when($this->type == 'chat', $this->owner_id),
            'readed' => /*property_exists($this->pivot->readed, 'readed')*/  $this->pivot->readed ?? ''
        ];
    }
}
