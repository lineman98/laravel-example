<?php

namespace App\Http\Resources\MassMessages;

use Illuminate\Http\Resources\Json\JsonResource;

class MassMessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'status' => $this->status,
            'users_count' => $this->users_count,
            'created_at' => $this->created_at,
        ];
    }
}
