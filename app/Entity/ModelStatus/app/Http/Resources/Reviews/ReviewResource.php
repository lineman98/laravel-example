<?php

namespace App\Http\Resources\Reviews;

use App\Http\Resources\Users\UserShortResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => $this->userFrom ? new UserShortResource($this->userFrom) : null,
            'positive' => $this->positive,
            'content' => $this->content,
            'created_at' => $this->created_at,
        ];
    }
}
