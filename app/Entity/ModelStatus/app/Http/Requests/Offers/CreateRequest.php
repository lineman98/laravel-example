<?php

namespace App\Http\Requests\Offers;

use App\DTO\Offers\OfferCreateDto;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Entity\Offers\Category\OfferCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Offers
 *
 * @bodyParam name string required Name. Example: Best party
 * @bodyParam description string required Description(10-1000) Example: Lorem ipsum dolor sit amen...
 * @bodyParam country int required Id of country Example: 1
 * @bodyParam city int required Id of city Example: 1
 * @bodyParam category int required Id of category Example: 1
 * @bodyParam age_from int Minimal age Example: 23
 * @bodyParam age_to int Maximal age Example: 60
 * @bodyParam meeting_date date required Date (d.m.Y) Example: 30.12.2000
 * @bodyParam only_verified bool required Search only verivied (only verified can enable) Example: true
 * @bodyParam nationalities.* int Ids of nationalities Example: 1,2
 * @bodyParam eyes.* int Ids of eye types Example: 1
 * @bodyParam hairs.* int Ids of hairs types Example: 1
 * @bodyParam visas.* int Ids of visas Example: 1,2
 * @bodyParam country_from int Id of searching users country Example: 12
 * @bodyParam city_from int Id of searching users city Example: 24
 * @bodyParam search_from_same_city bool Search only from same city Example: true
 * @bodyParam nearest_city_range int {50,100,250,500} Example: 100
 */
class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:30',
            'description' => 'required|min:10|max:1000',
            'country' => 'required|exists:countries,id',
            'category' => 'required',
            'city' => [
                'required',
                Rule::exists('cities', 'id')
                    ->where('country_id', $this->get('country'))
            ],
            'age_from' => 'nullable|integer|min:18|max:60',
            'age_to' => 'nullable|integer|min:18|max:60',
            /*'weight_from' => 'nullable|integer|min:30|max:200',
            'weight_to' => 'nullable|integer|min:30|max:200',
            'height_from' => 'nullable|integer|min:150|max:250',
            'height_to' => 'nullable|integer|min:150|max:250',
            'boobs_from' => 'nullable|integer|min:1|max:10',
            'boobs_to' => 'nullable|integer|min:1|max:10',
            'boobs_type' => [
                'nullable',
                Rule::in(['all', 'natural'])
            ],*/
            'meeting_date' => 'required|date_format:d.m.Y',
            'only_verified' => 'required|boolean',
            'nationalities.*' => 'nullable|exists:nationalities,id',
            'eyes.*' => 'nullable|exists:eyes,id',
            'hairs.*' => 'nullable|exists:hairs,id',
            'visas.*' => 'nullable|exists:visas,id',
            'country_from' => 'nullable|exists:countries,id',
            'city_from' => 'nullable|exists:cities,id',
            'search_from_same_city' => 'nullable|boolean',
            "nearest_city_range" => ['nullable', Rule::in([50, 100, 250, 500])],
        ];
    }

    public function getDto(): OfferCreateDto
    {
        return new OfferCreateDto([
            'name' => $this->get('name'),
            'description' => $this->get('description'),
            'category' => OfferCategory::find($this->get('category')),
            'country' => Country::find($this->get('country')),
            'city' => City::find($this->get('city')),
            'age_from' => $this->get('age_from'),
            'age_to' => $this->get('age_to'),
            'weight_from' => $this->get('weight_from'),
            'weight_to' => $this->get('weight_to'),
            'height_from' => $this->get('height_from'),
            'height_to' => $this->get('height_to'),
            'boobs_from' => $this->get('boobs_from'),
            'boobs_to' => $this->get('boobs_to'),
            'boobs_type' => $this->get('boobs_type'),
            'cost' => $this->get('cost'),
            'meeting_date' => $this->get('meeting_date'),
            'search_from_same_city' => $this->get('search_from_same_city', false),
            'only_with_reviews' => $this->get('only_with_reviews'),
            'only_verified' => $this->get('only_verified'),
            'user' => $this->user(),
            'nationalities' => $this->get('nationalities'),
            'eyes' => $this->get('eyes'),
            'hairs' => $this->get('hairs'),
            'visas' => $this->get('visas'),
            'country_from' => $this->get('country_from') ? Country::find($this->get('country_from')) : null,
            'city_from' => $this->get('city_from') ? City::find($this->get('city_from')) : null,
            'nearest_city_range' => $this->get('nearest_city_range'),
            'genders' => $this->get('genders'),
        ]);
    }
}
