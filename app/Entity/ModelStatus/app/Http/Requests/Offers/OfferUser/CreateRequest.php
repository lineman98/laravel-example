<?php

namespace App\Http\Requests\Offers\OfferUser;

use App\Entity\Offers\OfferUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Offers\OfferUser
 *
 * @bodyParam message string Message(128) Example: lorem ipsum..
 * @bodyParam status string Status (sended, ignored) Example: ignored
 */
class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'nullable|max:128',
            'status' => [
                'nullable',
                Rule::in([OfferUser::STATUS_SENDED, OfferUser::STATUS_IGNORE])
            ]
        ];
    }
}
