<?php

namespace App\Http\Requests\Offers;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsersCountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country' => 'required|exists:countries,id',
            'category' => 'required|exists:offer_categories,id',
            'city' => [
                'required',
                Rule::exists('cities', 'id')
                    ->where('country_id', $this->get('country'))
            ],
            'age_from' => 'nullable|integer|min:18|max:60',
            'age_to' => 'nullable|integer|min:18|max:60',
            'weight_from' => 'nullable|integer|min:30|max:200',
            'weight_to' => 'nullable|integer|min:30|max:200',
            'height_from' => 'nullable|integer|min:150|max:250',
            'height_to' => 'nullable|integer|min:150|max:250',
            'boobs_from' => 'nullable|integer|min:1|max:10',
            'boobs_to' => 'nullable|integer|min:1|max:10',
            'boobs_type' => [
                'nullable',
                Rule::in(['all', 'natural'])
            ],
            'cost' => 'required|integer|min:100|max:1000000',
            'meeting_date' => 'required|date_format:d.m.Y',
            'possible_increase_cost' => 'required|boolean',
            'only_with_reviews' => 'required|boolean',
            'only_verified' => 'required|boolean',
            'nationalities.*' => 'nullable|exists:nationalities,id',
            'eyes.*' => 'nullable|exists:eyes,id',
            'hairs.*' => 'nullable|exists:hairs,id',
            'visas.*' => 'nullable|exists:visas,id',
            'country_from' => 'nullable|exists:countries,id',
            'city_from' => 'nullable|exists:cities,id',
            'search_from_same_city' => 'nullable|boolean',
            "nearest_city_range" => ['nullable', Rule::in([50, 100, 250, 500])],
        ];
    }

}
