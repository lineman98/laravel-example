<?php

namespace App\Http\Requests\Tours;

use App\DTO\Tours\TourUpdateDto;
use App\Entity\Geo\Country\Country;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|min:10|max:1000',
            'country' => 'required|exists:countries,id',
        ];
    }

    public function getDto(): TourUpdateDto
    {
        return new TourUpdateDto([
            'description' => $this->get('description'),
            'country' => Country::find($this->get('country')),
        ]);
    }
}
