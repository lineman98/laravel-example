<?php

namespace App\Http\Requests\Users;

use App\DTO\Users\PushTokenCreateDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AddPushTokenRequest
 * @package App\Http\Requests\Users
 *
 * @bodyParam token string required Example: EewbfbYGfyef7ew78fewfewYEfge
 * @bodyParam device string Example: phone
 * @bodyParam os string {ios, androind, web} Example: ios
 */
class AddPushTokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'device' => 'nullable',
            'os' => [
                'nullable',
                Rule::in(['ios', 'android', 'web']),
            ],
        ];
    }

    public function getDto(): PushTokenCreateDto
    {
        return new PushTokenCreateDto([
            'device' => $this->get('device'),
            'token' => $this->get('token'),
            'os' => $this->get('os', 'ios'),
        ]);
    }
}
