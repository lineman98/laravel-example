<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UploadPhotoRequest
 * @package App\Http\Requests\Users
 *
 * @bodyParam file video required Image for uploading
 */
class UploadVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => [
                'required',
                'file',
                'mimetypes:video/webm,video/mpeg,video/mp4,video/quicktime',
                'max:102400', // 100 MB max
            ],
        ];
    }
}
