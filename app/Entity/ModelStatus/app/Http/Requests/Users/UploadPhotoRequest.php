<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UploadPhotoRequest
 * @package App\Http\Requests\Users
 *
 * @bodyParam file image required Image for uploading
 */
class UploadPhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => [
                'required',
                'file',
                'mimetypes:application/octet-stream,image/heif,image/heic,image/HEIF,image/HEIF,image/heif-sequence,image/heic-sequence,image/jpeg',
                'max:20480', // 20 MB max
            ]
        ];
    }
}
