<?php

namespace App\Http\Requests\Messages;

use App\DTO\Messages\DialogMessageDto;
use App\Entity\Offers\OfferUser;
use App\Entity\Users\User;
use Illuminate\Foundation\Http\FormRequest;
use App\Entity\Messages\Dialog;

/**
 * Class CreateByDialogRequest
 * @package App\Http\Requests\Messages
 *
 * @bodyParam dialog_id int required Id of dialog. Example: 1
 * @bodyParam content string required Text of message. Example: Hello world
 */
class CreateByDialogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $dto = $this->getDto();
        $dialog = $dto->getDialog();

        if($dialog->type == 'chat') {
            return OfferUser::where('offer_id', $dialog->owner_id)
                ->where('status', OfferUser::STATUS_ACCEPT)
                ->where('user_id', \Auth::user()->id)->count() > 0;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dialog_id' => 'required|exists:dialogs,id',
            'content' => 'required|min:1|max:300'
        ];
    }

    public function getDto(): DialogMessageDto
    {
        return new DialogMessageDto([
            'user_from' => $this->user(),
            'dialog' => Dialog::findOrFail($this->get('dialog_id')),
            'content' => $this->get('content'),
        ]);
    }
}
