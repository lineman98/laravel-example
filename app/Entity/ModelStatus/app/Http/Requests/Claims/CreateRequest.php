<?php

namespace App\Http\Requests\Claims;

use App\DTO\Claims\ClaimCreateDto;
use App\Rules\CheckModelExists;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required|max:500',
            'claimable_type' => [
                'required',
                Rule::in(array_keys(config('relations.map_relations'))),
            ],
            'claimable_id' => [
                'required',
                new CheckModelExists($this->get('claimable_type'))
            ],
        ];
    }

    public function getDto(): ClaimCreateDto
    {
        $claimable_class = config('relations.map_relations')[$this->get('claimable_type')];

        return new ClaimCreateDto([
            'content' => $this->get('content'),
            'user' => $this->user(),
            'claimable' => app($claimable_class)->find($this->get('claimable_id')),
        ]);
    }
}
