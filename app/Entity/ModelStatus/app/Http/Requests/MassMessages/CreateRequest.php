<?php

namespace App\Http\Requests\MassMessages;

use App\DTO\MassMessages\MassMessageDto;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required',
            'filter' => 'nullable|json',
        ];
    }

    /**
     * @return MassMessageDto
     */
    public function getDto(): MassMessageDto
    {
        return new MassMessageDto([
            'content' => $this->get('content'),
            'filter' => $this->get('filter'),
        ]);
    }

}
