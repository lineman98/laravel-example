<?php

namespace App\Http\Controllers\Api;

use App\Entity\Users\User;
use App\Http\Controllers\Controller;
use App\Services\Users\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class IntercomController extends Controller
{

    /** @var UsersService */
    private $usersService;

    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    public function initialize(Request $request)
    {
        $data = $request->input('customer');

        if (!isset($data['user_id']) || !$data['user_id']) {
            return response()->json([
                'canvas' => [
                    'content' => [
                        'components' => [
                            [
                                "type" => "text",
                                "text" => 'Не зарегистрирован в приложении',
                                "style" => "header",
                                "align" => "center",
                            ],
                            [
                                "type" => "text",
                                "text" => '*Список инвайтов:*',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*YES* - laravel-example (и ВК)',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*STAR* - инстаграм',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*SUPER* - рассылка телега (рг, срм)',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*BABY* - рассылка телега (ФИМП)',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*TELEGA* - телега Слава',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*WHATS* - вотсап Слава',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*BEST* - рассылка вотсап',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*MEN* - менеджер 30 дней доступа',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*GRAND* - менеджер 30 дней доступа',
                                "align" => "left",
                            ],
                        ]
                    ]
                ]
            ]);
        }

        $user_id = $data['user_id'];
        $user = is_numeric($user_id) ? User::find($user_id) : null;

        if (!$user) {
            return response()->json([
                'canvas' => [
                    'content' => [
                        'components' => [
                            [
                                "type" => "text",
                                "text" => 'Пользователь не найден',
                                "style" => "header",
                                "align" => "center",
                            ],
                            [
                                "type" => "text",
                                "text" => '*Список инвайтов:*',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*YES* - laravel-example (и ВК)',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*STAR* - инстаграм',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*SUPER* - рассылка телега (рг, срм)',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*BABY* - рассылка телега (ФИМП)',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*TELEGA* - телега Слава',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*WHATS* - вотсап Слава',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*BEST* - рассылка вотсап',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*MEN* - менеджер 30 дней доступа',
                                "align" => "left",
                            ],
                            [
                                "type" => "text",
                                "text" => '*GRAND* - менеджер 30 дней доступа',
                                "align" => "left",
                            ],
                        ]
                    ]
                ]
            ]);
        }

        return response()->json([
            'canvas' => [
                'content' => [
                    'components' => [
                        [
                            "type" => "text",
                            "text" => $user->name,
                            "style" => "header",
                            "align" => "center",
                        ], [
                            "type" => "text",
                            "text" => '*Тип аккаунта:* ' . 'Пользователь',
                            "align" => "left",
                        ], [
                            "type" => "text",
                            "text" => '*Телефон:* '.$user->phone,
                            "align" => "left",
                        ], [
                            "type" => "text",
                            "text" => '*Зарегистрирован:* '.$user->created_at->format('d-m-Y'),
                            "align" => "left",
                        ], [
                            "type" => "text",
                            "text" => '*Проверка:* '. ($user->isWaitVerify() ? 'ожидает проверку' : ($user->isVerified() ? 'проверен': 'не проверен')),
                            "align" => "left",
                        ], [
                            "type" => "text",
                            "text" => '*Анкета:* '. ($user->isActive() ? 'заполнена' : 'не заполнена'),
                            "align" => "left",
                        ], [
                            'type' => 'button',
                            'label' => 'Открыть в админке',
                            'style' => 'primary',
                            'id' => 'url_button',
                            'action' => ['type' => 'url', 'url' => "http://awwadmin.rusgirls.vip/#/users/".$user_id]
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function webhook(Request $request)
    {
        return 'good';
    }

}
