<?php

namespace App\Http\Controllers\Api\Users;

use App\Entity\Users\User;
use App\Http\Requests\Users\AddPushTokenRequest;
use App\Http\Requests\Users\CheckMediaRequest;
use App\Http\Requests\Users\DeleteRequest;
use App\Http\Requests\Users\RemovePhotoRequest;
use App\Http\Requests\Users\SetAvatarRequest;
use App\Http\Requests\Users\SetUnverifiedRequest;
use App\Http\Requests\Users\UpdateRequest;
use App\Http\Requests\Users\UploadPhotoRequest;
use App\Http\Requests\Users\UploadVerifyPhotoRequest;
use App\Http\Requests\Users\UploadVideoRequest;
use App\Http\Resources\Reviews\ReviewResource;
use App\Http\Resources\Users\GirlLinkResource;
use App\Http\Resources\Users\UserFullResource;
use App\Http\Resources\Users\GirlResource;
use App\Http\Resources\Users\ManagerResource;
use App\Http\Resources\Users\UserImageResource;
use App\Http\Resources\Users\UserResource;
use App\Http\Resources\Users\UserVideoResource;
use App\Services\Reviews\ReviewsService;
use App\Services\Users\GirlLinksService;
use App\Services\Users\UsersService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @group Users
 * Class UsersController
 * @package App\Http\Controllers\Api\Users
 */
class UsersController extends Controller
{

    /** @var UsersService */
    private $userService;

    /** @var ReviewsService */
    private $reviewsService;

    /** @var GirlLinksService */
    private $linksService;

    public function __construct(
        UsersService $userService,
        ReviewsService $reviewsService,
        GirlLinksService $linksService
    ) {
        $this->userService = $userService;
        $this->reviewsService = $reviewsService;
        $this->linksService = $linksService;
    }

    /**
     * Get all users (For admins)
     *
     * @queryParam count int Count of users
     *
     * @apiResourceCollection App\Http\Resources\Users\UserResource
     * @apiResourceModel App\Entity\Users\User
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $this->middleware('admin');
        $users = $this->userService->paginate(
            $request->get('count', 20),
            $request->all()
        );
        return UserResource::collection($users);
    }

    /**
     * Create user (dont works)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Show user
     *
     * @urlParam id int Id of user. Example: 1
     *
     * @apiResource App\Http\Resources\Users\UserFullResource
     * @apiResourceModel App\Entity\Users\User
     * @return UserFullResource
     */
    public function show(Request $request, $id)
    {
        $user = $this->userService->findUserFromUser(
            $id,
            $request->user()
        );
        return new UserFullResource($user);
    }


    /**
     * Update user
     *
     * @urlParam user int User for update. Example: 1
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function update(UpdateRequest $request, User $user)
    {
        $this->authorize('edit-user', $user);
        $result = $this->userService->update($user, $request->getDto());
        return response()->json(['success' => $result]);
    }

    /**
     * Upload Photo
     *
     * @urlParam id int Id of user
     *
     * @apiResource App\Http\Resources\Users\UserImageResource
     * @apiResourceModel Spatie\MediaLibrary\Models\Media
     * @return UserImageResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function uploadPhoto(UploadPhotoRequest $request, $id)
    {
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $media = $this->userService->uploadPhoto($request->file('file'), $user);
        return new UserImageResource($media);
    }

    /**
     * Upload verify photo
     *
     * @urlParam id int Id of user. Example: 1
     *
     * @apiResource App\Http\Resources\Users\UserImageResource
     * @apiResourceModel Spatie\MediaLibrary\Models\Media
     * @return UserImageResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function uploadVerifyPhoto(UploadVerifyPhotoRequest $request, $id)
    {
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $media = $this->userService->uploadVerifyPhoto($request->file('file'), $user);
        return new UserImageResource($media);
    }

    /**
     * Upload video
     *
     * @urlParam id int Id of user. Example: 1
     *
     * @apiResource App\Http\Resources\Users\UserVideoResource
     * @apiResourceModel Spatie\MediaLibrary\Models\Media
     * @return UserVideoResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function uploadVideo(UploadVideoRequest $request, $id)
    {
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $media = $this->userService->uploadVideo($request->file('file'), $user);
        return new UserVideoResource($media);
    }

    /**
     * Remove photo
     *
     * @urlParam id int Id of user. Example: 1
     *
     * @response {
     *   "success": "true"
     * }
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function removePhoto(RemovePhotoRequest $request, $id)
    {
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $result = $this->userService->removePhoto($user, $request->get('id'));
        return ['success' => $result];
    }

    /**
     * Check media (for Admins)
     *
     * @urlParam id int Id of user. Example: 1
     *
     * @response {
     *   "success": "true"
     * }
     * @param CheckMediaRequest $request
     * @return array
     */
    public function checkMedia(CheckMediaRequest $request, $id)
    {
        $this->middleware('admin');
        $user = $this->userService->find($id);
        $result = $this->userService->checkMedia($user, $request->get('id'));
        return ['success' => $result];
    }

    /**
     * Set avatar
     *
     * @urlParam id int Id of user
     *
     * @param SetAvatarRequest $request
     *
     * @response {
     *   "success": "true"
     * }
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function setAvatar(SetAvatarRequest $request, $id)
    {
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $result = $this->userService->setAvatar($user, $request->get('id'));
        return ['success' => $result];
    }

    /**
     * Add push token
     *
     * @urlParam id int Id of user
     *
     * @param AddPushTokenRequest $request
     * @response {
     *   "success": "true"
     * }
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function addPushToken(AddPushTokenRequest $request, $id)
    {
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $this->userService->addPushToken($user, $request->getDto());
        return ['success' => true];
    }

    /**
     * Remove push token
     *
     * @urlParam id int Id of user
     *
     * @param AddPushTokenRequest $request
     * @response {
     *   "success": "true"
     * }
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function removePushToken(AddPushTokenRequest $request, $id)
    {
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $result = $this->userService->removePushToken($user, $request->getDto());
        return ['success' => $result];
    }

    /**
     * Set user virified (for admins)
     *
     * @urlParam id int Id of user
     *
     * @response {
     *   "success": "true"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function setVerified(Request $request, $id)
    {
        $this->middleware('admin');
        $user = $this->userService->find($id);
        $success = $this->userService->setVerified($user);
        return response()->json(compact('success'));
    }

    /**
     * Set user unvirified (for admins)
     *
     * @urlParam id int Id of user
     *
     * @response {
     *   "success": "true"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function setUnverified(SetUnverifiedRequest $request, $id)
    {
        $this->middleware('admin');
        $user = $this->userService->find($id);
        $success = $this->userService->setUnverified($user, $request->get('reason'));
        return response()->json(compact('success'));
    }

    /**
     * Send verify request
     *
     * @urlParam id int Id of user
     *
     * @response {
     *   "success": "true"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function sendVerifyRequest(Request $request, $id)
    {
        $this->middleware('manager');
        $user = $this->userService->find($id);
        $this->authorize('edit-user', $user);
        $this->userService->setManagerWaitVerify($user);
        return response()->json(['success' => true]);
    }

    /**
     * Block user (for admins)
     *
     * @urlParam id int Id of user for blocking
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setBlock($id)
    {
        $this->middleware('admin');
        $user = $this->userService->find($id);
        $this->userService->setBlocked($user);
        return response()->json(['success' => true]);
    }

    /**
     * Set virified phone (for admins)
     *
     * @urlParam id int Id of user. Example: 1
     *
     * @response {
     *   "success": "true"
     * }
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function setVerifiedPhone($id)
    {
        $this->middleware('admin');
        $user = $this->userService->find($id);
        $success = $this->userService->setVerifiedPhone($user);
        return response()->json(compact('success'));
    }

    /**
     * Выдача доступа на месяц
     *
     * @urlParam id int Id of user.  Example: 1
     *
     * @response {
     *   "success": "true"
     * }
     * @param Request $request
     * @return array
     */
    public function extendSubscription(Request $request, $id)
    {
        $this->middleware('admin');
        $user = $this->userService->find($id);

        $currentUserSubscription = $user->getCurrentSubscription();
        // обновляем тарифный план
        $currentUserSubscription->renew();

        $user->subscription_payed = true;
        $user->save();

        return ['success' => true];
    }

    /**
     * Get user reviews
     *
     * @urlParam id int Id of user
     *
     * @apiResourceCollection \App\Http\Resources\Reviews\ReviewResource
     * @apiResourceModel \App\Entity\Reviews\Review
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function reviews($id)
    {
        $reviews = $this->reviewsService->getReviewsForUser($id);
        return ReviewResource::collection($reviews);
    }

    /**
     * Get user short link
     *
     * @urlParam id int Id of user
     *
     * @apiResource \App\Http\Resources\Users\GirlLinkResource
     * @apiResourceModel \App\Entity\Users\GirlLink
     * @return GirlLinkResource
     * @throws \Throwable
     */
    public function link($id, Request $request)
    {
        $girl = $this->userService->find($id);
        $link = $this->linksService->createLinkToGirl($girl, $request->user());
        return new GirlLinkResource($link);
    }

    /**
     * Delete user
     *
     * @urlParam user int Id of user.  Example: 1
     *
     * @param DeleteRequest $request
     *
     * @response {
     *   "success": "true"
     * }
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function destroy(DeleteRequest $request, User $user)
    {
        $this->authorize('delete-user', $user);
        $result = $this->userService->delete($user, $request->get('reason'));
        return response()->json(compact('result'));
    }

}
