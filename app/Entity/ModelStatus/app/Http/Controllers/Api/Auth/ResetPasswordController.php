<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\Auth\RestorePasswordCodeRequest;
use App\Http\Requests\Auth\RestorePasswordRequest;
use App\Http\Requests\Verification\SendVerifyRequest;
use App\Services\Users\UsersService;
use App\Services\Verification\Exceptions\ExpiredException;
use App\Services\Verification\Exceptions\LimitException;
use App\Services\Verification\Exceptions\VerificationException;
use App\Services\Verification\PhoneVerification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @group Auth
 * Class ResetPasswordController
 * @package App\Http\Controllers\Api\Auth
 */
class ResetPasswordController extends Controller
{

    /** @var PhoneVerification */
    private $phoneVerification;

    /** @var UsersService */
    private $userService;

    public function __construct(UsersService $userService, PhoneVerification $phoneVerification)
    {
        $this->phoneVerification = $phoneVerification;
        $this->userService = $userService;
    }

    /**
     * Send verify sms code to users phone
     * @param Request $request
     *
     * @response {
     *   "success": true
     * }
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(RestorePasswordRequest $request)
    {
        $user = $this->userService->findByPhone($request->get('phone'));
        $result = $this->phoneVerification->sendSms($user, 'restore_password');
        return response()->json(['success' => $result]);
    }

    /**
     * Check verify code
     * @param SendVerifyRequest $request
     *
     * @response {
     *   "success": true
     * }
     * @response {
     *   "success": "false",
     *   "error": "Error message"
     * }
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(RestorePasswordCodeRequest $request)
    {
        $user = $this->userService->findByPhone($request->get('phone'));
        try {
            $result = $this->phoneVerification->verify(
                $user,
                $request->get('code'),
                'restore_password',
                true
            );
            // если успешное подтверждение, то меняем пароль
            if($result) {
                $this->userService->changePassword(
                    $user,
                    $request->get('new_password')
                );
            }
            return response()->json(['success' => $result]);
        } catch (VerificationException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        } catch (ExpiredException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        } catch (LimitException $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }

}
