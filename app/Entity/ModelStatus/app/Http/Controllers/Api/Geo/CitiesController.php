<?php

namespace App\Http\Controllers\Api\Geo;

use App\Http\Requests\Cities\CreateRequest;
use App\Http\Requests\Cities\IndexRequest;
use App\Http\Requests\Cities\UpdateRequest;
use App\Http\Resources\Geo\CityResource;
use App\Services\Geo\CityService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @group Geo
 * Class CitiesController
 * @package App\Http\Controllers\Api\Geo
 */
class CitiesController extends Controller
{

    protected $service;

    public function __construct(CityService $service)
    {
        $this->service = $service;
    }

    /**
     * Get cities
     *
     * @queryParam country_id int Id of country. Example: 1
     * @param  \Illuminate\Http\Request $request
     *
     * @apiResource \App\Http\Resources\Geo\CityResource
     * @apiResourceModel \App\Entity\Geo\City\City
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
		if($request->has('country_id') && !$request->has('search')) {
            $cities = $this->service->getCitiesForCountry($request->country_id);
        }
		else if($request->has('search')) {
		    $cities = $this->service->searchCities($request->all());
		} else {
			$cities = $this->service->getCities();
		}

        return CityResource::collection($cities);
    }

    /**
     * Add city
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @apiResource \App\Http\Resources\Geo\CityResource
     * @apiResourceModel \App\Entity\Geo\City\City
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        return new CityResource($this->service->createFromRequest($request));
    }

    /**
     * Show city
     *
     * @urlParam id required Id of city. Example: 1
     *
     * @apiResource \App\Http\Resources\Geo\CityResource
     * @apiResourceModel \App\Entity\Geo\City\City
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new CityResource($this->service->find($id));
    }

    /**
     * Update city
     *
     * @param  \Illuminate\Http\Request  $request
     * @urlParam id required Id of city. Example: 1
     *
     * @response {
     *   "status": "true"
     * }
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return response()->json([
            'status' => $this->service->updateFromRequest($request, $id)
        ]);
    }

    /**
     * Remove city (dont works)
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get most popular cities
     *
     * @queryParam count int Count of cities (max 20) Example: 15
     *
     * @apiResourceCollection App\Http\Resources\Geo\CityResource
     * @apiResourceModel App\Entity\Geo\City\City
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getTop(Request $request) {
        $top = $this->service->getTopByOffers([
            'count' => max($request->count, 20) ?? 20
        ]);
        return CityResource::collection($top);
    }
}
