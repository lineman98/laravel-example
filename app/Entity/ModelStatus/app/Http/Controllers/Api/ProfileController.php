<?php

namespace App\Http\Controllers\Api;

use App\Entity\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\AddToBlackListRequest;
use App\Http\Requests\Profile\ChangePasswordRequest;
use App\Http\Requests\Profile\GetBillRequest;
use App\Http\Requests\Profile\NotificationsChangeRequest;
use App\Http\Requests\Profile\RemoveFromBlackListRequest;
use App\Http\Resources\Bills\BillResource;
use App\Services\Bills\BillService;
use App\Services\BlackList\BlackListService;
use Illuminate\Http\Request;

/**
 * @group Users
 * Class ProfileController
 * @package App\Http\Controllers\Api
 */
class ProfileController extends Controller
{

    /** @var BillService */
    private $billService;
    /** @var BlackListService */
    private $blackListService;

    public function __construct(BillService $billService, BlackListService $blackListService)
    {
        $this->billService = $billService;
        $this->blackListService = $blackListService;
    }

    /**
     * @group Users
     * Change password
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $request->user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return response()->json(['success' => true]);
    }

    /**
     * Change notifications
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @param NotificationsChangeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function notificationsChange(NotificationsChangeRequest $request)
    {
        $user = $request->user();
        $user->receive_offers_notifications = $request->get('receive_offers_notifications');
        $user->receive_messages_notifications = $request->get('receive_messages_notifications');
        $user->save();
        return response()->json(['success' => true]);
    }

    /**
     * Get subscription bill
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getSubscriptionBill(Request $request)
    {
        $user = $request->user();
        $subscription = $user->getCurrentSubscription();
        // если нет плана, то проставляем
        if (!$subscription) {
            if ($user->isGirl()) {
                $plan = app('rinvex.subscriptions.plan')
                    ->find(config('plans.model_plan_id'));
            } else {
                $plan = app('rinvex.subscriptions.plan')
                    ->find(config('plans.manager_plan_id'));
            }
            $subscription = $user->newSubscription('main', $plan);
            // сразу завершаем подписку (это костыль)
            $subscription->ends_at = now();
            $subscription->save();
            $user->subscription_slug = $subscription->slug;
            $user->save();
        } else {
            $plan = $subscription->plan;
        }

        $bill = $this->billService->createPlanBill($user, $plan);
        $payment_url = $this->billService->generateUrlForPay($bill);
        return response()->json([
            'bill' => new BillResource($bill),
            'plan' => $plan,
            'payment_url' => $payment_url
        ]);
    }

    /**
     * Get bill
     *
     * @apiResource \App\Http\Resources\Bills\BillResource
     * @apiResourceModel App\Entity\Bill\Bill
     * @param GetBillRequest $request
     * @return BillResource
     */
    public function getBill(GetBillRequest $request)
    {
        $bill = $this->billService->findBill($request->id);
        return new BillResource($bill);
    }

    /**
     * @group Users
     * Add to black list
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @param AddToBlackListRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addToBlackList(AddToBlackListRequest $request)
    {
        $this->blackListService->addToBlackList($request->user(), User::find($request->id));
        return response()->json(['success' => true]);
    }

    /**
     * @group Users
     * Remove from black list
     *
     * @response {
     *   "success": "true"
     * }
     *
     * @param RemoveFromBlackListRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeFromBlackList(RemoveFromBlackListRequest $request)
    {
        $this->blackListService->removeFromBlackList($request->user(), User::find($request->id));
        return response()->json(['success' => true]);
    }

}
