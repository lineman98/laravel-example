<?php

namespace App\Http\Controllers\Api\Offers;

use App\Entity\Offers\Category\OfferCategory;
use App\Http\Resources\Offers\OfferCategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class CategoriesController extends Controller
{
    /**
     *
     * Get offers categories
     *
     * @group Options
     *
     * @apiResourceCollection App\Http\Resources\Offers\OfferCategoryResource
     * @apiResourceModel App\Entity\Offers\Category\OfferCategory
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Cache::tags(OfferCategory::class)->rememberForever('offer_categories', function () {
           return OfferCategory::with('translations')->get();
        });
        return OfferCategoryResource::collection($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
