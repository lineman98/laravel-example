<?php

namespace App\Http\Controllers\Api\Offers;

use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferUser;
use App\Http\Requests\Offers\CreateRequest;
use App\Http\Requests\Offers\OfferBlockRequest;
use App\Http\Requests\Offers\UpdateRequest;
use App\Http\Resources\Bills\BillResource;
use App\Http\Resources\Offers\OfferForModelResource;
use App\Http\Resources\Offers\OfferFullResource;
use App\Http\Resources\Offers\OfferResource;
use App\Services\Bills\BillService;
use App\Services\Offers\OffersService;
use App\Services\Offers\OfferDialogService;
use App\Services\Offers\OfferUserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @group Offers
 * Class OffersController
 * @package App\Http\Controllers\Api\Offers
 */
class OffersController extends Controller
{

    /** @var OffersService */
    private $offersService;

    /** @var BillService */
    private $billService;

    public function __construct(OffersService $offersService, BillService $billService, OfferUserService $offerUserService, OfferDialogService $offerDialogService)
    {
        $this->offersService = $offersService;
        $this->offerUserService = $offerUserService;
        $this->billService = $billService;
        $this->offerDialogService = $offerDialogService;
    }

    /**
     * All offers (for admin)
     *
     * @queryParam count int Count of offers, Default 20. Example: 30
     *
     * @apiResourceCollection \App\Http\Resources\Offers\OfferResource
     * @apiResourceModel \App\Entity\Offers\Offer
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function indexAll(Request $request)
    {
        $this->middleware('admin');
        $offers = $this->offersService->paginate(
            $request->get('count', 20),
            $request->all()
        );
        return OfferResource::collection($offers);
    }

    /**
     * Get offers
     *
     * @queryParam city_id int Id of city. Example: 1
     * @queryParam category int Id of category. Example: 1
     * @queryParam userStatus string User status for offer {viewed, not_viewed, applicated}. Example: not_viewed
     *
     * @apiResourceCollection \App\Http\Resources\Offers\OfferForModelResource
     * @apiResourceModel \App\Entity\Offers\Offer
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $offers = $this->offersService->paginateOffersForModel($request->user(), [
            'city' => $request->get('city_id'),
            'category' => $request->get('category_id'),
            'userStatus' => $request->get('user_status')
        ]);
        return OfferForModelResource::collection($offers);
    }

    /**
     * Get own offers
     *
     * @apiResourceCollection App\Http\Resources\Offers\OfferResource
     * @apiResourceModel \App\Entity\Offers\Offer
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function my(Request $request)
    {
        $offers = $this->offersService->paginateOwnOffers($request->user());
        return OfferResource::collection($offers);
    }

    /**
     * Create offer
     *
     * @apiResource \App\Http\Resources\Offers\OfferFullResource
     * @apiResourceModel \App\Entity\Offers\Offer
     * @param CreateRequest $request
     * @return OfferFullResource
     * @throws \Throwable
     */
    public function store(CreateRequest $request)
    {
        $this->middleware('subscription_active');
        $offer = $this->offersService->create($request->getDto());
        $this->offerDialogService->createOfferDialog($offer);

        $offer_user = $this->offerUserService->create(
            $request->user()->id,
            $offer->id,
            '',
            OfferUser::STATUS_ACCEPT
        );
        $offer->usersGoing[0] = $offer_user;

        return new OfferFullResource($offer);
    }

    /**
     * Set viewed offer
     *
     * @urlParam offer int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param Request $request
     * @param Offer $offer
     * @return \Illuminate\Http\JsonResponse
     */
    public function setViewed(Request $request, Offer $offer) {
        $result = $this->offersService->setViewed($offer, $request->user());
        return response()->json(compact('result'));
    }

    /**
     * Show offer (for admins)
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @apiResource \App\Http\Resources\Offers\OfferFullResource
     * @apiResourceModel \App\Entity\Offers\Offer
     * @param Request $request
     * @return OfferFullResource
     */
    public function show(Request $request, $id)
    {
        $offer = $this->offersService->fetchOfferForManager($request->user(), $id);
        return new OfferFullResource($offer);
    }

    /**
     * Show offer
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @apiResource \App\Http\Resources\Offers\OfferForModelResource
     * @apiResourceModel \App\Entity\Offers\Offer
     * @param Request $request
     * @return OfferFullResource
     */
    public function showForModel(Request $request, $id)
    {
        $this->middleware('subscription_active');
        $offer = $this->offersService->fetchOfferForModel(
            $request->user(),
            $id
        );
        return new OfferForModelResource($offer);
    }

    /**
     * Update offer
     *
     * @urlParam offer int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param UpdateRequest $request
     * @param Offer $offer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateRequest $request, Offer $offer)
    {
        //$this->middleware('subscription_active');
        $this->authorize('edit-offer', $offer);
        $result = $this->offersService->update($offer, $request->getDto());
        return response()->json(compact('result'));
    }

    /**
     * Activate offer
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function activate($id)
    {
        $this->middleware('admin');
        $offer = $this->offersService->find($id);
        $result = $this->offersService->setActive($offer);
        return response()->json(compact('result'));
    }

    /**
     * End offer
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function setEnded($id)
    {
        $offer = $this->offersService->find($id);
        $this->authorize('edit-offer', $offer);
        $result = $this->offersService->setEnded($offer);
        return response()->json(compact('result'));
    }

    /**
     * Block offer
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param OfferBlockRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function block(OfferBlockRequest $request, $id)
    {
        $this->middleware('admin');
        $offer = $this->offersService->find($id);
        $result = $this->offersService->setBlocked($offer, $request->get('reason'));
        return response()->json(compact('result'));
    }

    /**
     * Close offer
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function close($id)
    {
        $offer = $this->offersService->find($id);
        $this->authorize('edit-offer', $offer);
        $result = $this->offersService->close($offer);

        return response()->json(compact('result'));
    }

    /**
     * End offer (offer happend)
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function end($id)
    {
        $offer = $this->offersService->find($id);
        $this->authorize('edit-offer', $offer);
        $result = $this->offersService->end($offer);
        return response()->json(compact('result'));
    }

    /**
     * Получить счет на оплату публикации
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getBill(Request $request, $id)
    {
        $user = $request->user();
        $offer = $this->offersService->fetchOfferForManager($user, $id);
        $bill = $this->billService->createOfferPublicationBill($user, $offer);
        $payment_url = $this->billService->generateUrlForPay($bill);
        return response()->json([
            'bill' => new BillResource($bill),
            'payment_url' => $payment_url
        ]);
    }

    /**
     * Delete offer
     *
     * @urlParam id int Id of offer Example: 1
     *
     * @response {
     *   "result": "true"
     * }
     * @param Offer $offer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Offer $offer)
    {
        $this->authorize('edit-offer', $offer);
        $result = $this->offersService->delete($offer);
        return response()->json(compact('result'));
    }
}
