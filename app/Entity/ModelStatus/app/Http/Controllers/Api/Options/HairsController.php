<?php

namespace App\Http\Controllers\Api\Options;

use App\Entity\Options\Hair\Hair;
use App\Http\Resources\Options\HairResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

/**
 * @group Options
 * Class HairsController
 * @package App\Http\Controllers\Api\Options
 */
class HairsController extends Controller
{
    /**
     * Get hairs
     *
     * @apiResourceCollection App\Http\Resources\Options\HairResource
     * @apiResourceModel App\Entity\Options\Hair\Hair
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hairs = Cache::tags(Hair::class)->rememberForever('all_hairs', function () {
            return Hair::with('translations')->get();
        });
        return HairResource::collection($hairs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
