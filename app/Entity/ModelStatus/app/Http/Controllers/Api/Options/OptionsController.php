<?php

namespace App\Http\Controllers\Api\Options;

use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Options\Eye\Eye;
use App\Entity\Options\Hair\Hair;
use App\Entity\Options\Nationality\Nationality;
use App\Entity\Options\Purpose\Purpose;
use App\Entity\Options\Visa\Visa;
use App\Http\Resources\Options\EyeResource;
use App\Http\Resources\Options\HairResource;
use App\Http\Resources\Options\ModelStatusResource;
use App\Http\Resources\Options\NationalityResource;
use App\Http\Resources\Options\PurposeResource;
use App\Http\Resources\Options\VisaResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

/**
 * @group Options
 * Class OptionsController
 * @package App\Http\Controllers\Api\Options
 */
class OptionsController extends Controller
{

    /**
     * Get main site options (eyes, hairs, nationalities, etc.)
     *
     *
     * @return mixed
     */
    public function index()
    {
        $getAllOptions = function () {
            $eyes = Cache::tags(Eye::class)->rememberForever('all_eyes', function () {
                return Eye::with('translations')->get();
            });
            $data['eyes'] = EyeResource::collection($eyes);

            $hairs = Cache::tags(Hair::class)->rememberForever('all_hairs', function () {
                return Hair::with('translations')->get();
            });
            $data['hairs'] = HairResource::collection($hairs);

            $nationalities = Cache::tags(Nationality::class)->rememberForever('all_nationalities', function () {
                return Nationality::with('translations')->get();
            });
            $data['nationalities'] = NationalityResource::collection($nationalities);

            $purposes = Cache::tags(Purpose::class)->rememberForever('all_purposes', function () {
                return Purpose::with('translations')->get();
            });
            $data['purposes'] = PurposeResource::collection($purposes);

            $purposes = Cache::tags(ModelStatus::class)->rememberForever('all_statuses', function () {
                return ModelStatus::all();
            });
            $data['statuses'] = ModelStatusResource::collection($purposes);

            $visas = Cache::tags(Visa::class)->rememberForever('all_visas', function () {
                return Visa::all();
            });
            $data['visas'] = VisaResource::collection($visas);

            return response()->json($data);
        };

        $options = Cache::remember('all_options_'.App::getLocale(), 60, $getAllOptions);
        return $options;
    }

}
