<?php

namespace App\Http\Controllers\Api\Links;

use App\Http\Controllers\Controller;
use App\Services\Users\GirlLinksService;

class LinksController extends Controller
{

    /** @var GirlLinksService */
    private $linksService;

    public function __construct(GirlLinksService $linksService)
    {
        $this->linksService = $linksService;
    }

    public function __invoke(string $alias)
    {
        try {
            $girl = $this->linksService->getGirlByLinkAlias($alias);
            return response()->json([
                'success' => true,
                'girl_id' => $girl->id,
            ]);
        } catch (\Throwable $exception) {
            return response()->json([
                'success' => false,
            ]);
        }
    }

}
