<?php

namespace App\Http\Controllers\Api\Messages;

use App\Entity\Messages\Message;
use App\Entity\Users\User;
use App\Http\Requests\Messages\CreateAdminRequest;
use App\Http\Requests\Messages\CreateRequest;
use App\Http\Requests\Messages\ListDialogAdminRequest;
use App\Http\Requests\Messages\ListDialogRequest;
use App\Http\Requests\Messages\MarkAsReadAdminRequest;
use App\Http\Requests\Messages\MarkAsReadRequest;
use App\Http\Resources\Messages\DialogResource;
use App\Http\Resources\Messages\MessageResource;
use App\Services\Messages\DialogService;
use App\Services\Messages\MessageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class MessagesAdminController extends Controller
{

    /** @var DialogService */
    private $dialogService;

    /** @var MessageService */
    private $messageService;

    /** @var User */
    private $adminUser;

    public function __construct(DialogService $dialogService, MessageService $messageService)
    {
        $this->dialogService = $dialogService;
        $this->messageService = $messageService;
        // получаем админа
        $this->adminUser = Cache::rememberForever('admin_support_user', function () {
           return User::findOrFail(config('support.admin_support_id'));
        });
    }

    public function index(Request $request)
    {
        $dialogs = $this->dialogService->getUserDialogs($this->adminUser);
        return DialogResource::collection($dialogs);
    }

    public function create(CreateAdminRequest $request)
    {
        $message = $this->messageService->createMessage($request->getDto());
        return new MessageResource($message);
    }

    public function listDialog(ListDialogAdminRequest $request)
    {
        $messages = $this->messageService->listDialogMessages($request->get('dialog_id'));
        return MessageResource::collection($messages);
    }

    public function markAsRead(MarkAsReadAdminRequest $request)
    {
        $status = $this->messageService->markMessageAsRead(
            Message::find($request->get('id')),
            $this->adminUser
        );
        return response()->json(compact('status'));
    }

    public function unreadCount(Request $request)
    {
        $unread_count = $this->messageService->getUnreadCount($this->adminUser);
        return response()->json(compact('unread_count'));
    }

}
