<?php

namespace App\Http\Controllers;

use App\Services\Bills\BillService;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{

    private $billService;

    public function __construct(BillService $billService)
    {
        $this->billService = $billService;
    }

    public function yandex(Request $request)
    {
        if(!$request->has('label'))
            return 'BAD';

        $bill_id = explode('bill_', $request->get('label'))[1];
        $bill = $this->billService->findActiveBill($bill_id);
        if(!$bill)
            return 'BAD';

        // выставляем оплату
        $this->billService->setPaid($bill);

        return 'OK';
    }

}
