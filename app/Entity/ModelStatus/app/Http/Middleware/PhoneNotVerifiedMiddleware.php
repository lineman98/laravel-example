<?php

namespace App\Http\Middleware;

use Closure;

class PhoneNotVerifiedMiddleware
{
    /**
     * Check that phone is already confirmed
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->isPhoneVerified())
            return $next($request);

        return response()->json([
            'success' => false,
            'error' => 'Phone has already confirmed'
        ], 403);
    }
}
