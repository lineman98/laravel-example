<?php

namespace App\Http\Middleware;

use Closure;

class GirlMiddleware
{
    /**
     * Check that phone is already confirmed
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->isGirl())
            return $next($request);

        return response()->json([
            'success' => false,
            'error' => 'You are not girl'
        ], 401);
    }
}
