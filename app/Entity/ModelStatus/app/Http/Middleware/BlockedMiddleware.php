<?php

namespace App\Http\Middleware;

use Closure;

class BlockedMiddleware
{
    /**
     * Check user block
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->isBlocked())
            return $next($request);

        return response()->json([
            'success' => false,
            'error' => 'Ваш аккаунт заблокирован. Узнайте подробности в поддержке'
        ], 403);
    }
}
