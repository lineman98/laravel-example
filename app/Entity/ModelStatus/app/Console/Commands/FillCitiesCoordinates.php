<?php

namespace App\Console\Commands;

use App\Entity\Geo\City\City;
use App\Entity\Geo\City\CityTranslation;
use App\Entity\Geo\Country\Country;
use App\Entity\Geo\Country\CountryTranslation;
use GeoNames\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use VK\Client\Enums\VKLanguage;
use VK\Client\VKApiClient;

class FillCitiesCoordinates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geo:coordinates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /** @var array */
    protected $countries;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // добавляем все города
        $fileName = storage_path("geo/cities15000.txt");

        // Read Raw file
        $this->info("Reading File '$fileName'");
        $filesize = filesize($fileName);
        $handle = fopen($fileName, 'r');

        while (($line = fgets($handle)) !== false) {
            // ignore empty lines and comments
            if ( ! $line or $line === '' or strpos($line, '#') === 0) continue;

            // Convert TAB sepereted line to array
            $line = explode("\t", $line);

            // Check for errors
            if(count($line)!== 19) {
                continue;
            }

            $geoname_id = $line[0];
            $name = $line[2];
            $lat = $line[4];
            $lng = $line[5];
            $country_code = Str::lower($line[8]);
            $population = $line[14];

            $geoCity = City::where('geoname_id', $geoname_id)->first();

            if ($geoCity) {
                $geoCity->population = $population;
                $geoCity->lat = $lat;
                $geoCity->lng = $lng;
                $geoCity->save();
            }

        }
    }
}
