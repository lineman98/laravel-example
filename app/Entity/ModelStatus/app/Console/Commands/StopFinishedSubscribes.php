<?php

namespace App\Console\Commands;

use App\Entity\Users\User;
use App\Notifications\SubscriptionEndedNotification;
use Illuminate\Console\Command;

class StopFinishedSubscribes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:stop-finished';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stop finished subscriptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = app('rinvex.subscriptions.plan_subscription')
            ->where('ends_at', '<=', now())
            ->where('ends_at', '>=', now()->subHour())
            ->get();

        foreach ($subscriptions as $subscription) {
            /** @var User $user */
            $user = $subscription->user;
            if ($user && $user->subscription_payed) {
                $isTrial = $user->subscription_trial;
                $user->subscription_payed = false;
                $user->subscription_trial = false;
                $user->save();

                // отправляем оповещение о том, что нужно оплатить подписку
                $user->notify(new SubscriptionEndedNotification($isTrial));
            }
        }
    }
}
