<?php

namespace App\Console\Commands;

use App\Entity\Users\User;
use App\Notifications\FillAncetaNotification;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class SendFillAncetaNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:anceta-fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка уведомлений о заполнении анкеты моделям';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // выбираем моделей, которые не заполнили анкеты
        $girls = User::query()->girl()
            ->where('status', User::STATUS_NOT_ACTIVE);

        $girls->chunk(100, function (Collection $girls) {
            $girls->each(function (User $girl) {
                $girl->notify(new FillAncetaNotification());
            });
        });
    }
}
