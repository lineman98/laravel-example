<?php

namespace App\Console\Commands;

use App\Entity\Geo\City\City;
use App\Entity\Geo\City\CityTranslation;
use App\Entity\Geo\Country\Country;
use App\Entity\Geo\Country\CountryTranslation;
use GeoNames\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use VK\Client\Enums\VKLanguage;
use VK\Client\VKApiClient;

class FillCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geo:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /** @var array */
    protected $countries;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->initMaps();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        $g = new Client('filmiks5');
        $countries = $g->countryInfo([
            'lang' => 'en'
        ]);

        $countriesWithCodes = [];

        foreach ($countries as $country) {
            break;
            $countryNameEn = $country->countryName;
            $countryRu = $g->countryInfo([
                'country' => $country->countryCode,
                'lang' => 'ru',
                'maxRows' => 1000
            ]);
            $countryNameRu = $countryRu[0]->countryName;

            $geoCountry = Country::where('code', Str::lower($country->countryCode))->first();
            if (!$geoCountry) {
                $geoCountry = Country::create([
                    'code' => Str::lower($country->countryCode),
                    'geoname_id' => $country->geonameId
                ]);
            } else {
                $geoCountry->geoname_id = $country->geonameId;
                $geoCountry->save();
            }

            $geoCountry->update([
                'ru' => ['name' => $countryNameRu],
                'en' => ['name' => $countryNameEn],
            ]);

            $countriesWithCodes[Str::lower($country->countryCode)] = $geoCountry->id;
        }

        // добавляем все города
        $fileName = storage_path("geo/cities15000.txt");

        // Read Raw file
        $this->info("Reading File '$fileName'");
        $filesize = filesize($fileName);
        $handle = fopen($fileName, 'r');

        while (($line = fgets($handle)) !== false) {
            break;
            // ignore empty lines and comments
            if ( ! $line or $line === '' or strpos($line, '#') === 0) continue;

            // Convert TAB sepereted line to array
            $line = explode("\t", $line);

            // Check for errors
            if(count($line)!== 19) {
                continue;
            }

            $geoname_id = $line[0];
            $name = $line[2];
            $lat = $line[4];
            $lng = $line[5];
            $country_code = Str::lower($line[8]);
            $population = $line[14];

            $geoCity = City::where('geoname_id', $geoname_id)->first();
            if (!$geoCity) {
                $tr = CityTranslation::where('name', $name)->first();
                if ($tr) {
                    $geoCity = $tr->city;
                }
            }
            if (!$geoCity) {
                $city_slug = Str::slug($name);
                $geoCity = City::create([
                    'image_path' => $this->getCityPhoto($name, $city_slug),
                    'geoname_id' => $geoname_id,
                    'country_id' => $countriesWithCodes[$country_code],
                    'population' => $population,
                    'lat' => $lat,
                    'lng' => $lng
                ]);
            } else {
                $geoCity->geoname_id = $geoname_id;
                $geoCity->population = $population;
                $geoCity->lat = $lat;
                $geoCity->lng = $lng;
                $geoCity->save();
            }

            $geoCity->update([
                'en' => ['name' => $name],
            ]);
        }
        */

        // переводы
        $fileName = storage_path("geo/alternateNamesV2.txt");

        // Read Raw file
        $this->info("Reading File '$fileName'");
        $filesize = filesize($fileName);
        $handle = fopen($fileName, 'r');

        while (($line = fgets($handle)) !== false) {
            // ignore empty lines and comments
            if ( ! $line or $line === '' or strpos($line, '#') === 0) continue;

            // Convert TAB sepereted line to array
            $line = explode("\t", $line);

            $geoname_id = $line[1];
            $lang = $line[2];
            $name = $line[3];
            $isPreferred = $line[4];

            if ($lang !== 'ru')
                continue;

            if (!$isPreferred) {
                continue;
            }

            $geoCity = City::where('geoname_id', $geoname_id)->first();
            if (!$geoCity)
                continue;

            try {
                $geoCity->update([
                    'ru' => ['name' => $name],
                ]);
            } catch (\Exception $exception) {
                print_r($exception->getMessage());
            }
        }
    }

    public function initMaps() {
        $countries_json = '{"Острова Теркс и Кайкос":"TC","Тайвань":"TW","Бонайре, Синт-Эстатиус и Саба":"SABA","Кюрасао":"CW","Гернси":"GG","Джерси":"JE","Остров Мэн":"IM","Синт-Мартен":"SX","Австралия":"AU","Австрия":"AT","Азербайджан":"AZ","Аландские острова":"AX","Албания":"AL","Алжир":"DZ","Внешние малые острова (США)":"UM","Американские Виргинские острова":"VI","Виргинские острова, США":"VI","Американское Самоа":"AS","Ангилья":"AI","Ангола":"AO","Андорра":"AD","Антарктида":"AQ","Антигуа и Барбуда":"AG","Аргентина":"AR","Армения":"AM","Аруба":"AW","Афганистан":"AF","Багамы":"BS","Бангладеш":"BD","Барбадос":"BB","Бахрейн":"BH","Белиз":"BZ","Белоруссия":"BY","Беларусь":"BY","Бельгия":"BE","Бенин":"BJ","Бермуды":"BM","Болгария":"BG","Боливия":"BO","Босния и Герцеговина":"BA","Ботсвана":"BW","Бразилия":"BR","Британская территория в Индийском океане":"IO","Виргинские острова, Великобритания":"VG","Бруней":"BN","Буркина-Фасо":"BF","Бурунди":"BI","Бутан":"BT","Вануату":"VU","Ватикан":"VA","Великобритания":"GB","Венгрия":"HU","Венесуэла":"VE","Восточный Тимор":"TL","Вьетнам":"VN","Габон":"GA","Гаити":"HT","Гайана":"GY","Гамбия":"GM","Гана":"GH","Гваделупа":"GP","Гватемала":"GT","Гвинея":"GN","Гвинея-Бисау":"GW","Германия":"DE","Гибралтар":"GI","Гондурас":"HN","Гонконг":"HK","Гренада":"GD","Гренландия":"GL","Греция":"GR","Грузия":"GE","Гуам":"GU","Дания":"DK","Конго":"CD","Конго, демократическая республика":"CD","Джибути":"DJ","Доминика":"DM","Доминиканская Республика":"DO","Европейский союз":"EU","Египет":"EG","Замбия":"ZM","Западная Сахара":"EH","Зимбабве":"ZW","Израиль":"IL","Индия":"IN","Индонезия":"ID","Иордания":"JO","Ирак":"IQ","Иран":"IR","Ирландия":"IE","Исландия":"IS","Испания":"ES","Италия":"IT","Йемен":"YE","Северная Корея":"KP","Кабо-Верде":"CV","Казахстан":"KZ","Острова Кайман":"KY","Камбоджа":"KH","Камерун":"CM","Канада":"CA","Катар":"QA","Кения":"KE","Кипр":"CY","Кыргызстан":"KG","Кирибати":"KI","Китай":"CN","Кокосовые острова":"CC","Колумбия":"CO","Коморы":"KM","Коста-Рика":"CR","Кот-д\'Ивуар":"CI","Куба":"CU","Кувейт":"KW","Лаос":"LA","Латвия":"LV","Лесото":"LS","Либерия":"LR","Ливан":"LB","Ливия":"LY","Литва":"LT","Лихтенштейн":"LI","Люксембург":"LU","Маврикий":"MU","Мавритания":"MR","Мадагаскар":"MG","Майотта":"YT","Макао":"MO","Северная Македония":"MK","Малави":"MW","Малайзия":"MY","Мали":"ML","Мальдивы":"MV","Мальта":"MT","Марокко":"MA","Мартиника":"MQ","Маршалловы Острова":"MH","Мексика":"MX","Мозамбик":"MZ","Молдова":"MD","Монако":"MC","Монголия":"MN","Монтсеррат":"MS","Мьянма":"MM","Намибия":"NA","Науру":"NR","Непал":"NP","Нигер":"NE","Нигерия":"NG","Нидерландские Антильские острова":"AN","Нидерланды":"NL","Никарагуа":"NI","Ниуэ":"NU","Новая Каледония":"NC","Новая Зеландия":"NZ","Норвегия":"NO","ОАЭ":"AE","Объединенные Арабские Эмираты":"AE","Оман":"OM","Остров Рождества":"CX","Острова Кука":"CK","Херд и Макдональд":"HM","Пакистан":"PK","Палау":"PW","Палестинская автономия":"PS","Панама":"PA","Папуа — Новая Гвинея":"PG","Парагвай":"PY","Перу":"PE","Питкерн":"PN","Польша":"PL","Португалия":"PT","Пуэрто-Рико":"PR","Республика Конго":"CG","Реюньон":"RE","Россия":"RU","Руанда":"RW","Румыния":"RO","США":"US","Сальвадор":"SV","Самоа":"WS","Сан-Марино":"SM","Сан-Томе и Принсипи":"ST","Саудовская Аравия":"SA","Свазиленд":"SZ","Шпицберген и Ян-Майен":"SJ","Шпицберген и Ян Майен":"SJ","Северные Марианские острова":"MP","Сейшелы":"SC","Сенегал":"SN","Сент-Винсент и Гренадины":"VC","Сент-Китс и Невис":"KN","Сент-Люсия":"LC","Сент-Пьер и Микелон":"PM","Сербия":"RS","Сербия и Черногория (действовал до сентября 2006 года)":"CS","Сингапур":"SG","Сирия":"SY","Словакия":"SK","Словения":"SI","Соломоновы Острова":"SB","Сомали":"SO","Южный Судан":"SD","Судан":"SD","Суринам":"SR","Сьерра-Леоне":"SL","СССР (действовал до сентября 1992 года)":"SU","Таджикистан":"TJ","Таиланд":"TH","Китайская Республика":"TW","Танзания":"TZ","Того":"TG","Токелау":"TK","Тонга":"TO","Тринидад и Тобаго":"TT","Тувалу":"TV","Тунис":"TN","Туркмения":"TM","Туркменистан":"TM","Турция":"TR","Уганда":"UG","Узбекистан":"UZ","Украина":"UA","Уругвай":"UY","Фарерские острова":"FO","Микронезия, федеративные штаты":"FM","Фиджи":"FJ","Филиппины":"PH","Финляндия":"FI","Фолклендские острова":"FK","Франция":"FR","Французская Гвиана":"GF","Французская Полинезия":"PF","Французские Южные и Антарктические Территории":"TF","Хорватия":"HR","Центральноафриканская Республика":"CF","Чад":"TD","Черногория":"ME","Чехия":"CZ","Чили":"CL","Швейцария":"CH","Швеция":"SE","Шри-Ланка":"LK","Эквадор":"EC","Экваториальная Гвинея":"GQ","Эритрея":"ER","Эстония":"EE","Эфиопия":"ET","ЮАР":"ZA","Южно-Африканская Республика":"ZA","Южная Корея":"KR","Южная Георгия и Южные Сандвичевы острова":"GS","Ямайка":"JM","Япония":"JP","Остров Буве":"BV","Остров Норфолк":"NF","Остров Святой Елены":"SH","Святая Елена":"SH","Острова Тёркс и Кайкос":"TC","Уоллис и Футуна":"WF"}';
        $this->countries = json_decode($countries_json, true);
    }

    public function getCityPhoto($city_name, $code) {
        $key = 'AIzaSyA6yJzUIvZ-tk8Ufex3GJMnJxBERx_Vmbc';
        $data = file_get_contents('https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='.urlencode($city_name).'&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key='.$key);
        $data = json_decode($data, true);
        try {
            $reference = $data['candidates'][0]['photos'][0]['photo_reference'];
            $contents = file_get_contents('https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=' . $reference . '&key=' . $key);
            $filePath = 'cities/' . $code . '.jpg';
            Storage::disk('s3')->put($filePath, $contents);
            return $filePath;
        } catch (\Throwable $exception) {
            return null;
        }
    }
}
