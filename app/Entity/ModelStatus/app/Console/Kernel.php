<?php

namespace App\Console;

use App\Console\Commands\CheckPayments;
use App\Console\Commands\CloseEndedOffers;
use App\Console\Commands\FillCountries;
use App\Console\Commands\LastSeenUpdate;
use App\Console\Commands\SearchModelsToOffer;
use App\Console\Commands\SendFillAncetaNotifications;
use App\Console\Commands\SendVerifyPhotoNotifications;
use App\Console\Commands\StopFinishedSubscribes;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        FillCountries::class,
        StopFinishedSubscribes::class,
        CloseEndedOffers::class,
        SearchModelsToOffer::class,
        SendFillAncetaNotifications::class,
        SendVerifyPhotoNotifications::class,
        LastSeenUpdate::class,
        CheckPayments::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('subscriptions:stop-finished')
                  ->everyTenMinutes();

        // отключение предложений через 9ч
        $schedule->command('offers:close-ended')
            ->everyFiveMinutes();

        // отправка оповещений о необходимости заполнения анкеты моделям
        $schedule->command('notifications:anceta-fill')
            ->everyThirtyMinutes();

        // отправка оповещений о необходимости верификации анкеты моделям
        // каждые 3 часа
        $schedule->command('notifications:verify-upload')
            ->cron('0 */3 * * *');

        // поиск моделей на предложения
        $schedule->command('offers:search')
            ->cron('*/3 * * * *');

        // обновление времени последней активности пользователей
        $schedule->command('users:last-seen')
            ->everyMinute();

        // проверка платежей яндекс деньги
        $schedule->command('payments:check')
            ->everyMinute();

        $schedule->command('backup:clean')->daily();
        $schedule->command('backup:run --only-db')->cron('0 */3 * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
