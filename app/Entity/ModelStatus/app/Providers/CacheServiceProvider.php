<?php

namespace App\Providers;

use App\Entity\Faq\Faq;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Offers\Category\OfferCategory;
use App\Entity\Options\Eye\Eye;
use App\Entity\Options\Hair\Hair;
use App\Entity\Options\Nationality\Nationality;
use App\Entity\Options\Purpose\Purpose;
use App\Entity\Options\Visa\Visa;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class CacheServiceProvider extends ServiceProvider
{
    private $classes = [
        City::class,
        Country::class,
        Hair::class,
        Eye::class,
        Nationality::class,
        Purpose::class,
        OfferCategory::class,
        ModelStatus::class,
        Faq::class,
        Visa::class,
    ];

    public function boot(): void
    {
        foreach ($this->classes as $class) {
            $this->registerFlusher($class);
        }
    }

    private function registerFlusher($class): void
    {
        $flush = function() use ($class) {
            Cache::tags($class)->flush();
        };

        /** @var Model $class */
        $class::created($flush);
        $class::saved($flush);
        $class::updated($flush);
        $class::deleted($flush);
    }
}
