<?php

namespace App\Entity\BlackList;

use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class BlackListUser extends Model
{

    protected $fillable = ['user_from_id', 'user_to_id'];

    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_from_id');
    }

    public function userTo()
    {
        return $this->belongsTo(User::class, 'user_to_id');
    }

}
