<?php

namespace App\Entity\Orders\Interfaces;

interface Order
{

    /**
     * Выдача материалов/доступов заказа
     */
    public function afterPay(): void;

    /**
     * Название заказа
     * @return string
     */
    public function getName(): string;

    /**
     * Проверка, возможна ли оплата счета с реферального баланса
     * @return bool
     */
    public function canPayFromReferralBalance(): bool;

    /**
     * Стоимость при оплате с реферального баланса
     * @return int
     */
    public function getReferralBalanceCost(): int;

}
