<?php

namespace App\Entity\Orders;

use App\Entity\Bill\Bill;
use App\Entity\Offers\Offer;
use App\Entity\Orders\Interfaces\Order;
use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class OrderOfferPublication extends Model implements Order
{

    protected $fillable = ['offer_id', 'user_id'];

    public function bill()
    {
        return $this->morphOne(Bill::class, 'order');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

    public function afterPay(): void
    {
        if(!$this->user || !$this->offer)
            return;

        // отправляем на проверку
        $this->offer->setInspection();
    }

    public function getName(): string
    {
        return 'Оплата публикации предложения';
    }

    public function canPayFromReferralBalance(): bool
    {
        return true;
    }

    public function getReferralBalanceCost(): int
    {
        return 2;
    }

}
