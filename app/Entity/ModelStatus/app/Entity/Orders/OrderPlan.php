<?php

namespace App\Entity\Orders;

use App\Entity\Bill\Bill;
use App\Entity\Orders\Interfaces\Order;
use App\Entity\Users\User;
use App\Services\Users\ReferralBonusService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class OrderPlan extends Model implements Order
{

    protected $fillable = ['plan_id', 'user_id'];

    public function bill()
    {
        return $this->morphOne(Bill::class, 'order');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function plan()
    {
        return $this->belongsTo(
            app('rinvex.subscriptions.plan'),
            'plan_id'
        );
    }

    public function afterPay(): void
    {
        if(!$this->user || !$this->plan)
            return;

        // продлеваем тарифный план
        $user = $this->user;

        DB::transaction(function () use ($user) {
            $currentUserSubscription = $user->getCurrentSubscription();

            // добавляем тарифный план, если он еще не подключен
            if (!$currentUserSubscription) {
                $subscription = $user->newSubscription('main', $this->plan);
                $user->subscription_slug = $subscription->slug;
            } else {
                // обновляем тарифный план
                $currentUserSubscription->renew();
            }

            $user->subscription_payed = true;
            $user->save();
        });
    }

    public function getName(): string
    {
        return 'Оплата доступа на 1 месяц';
    }

    public function canPayFromReferralBalance(): bool
    {
        return true;
    }

    public function getReferralBalanceCost(): int
    {
        /** @var User $user */
        $user = $this->user;
        if ($user->isManager())
            return config('plans.manager_free_month_referrals');
        return config('plans.model_free_month_referrals');
    }

}
