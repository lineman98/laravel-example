<?php

namespace App\Entity\Orders;

use App\Entity\Bill\Bill;
use App\Entity\Orders\Interfaces\Order;
use App\Entity\Tours\Tour;
use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class OrderTourPublication extends Model implements Order
{

    protected $fillable = ['tour_id', 'user_id'];

    public function bill()
    {
        return $this->morphOne(Bill::class, 'order');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function tour()
    {
        return $this->belongsTo(Tour::class, 'tour_id');
    }

    public function afterPay(): void
    {
        if(!$this->user || !$this->tour)
            return;

        // отправляем на проверку
        $this->tour->setInspection();
    }

    public function getName(): string
    {
        return 'Оплата публикации тура';
    }

    public function canPayFromReferralBalance(): bool
    {
        return false;
    }

    public function getReferralBalanceCost(): int
    {
        return -1;
    }

}
