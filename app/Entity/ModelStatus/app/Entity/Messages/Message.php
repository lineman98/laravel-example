<?php

namespace App\Entity\Messages;

use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{

    use SoftDeletes;

    public function dialog()
    {
        return $this->belongsTo(Dialog::class, 'dialog_id');
    }

    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_from_id');
    }
}
