<?php

namespace App\Entity\Faq;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{

    use Translatable;

    public $translatedAttributes = ['question', 'answer'];
    public $timestamps = false;

}
