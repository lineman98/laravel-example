<?php

namespace App\Entity\Users;

use App\Entity\Claims\Claim;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Entity\Invites\Invite;
use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Offers\Category\OfferCategory;
use App\Entity\Offers\Offer;
use App\Entity\Options\Eye\Eye;
use App\Entity\Options\Hair\Hair;
use App\Entity\Options\Nationality\Nationality;
use App\Entity\Options\Purpose\Purpose;
use App\Entity\Options\Visa\Visa;
use App\Entity\Reviews\Review;
use App\Entity\Messages\Dialog;
use App\Entity\Messages\Message;
use App\Filters\BaseFilter;
use App\Http\Resources\Users\UserImageResource;
use App\Services\BlackList\BlackListService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;
use Rinvex\Subscriptions\Models\PlanSubscription;
use Rinvex\Subscriptions\Traits\HasSubscriptions;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use VientoDigital\LaravelExpoPushNotifications\ExpoPushNotifiable;

class User extends Authenticatable implements HasMedia
{

    use HasApiTokens, Notifiable;
    use HasMediaTrait;
    use SoftDeletes;
    use HasSubscriptions;

    const ROLE_USER = 'user';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_ADMIN = 'admin';

    const TYPE_GIRL = 'girl';
    const TYPE_MANAGER = 'manager';

    const STATUS_NOT_ACTIVE = 'not_active';
    const STATUS_ACTIVE = 'active';
    const STATUS_BLOCKED = 'blocked';

    const NOT_VERIFIED = 'not';
    const WAIT_VERIFY = 'wait';
    const VERIFIED = 'yes';

    public $registerMediaConversionsUsingModelInstance = true;

    const PUBLIC_PHOTOS = 'public';
    const VERIFY_PHOTOS = 'verify_photos';
    const PUBLIC_VIDEOS = 'videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'status', 'type', 'verified', 'phone_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $dates = [
        'last_seen'
    ];

    public static function rolesList(): array
    {
        return [
            self::ROLE_USER,
            self::ROLE_MODERATOR,
            self::ROLE_ADMIN,
        ];
    }

    public static function typesList(): array
    {
        return [
            self::TYPE_GIRL => 'girl',
            self::TYPE_MANAGER => 'manager',
        ];
    }

    public function isModerator(): bool
    {
        return $this->role === self::ROLE_MODERATOR;
    }

    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function isGirl(): bool
    {
        return $this->type === self::TYPE_GIRL;
    }

    public function isManager(): bool
    {
        return $this->type === self::TYPE_MANAGER;
    }

    public function isPhoneVerified(): bool
    {
        return $this->phone_verified === 'yes';
    }

    public function isWaitVerify(): bool
    {
        return $this->verified === self::WAIT_VERIFY;
    }

    public function isVerified(): bool
    {
        return $this->verified === self::VERIFIED;
    }

    public function isNotVerified(): bool
    {
        return $this->verified === self::NOT_VERIFIED;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isBlocked(): bool
    {
        return $this->status === self::STATUS_BLOCKED;
    }

    public function isOnline(): bool
    {
        return !!$this->online;
    }

    public function setOnline(): bool
    {
        $this->online = true;
        return $this->saveOrFail();
    }

    public function setOffline(Carbon $lastSeenDate=null): bool
    {
        $this->online = false;
        if ($lastSeenDate) {
            $this->last_seen = $lastSeenDate;
        }
        return $this->saveOrFail();
    }

    public function dialogs() {
        return $this->belongsToMany(Dialog::class, 'dialog_users');
    }

    public function viewedOffers() {
        return $this->belongsToMany(Offer::class, 'offer_user_views');
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setVerifiedPhone(): bool
    {
        $this->phone_verified = 'yes';
        return $this->saveOrFail();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setVerified(): bool
    {
        $this->verified = self::VERIFIED;
        return $this->saveOrFail();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setWaitVerify(): bool
    {
        $this->verified = self::WAIT_VERIFY;
        return $this->saveOrFail();
    }

    /**
     * @param string $reason
     * @return bool
     * @throws \Throwable
     */
    public function setNotVerified(string $reason): bool
    {
        $this->verified = self::NOT_VERIFIED;
        $this->unverified_reason = $reason;
        return $this->saveOrFail();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function activate(): bool
    {
        if($this->isActive())
            return true;
        if($this->isBlocked())
            return false;
        $this->status = self::STATUS_ACTIVE;
        return $this->saveOrFail();
    }

    public function block(): bool
    {
        $this->status = self::STATUS_BLOCKED;
        return $this->saveOrFail();
    }

    public function canCreateOffers(): bool
    {
        return !!$this->can_create_offers;
    }

    public function canCreateTours(): bool
    {
        return !!$this->can_create_tours;
    }

    public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id');
    }

    public function invite()
    {
        return $this->hasOne(Invite::class, 'user_id')
            ->where('type', Invite::DEFAULT_TYPE);
    }

    public function inviteAtRegistration()
    {
        return $this->belongsTo(Invite::class, 'invite_id');
    }

    /**
     * Статусы, по которым модель получает предложения
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function modelStatuses()
    {
        return $this->belongsToMany(
            ModelStatus::class,
            'user_statuses',
            'user_id',
            'status_id'
        )->using(UserStatus::class);
    }

    /**
     * Статусы, по которым может рассылать предложения менеджер
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function accessStatuses()
    {
        return $this->belongsToMany(
            ModelStatus::class,
            'user_status_accesses',
            'user_id',
            'status_id'
        )->using(UserStatusAccess::class);
    }

    /**
     * Категории предложений, которые хочет получать модель
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function offerCategories()
    {
        return $this->belongsToMany(
            OfferCategory::class,
            'user_offer_categories',
            'user_id',
            'offer_category_id'
        )->using(UserOfferCategory::class);
    }

    public function purposes()
    {
        return $this->belongsToMany(Purpose::class, 'purpose_users')
            ->using(PurposeUser::class);
    }

    public function eye()
    {
        return $this->belongsTo(Eye::class, 'eye_id');
    }

    public function hair()
    {
        return $this->belongsTo(Hair::class, 'hair_id');
    }

    public function nationality()
    {
        return $this->belongsTo(Nationality::class, 'nationality_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('tiny')
            ->fit(Manipulations::FIT_CROP, 64, 64)
            ->performOnCollections(self::PUBLIC_PHOTOS, self::VERIFY_PHOTOS);

        $this->addMediaConversion('small')
            ->fit(Manipulations::FIT_CROP, 128, 128)
            ->performOnCollections(self::PUBLIC_PHOTOS, self::VERIFY_PHOTOS);

        $this->addMediaConversion('big')
            ->quality(60)
            ->performOnCollections(self::PUBLIC_PHOTOS, self::VERIFY_PHOTOS);

        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 128, 128)
            ->extractVideoFrameAtSecond(1)
            ->performOnCollections(self::PUBLIC_VIDEOS);
    }

    /**
     * Аватарка
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function avatar()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
            ->where('collection_name', static::PUBLIC_PHOTOS)
            ->where('custom_properties->avatar', true);
    }

    /**
     * Фото для прдтверждения анкеты
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function verifyPhoto()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
            ->where('collection_name', static::VERIFY_PHOTOS);
    }

    /**
     * Проверенный фотографии
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function publicPhotos()
    {
        return $this->morphMany(config('medialibrary.media_model'), 'model')
            ->where('collection_name', static::PUBLIC_PHOTOS)
            ->where('custom_properties->checked', '=',true);
    }

    /**
     * Непроверенные фотографии
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function uncheckedPhotos()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
            ->where('collection_name', static::PUBLIC_PHOTOS)
            ->where('custom_properties->checked', '!=',true);
    }

    /**
     * Проверенные видео
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function publicVideos()
    {
        return $this->morphMany(config('medialibrary.media_model'), 'model')
            ->where('collection_name', static::PUBLIC_VIDEOS)
            ->where('custom_properties->checked', '=',true);
    }

    /**
     * Непроверенные видео
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function uncheckedVideos()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
            ->where('collection_name', static::PUBLIC_VIDEOS)
            ->where('custom_properties->checked', '!=',true);
    }

    /*
     * Все жалобы на пользователя
     */
    public function claims()
    {
        return $this->morphMany(Claim::class, 'claimable');
    }

    public function expoTokens()
    {
        return $this->hasMany(UserExpoToken::class, 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'user_id');
    }

    public function reviewsPositiveCount()
    {
        return Cache::rememberForever('reviews_user_'.$this->id.'_positive', function () {
            return $this->reviews()->where('positive', 1)->count();
        });
    }

    public function reviewsNegativeCount()
    {
        return Cache::rememberForever('reviews_user_'.$this->id.'_negative', function () {
            return $this->reviews()->where('positive', 0)->count();
        });
    }

    public function pushTokens(): array
    {
        return Cache::rememberForever('push_tokens_user_'.$this->id, function() {
            return $this->expoTokens->pluck('token')->toArray();
        });
    }

    public function routeNotificationForApn(): array
    {
        return $this->pushTokens();
    }

    public function routeNotificationForFcm(): array
    {
        return $this->pushTokens();
    }

    public function visas()
    {
        return $this->belongsToMany(Visa::class, 'user_visas')
            ->using(UserVisa::class);
    }

    /**
     * Краткая информация о юзере
     */
    public function getMetaInfo()
    {
        return Cache::tags('user_'.$this->id)
            ->remember('user_meta_'.$this->id, 60*60*120, function() {
                return [
                    'type' => $this->type,
                    'id' => $this->id,
                    'name' => $this->name,
                    'avatar' => $this->avatar ? new UserImageResource($this->avatar) : null
                ];
            });
    }

    /**
     * Проверка, есть ли переданный пользователь в блэклисте
     * @param User $user
     * @return bool
     */
    public function hasInBlackList(User $user): bool
    {
        $service = new BlackListService;
        return $service->inBlackList($this, $user);
    }

    /**
     * @return \Rinvex\Subscriptions\Models\PlanSubscription|null
     */
    public function getCurrentSubscription(): ?PlanSubscription
    {
        if(!$this->subscription_slug)
            return null;
        return $this->subscription($this->subscription_slug);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeGirl(Builder $query)
    {
        return $query->where('users.type', self::TYPE_GIRL);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeManager(Builder $query)
    {
        return $query->where('users.type', self::TYPE_MANAGER);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('users.status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeVerified(Builder $query)
    {
        return $query->where('users.verified', 'yes');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePhoneVerified(Builder $query)
    {
        return $query->where('users.phone_verified', 'yes');
    }

    /**
     * @param Builder $builder
     * @param BaseFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $builder, BaseFilter $filter)
    {
        return $filter->apply($builder);
    }

}
