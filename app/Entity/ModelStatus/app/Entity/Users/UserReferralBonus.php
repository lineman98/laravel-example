<?php

namespace App\Entity\Users;

use Illuminate\Database\Eloquent\Model;

class UserReferralBonus extends Model
{

    protected $fillable = [
        'user_id',
        'user_from_id',
    ];

}
