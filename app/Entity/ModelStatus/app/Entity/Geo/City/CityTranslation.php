<?php

namespace App\Entity\Geo\City;

use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{

    protected $fillable = ['name'];
    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

}
