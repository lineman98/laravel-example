<?php

namespace App\Entity\Geo\City;

use App\Entity\Geo\Country\Country;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Entity\Offers\Offer;

class City extends Model implements TranslatableContract, HasMedia
{

    use Translatable;
    use HasMediaTrait;

    protected $fillable = ['slug', 'image_path', 'country_id', 'geoname_id', 'population', 'lat', 'lon'];
    public $translatedAttributes = ['name'];
    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function offers() {
        return $this->hasMany(Offer::class, 'city_id');
    }

    /**
     * @param Builder $builder
     * @param BaseFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $builder, BaseFilter $filter)
    {
        return $filter->apply($builder);
    }
}
