<?php

namespace App\Entity\Geo\Country;

use App\Entity\Geo\City\City;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Country extends Model implements TranslatableContract
{

    use Translatable;

    protected $fillable = ['code', 'geoname_id'];
    public $translatedAttributes = ['name'];
    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany(City::class, 'country_id');
    }

}
