<?php

namespace App\Entity\Invites;

use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{

    // создан после регистрации пользователя
    const DEFAULT_TYPE = 'default';
    // создан вручную
    const CUSTOM_TYPE = 'custom';

    /**
     * Включена ли функция выдачи триал-доступа
     * @return bool
     */
    public function giveTrial(): bool
    {
        return !!$this->give_trial;
    }

    /**
     * Выдавать ли триал моделям
     * @return bool
     */
    public function giveTrialForGirls(): bool
    {
        return $this->giveTrial() && !!$this->trial_for_girls;
    }

    /**
     * Выдавать ли триал менеджерам
     * @return bool
     */
    public function giveTrialForManagers(): bool
    {
        return $this->giveTrial() && !!$this->trial_for_managers;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function accessStatuses()
    {
        return $this->hasMany(InviteStatusAccess::class, 'invite_id');
    }

    public function modelStatus()
    {
        return $this->belongsTo(ModelStatus::class, 'model_status_id');
    }

}
