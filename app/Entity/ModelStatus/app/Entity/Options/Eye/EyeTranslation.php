<?php

namespace App\Entity\Options\Eye;

use Illuminate\Database\Eloquent\Model;

class EyeTranslation extends Model
{

    protected $fillable = ['name'];
    public $timestamps = false;

}
