<?php

namespace App\Entity\Options\Purpose;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Purpose extends Model implements TranslatableContract
{

    use Translatable;

    public $translatedAttributes = ['name'];
    public $timestamps = false;

}
