<?php

namespace App\Entity\Options\Visa;

use Illuminate\Database\Eloquent\Model;

class VisaTranslation extends Model
{

    protected $fillable = ['name'];
    public $timestamps = false;

}
