<?php

namespace App\Entity\Options\Nationality;

use Illuminate\Database\Eloquent\Model;

class NationalityTranslation extends Model
{

    protected $fillable = ['name_male', 'name_female'];
    public $timestamps = false;

}
