<?php

namespace App\Entity\Reviews;

use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferLink;
use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_from_id');
    }

    public function offerLink()
    {
        return $this->belongsTo(OfferLink::class, 'offer_link_id');
    }

    public function offer()
    {
        // todo протестить
        return $this->hasOneThrough(
            Offer::class,
            OfferLink::class,
            'offer_link_id',
            'id',
            'iddd',
            'id'
        );
    }

    public function isPositive(): bool
    {
        return !!$this->positive;
    }

}
