<?php

namespace App\Entity\Claims;

use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function claimable()
    {
        return $this->morphTo();
    }

}
