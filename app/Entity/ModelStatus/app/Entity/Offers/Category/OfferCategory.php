<?php

namespace App\Entity\Offers\Category;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class OfferCategory extends Model implements TranslatableContract
{

    use Translatable;

    public $translatedAttributes = ['name'];
    public $timestamps = false;

}
