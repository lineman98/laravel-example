<?php

namespace App\Entity\Offers;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OfferNationality extends Pivot
{
    public $timestamps = false;
}
