<?php

namespace App\Entity\Offers;

use App\Entity\Claims\Claim;
use App\Entity\Geo\City\City;
use App\Entity\Geo\Country\Country;
use App\Entity\Offers\Category\OfferCategory;
use App\Entity\Options\Eye\Eye;
use App\Entity\Options\Hair\Hair;
use App\Entity\Options\Nationality\Nationality;
use App\Entity\Options\Purpose\Purpose;
use App\Entity\Options\Visa\Visa;
use App\Entity\Users\User;
use App\Entity\Messages\Dialog;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{

    use SoftDeletes;

    protected $dates = ['meeting_date:Y-m-d', 'started_at', 'stopped_at'];

    protected $casts = [
        'genders' => 'array'
    ];

    // Предложение ожидает оплату
    public const STATUS_WAIT_PAY = 'wait_pay';
    // Предложение на проверке
    public const STATUS_INSPECTION = 'inspection';
    // Предложение заблокировано на модерации
    public const STATUS_BLOCKED = 'blocked';
    // Предложение активно: идет поиск моделей
    public const STATUS_ACTIVE = 'active';
    // Предложение завершено (принята хотя бы одна модель)
    public const STATUS_ENDED = 'ended';
    // Предложение закрыто
    public const STATUS_CLOSED = 'closed';

    public function isWaitPay(): bool
    {
        return $this->status === self::STATUS_WAIT_PAY;
    }

    public function onInspection(): bool
    {
        return $this->status === self::STATUS_INSPECTION;
    }

    public function isBlocked(): bool
    {
        return $this->status === self::STATUS_BLOCKED;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isEnded(): bool
    {
        return $this->status === self::STATUS_ENDED;
    }

    public function isClosed(): bool
    {
        return $this->status === self::STATUS_CLOSED;
    }

    public function setInspection(): bool
    {
        $this->status = self::STATUS_INSPECTION;
        return $this->saveOrFail();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function activate(): bool
    {
        if(!$this->onInspection())
            return false;

        $this->status = self::STATUS_ACTIVE;
        $this->saveOrFail();

        return true;
    }

    /**
     * @param string $reason
     * @return bool
     * @throws \Throwable
     */
    public function block(string $reason): bool
    {
        if($this->isBlocked())
            return false;

        $this->status = self::STATUS_BLOCKED;
        $this->rejection_reason = $reason;
        $this->saveOrFail();

        return true;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function close(): bool
    {
        /*if(!$this->isActive())
            return false;*/

        $this->delete();

        $this->status = self::STATUS_CLOSED;
        $this->stopped_at = now();
        $this->saveOrFail();

        return true;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setEnded(): bool
    {
        if(!$this->isActive())
            return false;

        $this->status = self::STATUS_ENDED;
        $this->stopped_at = now();
        $this->saveOrFail();

        return true;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(OfferCategory::class, 'category_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function countryFrom()
    {
        return $this->belongsTo(Country::class, 'country_from_id');
    }

    public function cityFrom()
    {
        return $this->belongsTo(City::class, 'city_from_id');
    }

    public function views() {
        return $this->belongsToMany(User::class, 'offer_user_views');
    }

    public function purposes()
    {
        return $this->belongsToMany(Purpose::class, 'offer_purposes')
            ->using(OfferPurpose::class);
    }

    public function eyes()
    {
        return $this->belongsToMany(Eye::class, 'offer_eyes')
            ->using(OfferEye::class);
    }

    public function hairs()
    {
        return $this->belongsToMany(Hair::class, 'offer_hairs')
            ->using(OfferHair::class);
    }

    public function visas()
    {
        return $this->belongsToMany(Visa::class, 'offer_visas')
            ->using(OfferVisa::class);
    }

    public function nationalities()
    {
        return $this->belongsToMany(Nationality::class, 'offer_nationalities')
            ->using(OfferNationality::class);
    }

    public function dialog() {
        return $this->hasOne(Dialog::class, 'owner_id')->where('type', 'chat');
    }

    /*
     * Все жалобы на предложение
     */
    public function claims()
    {
        return $this->morphMany(Claim::class, 'claimable');
    }

    public function application()
    {
		return $this->hasOne(OfferUser::class, 'offer_id')
			->where('user_id', \Auth::user()->id);
    }

    public function applications()
    {
        return $this->belongsToMany(User::class, 'offer_users')
            ->using(OfferUser::class);
    }

    public function usersGoing()
    {
        return $this->belongsToMany(
            User::class,
            OfferUser::class,
            'offer_id',
            'user_id',
            'id',
            'id'
        )->where('offer_users.status', OfferUser::STATUS_ACCEPT)
            ->withPivot(['id', 'status']);
    }

    public function usersRequesting()
    {
        return $this->belongsToMany(
            User::class,
            OfferUser::class,
            'offer_id',
            'user_id',
            'id',
            'id'
        )->where('offer_users.status', OfferUser::STATUS_SENDED)
            ->withPivot(['id', 'status']);
    }

    /**
     * Модели, от которых ждем ответ
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function waitModels()
    {
        return $this->belongsToMany(
            User::class,
            OfferLink::class,
            'offer_id',
            'user_id',
            'id',
            'id'
        )->where('offer_links.status', OfferLink::STATUS_WAIT_MODEL)
            ->withPivot(['id', 'status']);
    }

    /**
     * Модели, которые отказались
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function refusedModels()
    {
        return $this->belongsToMany(
            User::class,
            OfferLink::class,
            'offer_id',
            'user_id',
            'id',
            'id'
        )->where('offer_links.status', OfferLink::STATUS_MODEL_REFUSED)
            ->withPivot(['id', 'status']);
    }

    /**
     * Модели, которые согласились
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function agreedModels()
    {
        return $this->belongsToMany(
            User::class,
            OfferLink::class,
            'offer_id',
            'user_id',
            'id',
            'id'
        )->where('offer_links.status', OfferLink::STATUS_MODEL_AGREED)
            ->withPivot(['id', 'status']);
    }

    /**
     * Модели, которые отказаны менеджером
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function refusedByManagerModels()
    {
        return $this->belongsToMany(
            User::class,
            OfferLink::class,
            'offer_id',
            'user_id',
            'id',
            'id'
        )->where('offer_links.status', OfferLink::STATUS_MANAGER_REFUSED)
            ->withPivot(['id', 'status']);
    }

    /**
     * Модели, которые приняты менеджером
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function agreedByManagerModels()
    {
        return $this->belongsToMany(
            User::class,
            OfferLink::class,
            'offer_id',
            'user_id',
            'id',
            'id'
        )->where('offer_links.status', OfferLink::STATUS_MANAGER_AGREED)
            ->withPivot(['id', 'status']);
    }

    /**
     * @param Builder $builder
     * @param BaseFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $builder, BaseFilter $filter)
    {
        return $filter->apply($builder);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('offers.status', self::STATUS_ACTIVE);
    }

}
