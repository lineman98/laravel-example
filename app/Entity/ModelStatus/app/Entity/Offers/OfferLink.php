<?php

namespace App\Entity\Offers;

use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class OfferLink extends Model
{

    // ожидание ответа от модели
    public const STATUS_WAIT_MODEL = 'wait_model';
    // модель отказалась
    public const STATUS_MODEL_REFUSED = 'model_refused';
    // модель согласилась
    public const STATUS_MODEL_AGREED = 'model_agreed';
    // менеджер отказал модели
    public const STATUS_MANAGER_REFUSED = 'manager_refused';
    // менеджер принял модель
    public const STATUS_MANAGER_AGREED = 'manager_agreed';

    public $dates = ['opened_at'];

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setModelRefused(): bool
    {
        if(!$this->isWaitModel())
            return false;

        $this->status = self::STATUS_MODEL_REFUSED;
        return $this->saveOrFail();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setModelAgreed(): bool
    {
        if(!$this->isWaitModel())
            return false;

        $this->status = self::STATUS_MODEL_AGREED;
        return $this->saveOrFail();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setManagerRefused(): bool
    {
        if(!$this->isModelAgreed())
            return false;

        $this->status = self::STATUS_MANAGER_REFUSED;
        return $this->saveOrFail();
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setManagerAgreed(): bool
    {
        if(!$this->isModelAgreed())
            return false;

        $this->status = self::STATUS_MANAGER_AGREED;
        return $this->saveOrFail();
    }

    public function isWaitModel(): bool
    {
        return $this->status === self::STATUS_WAIT_MODEL;
    }

    public function isModelRefused(): bool
    {
        return $this->status === self::STATUS_MODEL_REFUSED;
    }

    public function isModelAgreed(): bool
    {
        return $this->status === self::STATUS_MODEL_AGREED;
    }

    public function isManagerRefused(): bool
    {
        return $this->status === self::STATUS_MANAGER_REFUSED;
    }

    public function isManagerAgreed(): bool
    {
        return $this->status === self::STATUS_MANAGER_AGREED;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

}
