<?php

namespace App\Entity\Offers;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OfferHair extends Pivot
{
    public $timestamps = false;
}
