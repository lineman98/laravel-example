<?php

namespace App\Entity\ModelStatus;

use Illuminate\Database\Eloquent\Model;

class ModelStatus extends Model
{

    protected $fillable = ['name'];

}
