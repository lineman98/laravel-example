<?php

namespace App\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidMessagePriority;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;
use NotificationChannels\Fcm\Resources\WebpushConfig;

class UserNotVerified extends Notification implements ShouldQueue
{
    use Queueable;

    private $user_id;

    /** @var string */
    private $reason;

    public function __construct(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $this->user_id = $notifiable->id;
        $channels = ['broadcast'];
        if(count($notifiable->pushTokens())) {
            $channels[] = FcmChannel::class;
        }
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'reason' => $this->reason,
        ];
    }

    public function toFcm($notifiable)
    {
        return FcmMessage::create()
            ->setData([
                'type' => 'UserNotVerified',
                'reason' => $this->reason
            ])
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setPriority(AndroidMessagePriority::HIGH())
            )
            ->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
                    ->setHeaders([
                        "apns-priority" => "5"
                    ])
            )
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle("⛔ Вы не прошли проверку")
                    ->setBody($this->reason)
                    ->setImage('https://web.laravel-example/static/img/logo.png')
            )
            ->setWebpush(
                WebpushConfig::create()
                    ->setNotification([
                        "title" => "⛔ Вы не прошли проверку",
                        "body" => $this->reason,
                        "click_action" => "https://web.laravel-example/",
                        "icon" => "https://web.laravel-example/static/img/logo.png"
                    ])
            );
    }

    public function broadcastType()
    {
        return 'UserNotVerified';
    }

    public function broadcastOn()
    {
        return [new PrivateChannel('App.User.'.$this->user_id)];
    }

}
