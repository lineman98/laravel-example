<?php

namespace App\Notifications;

use App\Entity\Tours\Tour;
use App\Http\Resources\Tours\TourResource;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Apn\ApnChannel;
use NotificationChannels\Apn\ApnMessage;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\WebpushConfig;
use VientoDigital\LaravelExpoPushNotifications\Channels\ExpoPushNotificationChannel;
use VientoDigital\LaravelExpoPushNotifications\Messages\ExpoPushNotificationMessage;

class TourBlocked extends Notification implements ShouldQueue
{
    use Queueable;

    private $tour;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Tour $tour)
    {
        $this->tour = $tour;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = ['broadcast'];
        if(count($notifiable->pushTokens())) {
            $channels[] = FcmChannel::class;
        }
        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'tour' => $this->tour,
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'tour' => $this->tour,
        ]);
    }

    public function toFcm($notifiable)
    {
        return FcmMessage::create()
            ->setData([
                'tour_id' => $this->tour->id,
                'type' => 'TourBlocked'
            ])
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle("❗ Ваш тур заблокирован")
                    ->setBody(":(")
                    ->setImage('https://web.laravel-example/static/img/logo.png')
            )
            ->setWebpush(
                WebpushConfig::create()
                    ->setNotification([
                        "title" => "❗ Ваш тур заблокирован",
                        "body" => ":(",
                        "click_action" => "https://web.laravel-example/",
                        "icon" => "https://web.laravel-example/static/img/logo.png"
                    ])
            );
    }

    public function broadcastType()
    {
        return 'TourBlocked';
    }

    public function broadcastOn()
    {
        return [new PrivateChannel('App.User.'.$this->tour->user_id)];
    }

}
