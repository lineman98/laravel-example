<?php

namespace App\Services\Invites;

use App\Entity\Invites\Invite;
use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Users\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InvitesService {

    public function randomString(
        int $length = 64,
        string $keyspace = '0123456789ABCDEFGHKLMNOPQRSTUVWXYZ'
    ): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    /**
     * @return string
     */
    public function generateInvite(): string
    {
        $code = $this->randomString(6);
        while (Invite::where('code', $code)->first()) {
            $code = $this->randomString(6);
        }
        return $code;
    }

    /**
     * Создание копии инвайта на основе прошлого инвайта
     * @param Invite $invite
     * @param User $user
     * @return Invite
     * @throws \Throwable
     */
    public function createInviteCloneForUser(Invite $invite, User $user): Invite
    {
        $clone = $invite->replicate(['user_id', 'code']);
        // подбираем уникальный код
        $clone->code = $this->generateInvite();
        $clone->type = Invite::DEFAULT_TYPE;
        $clone->user()->associate($user);
        $clone->saveOrFail();

        foreach ($invite->accessStatuses as $accessStatus) {
            $clone->accessStatuses()->create([
                'status_id' => $accessStatus->status_id
            ]);
        }

        return $clone;
    }

    /**
     * Создание дефолтного инвайта
     * @param User $user
     * @return Invite
     * @throws \Throwable
     */
    public function createDefaultInviteForUser(User $user): Invite
    {
        $invite = new Invite;
        // подбираем уникальный код
        $invite->code = $this->generateInvite();
        $invite->type = Invite::DEFAULT_TYPE;
        $invite->user()->associate($user);
        $invite->saveOrFail();
        return $invite;
    }

    /**
     * Применение инвайта к новому пользователю
     * @param Invite $invite
     * @param User $user
     * @throws \Throwable
     */
    public function applyInviteForUser(Invite $invite, User $user): void
    {
        DB::beginTransaction();

        try {
            $user->inviteAtRegistration()->associate($invite);
            $user->referrer_id = $invite->user_id;

            // присваиваем статус модели из инвайта
            if ($user->isGirl()) {
                // если статус прописан в инвайте
                if ($invite->modelStatus) {
                    $user->modelStatuses()->sync([$invite->modelStatus->id]);
                } else {
                    // иначе выставляем дефолтный
//                    $model_status = ModelStatus::where('name', 'NEW')->first();
//                    if ($model_status)
//                        $user->modelStatuses()->sync([$model_status->id]);
                }
            } else {
                // проставляем статусы моделей, к которым имеет доступ менеджер
                $user->accessStatuses()->sync($invite->accessStatuses->pluck('status_id'));
            }

            // если выдаем триал
            if ($invite->giveTrialForManagers() && $user->isManager() || $invite->giveTrialForGirls() && $user->isGirl()) {
                $subscription = $user->getCurrentSubscription();
                $subscription->ends_at = now()->addDays($invite->trial_days);
                $subscription->saveOrFail();
                $user->subscription_payed = true;
                $user->subscription_trial = true;
            }

            $user->saveOrFail();

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
    }

}
