<?php

namespace App\Services\Bills;

use App\Entity\Wallet\Wallet;

class WalletService
{

    /**
     * Активный кошелек, который может принять оплату
     * @return Wallet|null
     */
    public function getActiveWallet(): string
    {
        $wallet = Wallet::inRandomOrder()->active()->firstOrFail();
        return $wallet->number;
    }

}
