<?php

namespace App\Services\Reviews;

use App\DTO\Reviews\ReviewCreateDto;
use App\Entity\Reviews\Review;

class ReviewsService {

    /**
     * @param ReviewCreateDto $dto
     * @return Review
     * @throws \Throwable
     */
    public function create(ReviewCreateDto $dto): Review
    {
        $review = new Review;
        $review->content = $dto->getContent();
        $review->positive = $dto->isPositive();
        $review->user()->associate($dto->getUser());
        $review->userFrom()->associate($dto->getUserFrom());
        $review->saveOrFail();
        return $review;
    }

    /**
     * @param $id
     * @return App\Entity\Reviews\Review
     */
    public function getReviewsForUser($id): Review
    {
        return Review::where('user_id', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
    }

}
