<?php

namespace App\Services\Offers;

use App\DTO\Offers\OfferCreateDto;
use App\DTO\Offers\OfferUpdateDto;
use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferLink;
use App\Entity\Users\User;
use App\Events\OfferCreated;
use App\Filters\OffersFilter;
use App\Jobs\OfferCreatedSendAdminNotifications;
use App\Jobs\OfferEndedSendNotifications;
use App\Notifications\OfferActivated;
use App\Notifications\OfferBlocked;
use App\Notifications\OfferEnded;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;

class OffersService {

    /**
     * @param $id
     * @return Offer
     */
    public function find($id): Offer
    {
        return Offer::findOrFail($id);
    }

    /**
     * Create offer from DTO
     * @param OfferCreateDto $dto
     * @return Offer
     * @throws \Throwable
     */
    public function create(OfferCreateDto $dto): Offer
    {
        DB::beginTransaction();

        try {
            $offer = new Offer;
            $offer->name = $dto->getName();
            $offer->description = $dto->getDescription();
            $offer->age_from = $dto->getAgeFrom();
            $offer->age_to = $dto->getAgeTo();
            $offer->weight_from = $dto->getWeightFrom();
            $offer->weight_to = $dto->getWeightTo();
            $offer->height_from = $dto->getHeightFrom();
            $offer->height_to = $dto->getHeightTo();
            $offer->boobs_from = $dto->getBoobsFrom();
            $offer->boobs_to = $dto->getBoobsTo();
            $offer->only_verified = $dto->isOnlyVerified();
            $offer->meeting_date = Carbon::parse($dto->getMeetingDate())->format('Y-m-d');
            $offer->nearest_city_range = $dto->getNearestCityRange();
            $offer->search_male = $dto->getGenders()['male'];
            $offer->search_female = $dto->getGenders()['female'];
            $offer->cost = 0;

            $offer->user()->associate($dto->getUser());
            $offer->country()->associate($dto->getCountry());
            $offer->city()->associate($dto->getCity());
            $offer->category()->associate(1);//$dto->getCategory());

            // если поиск в городе проведения
            if ($dto->isSearchFromSameCity()) {
                $offer->countryFrom()->associate($dto->getCountry());
                $offer->cityFrom()->associate($dto->getCity());
            } else {
                if ($countryFrom = $dto->getCountryFrom())
                    $offer->countryFrom()->associate($countryFrom);
                if ($cityFrom = $dto->getCityFrom())
                    $offer->cityFrom()->associate($cityFrom);
            }

            $offer->status = Offer::STATUS_INSPECTION;

            $offer->saveOrFail();

            if ($dto->getNationalities())
                $offer->nationalities()->sync($dto->getNationalities());

            if ($dto->getEyes())
                $offer->eyes()->sync($dto->getEyes());

            if ($dto->getHairs())
                $offer->hairs()->sync($dto->getHairs());

            if ($visas = $dto->getVisas())
                $offer->visas()->sync($visas);

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();

        // отправляем оповещения админам
        dispatch(new OfferCreatedSendAdminNotifications($offer));

        return $offer;
    }

    public function setViewed(Offer $offer, User $user) {
        $user->viewedOffers()->syncWithoutDetaching([$offer->id]);
        return true;
    }

    /**
     * Update offer from DTO
     * @param Offer $offer
     * @param OfferUpdateDto $dto
     * @return bool
     * @throws \Throwable
     */
    public function update(Offer $offer, OfferUpdateDto $dto): bool
    {
        DB::beginTransaction();

        try {
            if ($dto->getName())
                $offer->name = $dto->getName();
            if ($dto->getDescription())
                $offer->description = $dto->getDescription();
            if ($dto->getAgeFrom())
                $offer->age_from = $dto->getAgeFrom();
            if ($dto->getAgeTo())
                $offer->age_to = $dto->getAgeTo();
            if ($dto->getWeightFrom())
                $offer->weight_from = $dto->getWeightFrom();
            if ($dto->getWeightTo())
                $offer->weight_to = $dto->getWeightTo();
            if ($dto->getHeightFrom())
                $offer->height_from = $dto->getHeightFrom();
            if ($dto->getHeightTo())
                $offer->height_to = $dto->getHeightTo();
            if ($dto->getBoobsFrom())
                $offer->boobs_from = $dto->getBoobsFrom();
            if ($dto->getBoobsTo())
                $offer->boobs_to = $dto->getBoobsTo();
            if ($dto->getCost())
                $offer->cost = $dto->getCost();
            /*if ($dto->isPossibleIncreaseCost() !== null)
                $offer->possible_increase_cost = $dto->isPossibleIncreaseCost();*/
            if ($dto->isOnlyWithReviews() !== null)
                $offer->only_with_reviews = $dto->isOnlyWithReviews();
            if ($dto->isOnlyVerified() !== null)
                $offer->only_verified = $dto->isOnlyVerified();
            if ($dto->getMeetingDate())
                $offer->meeting_date = Carbon::parse($dto->getMeetingDate())->format('Y-m-d');
            if ($dto->getBoobsType())
                $offer->boobs_type = $dto->getBoobsType();
            if ($dto->getNationalities())
                $offer->nationalities()->sync($dto->getNationalities());
            if ($dto->getEyes())
                $offer->eyes()->sync($dto->getEyes());
            if ($dto->getHairs())
                $offer->hairs()->sync($dto->getHairs());
            if ($dto->getCountry())
                $offer->country()->associate($dto->getCountry());
            if ($dto->getCity())
                $offer->city()->associate($dto->getCity());
            if ($visas = $dto->getVisas())
                $offer->visas()->sync($visas);

            $offer->saveOrFail();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
        return true;
    }

    /**
     * @param int $count
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(int $count=20, array $params=[])
    {
        $filter = new OffersFilter($params);
        $offers = Offer::with(['user', 'category', 'country', 'city'])
            ->orderBy('offers.created_at', 'desc')
            ->filter($filter)
            ->paginate($count);

        return $offers;
    }

    /**
     * Pagination offers received by model
     * @param User $user
     * @param int $count
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateOffersForModel(User $user, $params, $count=20)
    {
        $filter = new OffersFilter($params);
        $offers = Offer::with(['user', 'category', 'country', 'city', 'application'])
            ->filter($filter)
            ->where('offers.status', Offer::STATUS_ACTIVE)
            ->orderBy('offers.created_at', 'desc')
            ->paginate($count);

        return $offers;
    }

    /**
     * Paginate offers created by user
     * @param User $user
     * @param int $count
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateOwnOffers(User $user, int $count=20)
    {
        $offers = Offer::with(['user', 'category', 'country', 'city', 'application'])
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate($count);

        return $offers;
    }

    /**
     * @param User $user
     * @param int $id
     * @return Offer
     */
    public function fetchOfferForManager(User $user, int $id): Offer
    {
        $offer = Offer::with([
            'user',
            'category',
            'country',
            'city',
            'cityFrom',
            'countryFrom',
            'nationalities',
            'eyes',
            'hairs',
            'visas',
            'agreedModels',
            'agreedByManagerModels',
            'application'
        ]);

        if(!$user->isAdmin())
            $offer = $offer->where('user_id', $user->id);

        $offer = $offer->findOrFail($id);

        return $offer;
    }

    /**
     * @param User $user
     * @param int $id
     * @return mixed
     */
    public function fetchOfferForModel(User $user, $id): Offer
    {
        $offer = Offer::with(['user', 'category', 'country', 'city', 'application', 'usersGoing'])
            ->where('offers.id', $id)
            ->firstOrFail();

        return $offer;
    }

    /**
     * Отправить на проверку
     * @param Offer $offer
     */
    public function setInspection(Offer $offer)
    {
        // todo
    }

    /**
     * @param Offer $offer
     * @return bool
     * @throws \Throwable
     */
    public function setActive(Offer $offer): bool
    {
        $activateResult = $offer->activate();

        // если активирован оффер, то запускаем рассылку
        if($activateResult) {
            // оповещаем менеджера
            $offer->user->notify(new OfferActivated($offer));
            $offer->started_at = now();
            $offer->save();
        }

        return $activateResult;
    }

    /**
     * @param Offer $offer
     * @return bool
     * @throws \Throwable
     */
    public function setEnded(Offer $offer)
    {
        $changeStatusResult = $offer->setEnded();

        if($changeStatusResult) {
            // отправляем оповещение создателю
            $offer->user->notify(new OfferEnded($offer));
            // отправляем уведомления моделям
            OfferEndedSendNotifications::dispatch($offer);
        }

        return $changeStatusResult;
    }

    /**
     * Заблокировать предложение
     * @param Offer $offer
     * @param string $reason
     * @return bool
     * @throws \Throwable
     */
    public function setBlocked(Offer $offer, string $reason): bool
    {
        $changeStatusResult = $offer->block($reason);

        // если оффер заблокирован, то отправляем уведомление создателю
        if($changeStatusResult && $offer->user) {
            $offer->user->notify(new OfferBlocked($offer));
        }

        return $changeStatusResult;
    }

    /**
     * Закрыть предложение
     * @param Offer $offer
     * @return bool
     * @throws \Throwable
     */
    public function close(Offer $offer): bool
    {
        $changeStatusResult = $offer->close();

        // если оффер приостановлен, то отправляем уведомления моделям
        if($changeStatusResult) {
            OfferEndedSendNotifications::dispatch($offer)
                ->onQueue('notifications');
        }

        return $changeStatusResult;
    }

    /**
     * Завершить предложение
     * @param Offer $offer
     * @return bool
     * @throws \Throwable
     */
    public function end(Offer $offer): bool
    {
        $offer->status = Offer::STATUS_ENDED;
        $offer->saveOrFail();

        /*// если оффер приостановлен, то отправляем уведомления моделям
        if($changeStatusResult) {
            OfferEndedSendNotifications::dispatch($offer)
                ->onQueue('notifications');
        }*/

        return true;//$changeStatusResult;
    }

    /**
     * @param Offer $offer
     * @return bool
     * @throws \Exception
     */
    public function delete(Offer $offer): bool
    {
        return $offer->delete();
    }

}
