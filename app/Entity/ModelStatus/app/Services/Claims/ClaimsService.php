<?php

namespace App\Services\Claims;

use App\DTO\Claims\ClaimCreateDto;
use App\Entity\Claims\Claim;

class ClaimsService {

    /**
     * @param ClaimCreateDto $dto
     * @return Claim
     * @throws \Throwable
     */
    public function create(ClaimCreateDto $dto): Claim
    {
        $claim = new Claim;
        $claim->content = $dto->getContent();
        $claim->user()->associate($dto->getUser());
        $claim->claimable()->associate($dto->getClaimable());
        $claim->saveOrFail();
        return $claim;
    }

}
