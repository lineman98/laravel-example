<?php

namespace App\Services\Tours;

use App\DTO\Offers\OfferCreateDto;
use App\DTO\Offers\OfferUpdateDto;
use App\DTO\Tours\TourCreateDto;
use App\DTO\Tours\TourUpdateDto;
use App\Entity\Offers\Offer;
use App\Entity\Offers\OfferLink;
use App\Entity\Tours\Tour;
use App\Entity\Users\User;
use App\Filters\ToursFilter;
use App\Jobs\OfferEndedSendNotifications;
use App\Jobs\SearchModelsToOffer;
use App\Notifications\OfferActivated;
use App\Notifications\OfferBlocked;
use App\Notifications\TourActivated;
use App\Notifications\TourBlocked;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ToursService {

    /**
     * @param $id
     * @return Tour
     */
    public function find($id): Tour
    {
        return Tour::findOrFail($id);
    }

    /**
     * @param $id
     * @return Tour
     */
    public function findActive($id): Tour
    {
        return Tour::where('status', Tour::STATUS_ACTIVE)
            ->findOrFail($id);
    }

    public function fetchTourForManager(User $user, int $id): Tour
    {
        $tour = Tour::with(['user', 'country']);

        if(!$user->isAdmin())
            $tour = $tour->where('user_id', $user->id);

        $tour = $tour->findOrFail($id);

        return $tour;
    }

    /**
     * @param TourCreateDto $dto
     * @return Tour
     * @throws \Throwable
     */
    public function create(TourCreateDto $dto): Tour
    {
        $tour = new Tour;
        $tour->description = $dto->getDescription();
        $tour->status = Tour::STATUS_INSPECTION;
        $tour->country()->associate($dto->getCountry());
        $tour->user()->associate($dto->getUser());
        $tour->saveOrFail();
        return $tour;
    }

    /**
     * @param Tour $tour
     * @param TourUpdateDto $dto
     * @return bool
     * @throws \Throwable
     */
    public function update(Tour $tour, TourUpdateDto $dto): bool
    {
        $tour->description = $dto->getDescription();
        $tour->country()->associate($dto->getCountry());
        return $tour->saveOrFail();
    }

    /**
     * @param int $count
     * @param array $params
     * @return mixed
     */
    public function paginate(int $count=20, array $params=[])
    {
        $filter = new ToursFilter($params);
        $tours = Tour::with(['user', 'country'])
            ->filter($filter)
            ->orderBy('tours.created_at', 'desc')
            ->paginate($count);
        return $tours;
    }

    public function paginateToursForModel(int $count=20)
    {
        $tours = Tour::with(['user', 'country'])
            ->where('status', Tour::STATUS_ACTIVE)
            ->orderBy('created_at', 'desc')
            ->paginate($count);
        return $tours;
    }

    /**
     * @param User $user
     * @param int $count
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateOwnOffers(User $user, int $count=20)
    {
        $tours = Tour::with(['user', 'country'])
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate($count);
        return $tours;
    }

    /**
     * @param Tour $tour
     * @return bool
     * @throws \Throwable
     */
    public function setInspection(Tour $tour): bool
    {
        $result = $tour->setInspection();
        return $result;
    }

    /**
     * @param Tour $tour
     * @return bool
     * @throws \Throwable
     */
    public function setActive(Tour $tour): bool
    {
        $activateResult = $tour->activate();

        // если активирован оффер, то запускаем рассылку
        if($activateResult) {
            // оповещаем менеджера
            $tour->user->notify(new TourActivated($tour));
        }

        return $activateResult;
    }

    /**
     * @param Tour $tour
     * @return bool
     * @throws \Throwable
     */
    public function setBlocked(Tour $tour)
    {
        $changeStatusResult = $tour->block();

        // если оффер заблокирован, то отправляем уведомление создателю
        if($changeStatusResult && $tour->user) {
            $tour->user->notify(new TourBlocked($tour));
        }

        return $changeStatusResult;
    }

    /**
     * @param Tour $tour
     * @return bool
     * @throws \Throwable
     */
    public function close(Tour $tour)
    {
        $changeStatusResult = $tour->close();
        return $changeStatusResult;
    }

    /**
     * @param Tour $tour
     * @return bool
     * @throws \Exception
     */
    public function delete(Tour $tour): bool
    {
        return $tour->delete();
    }

}
