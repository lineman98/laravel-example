<?php

namespace App\Services\Users;

use App\DTO\Users\UserCreateDto;
use App\Entity\ModelStatus\ModelStatus;
use App\Entity\Users\User;
use App\Events\UserRegistered;
use App\Exceptions\ValidationException;
use App\Jobs\UserRegisteredSendAdminNotifications;
use App\Services\Invites\InvitesService;
use App\Services\Verification\PhoneVerification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class RegisterService
{

    /** @var PhoneVerification */
    private $phoneVerification;

    /** @var InvitesService */
    private $invitesService;

    public function __construct(PhoneVerification $phoneVerification, InvitesService $invitesService)
    {
        $this->phoneVerification = $phoneVerification;
        $this->invitesService = $invitesService;
    }

    /**
     * @param UserCreateDto $dto
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     * @throws ValidationException
     * @throws \Throwable
     */
    public function register(UserCreateDto $dto)
    {
        $user = User::where('phone', $dto->getPhone())->first();

        if ($user)
            throw new ValidationException('Phone already registered');

        DB::beginTransaction();

        try {
            $user = new User;
            $user->name = $dto->getName();
            $user->phone = $dto->getPhone();
            $user->password = Hash::make($dto->getPassword());
            $user->role = $dto->getRole();
            $user->status = $dto->getStatus();
            $user->verified = User::NOT_VERIFIED;
            $user->birthday = Carbon::createFromFormat('d-m-Y', $dto->getBirthday());
            $user->gender = $dto->getGender();
            $user->can_create_offers = true;
            $user->saveOrFail();

            /*
            // выбираем тарифный план
            if ($user->isGirl()) {
                $plan = app('rinvex.subscriptions.plan')
                    ->find(config('plans.model_plan_id'));
            } else {
                $plan = app('rinvex.subscriptions.plan')
                    ->find(config('plans.manager_plan_id'));
            }

            // создаем подписку
            $subscription = $user->newSubscription('main', $plan);
            if ($user->isGirl()) {
                // у моделей ставим доступ +1 год
                $subscription->ends_at = now()->addYear();
                $user->subscription_payed = true;
            } else {
                // у менеджеров сразу завершаем подписку (это костыль)
                $subscription->ends_at = now();
            }
            $subscription->save();
            $user->subscription_slug = $subscription->slug;
            $user->save();
            */

            $plan = app('rinvex.subscriptions.plan')->find(config('plans.manager_plan_id'));
            $subscription = $user->newSubscription('main', $plan);
            $subscription->ends_at = now()->addYear();
            $subscription->save();

            $user->subscription_payed = true;
            $user->subscription_slug = $subscription->slug;
            $user->save();

            // активируем инвайт            $plan = app('rinvex.subscriptions.plan')->find(config('plans.manager_plan_id'));
            $subscription = $user->newSubscription('main', $plan);
            $subscription->ends_at = now()->addYear();
            $subscription->save();
            if ($invite = $dto->getInvite()) {
                $this->invitesService->applyInviteForUser($invite, $user);
                $this->invitesService->createInviteCloneForUser($invite, $user);
            } else {
                $this->invitesService->createDefaultInviteForUser($user);
                // проставляем дефолтный статус модели
                $modelStatus = ModelStatus::query()->first();
                if ($modelStatus)
                    $user->modelStatuses()->sync([$modelStatus->id]);
            }

            DB::commit();

            // отправляем оповещение админам
            dispatch(new UserRegisteredSendAdminNotifications($user));

            return $user;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
