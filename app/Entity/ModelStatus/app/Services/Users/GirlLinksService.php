<?php

namespace App\Services\Users;

use App\Entity\Users\GirlLink;
use App\Entity\Users\User;
use Illuminate\Support\Str;

class GirlLinksService
{

    /**
     * @param User $girl
     * @param User $creator
     * @return GirlLink
     * @throws \Throwable
     */
    public function createLinkToGirl(User $girl, User $creator): GirlLink
    {
        // проверяем, есть ли связь между пользователями
        $hasAccess = $this->hasAccess($girl->id, $creator);
        if (!$hasAccess)
            throw new \Exception('User doesnt have access');

        $link = GirlLink::where([
            'girl_id' => $girl->id,
            'user_id' => $creator->id,
            'status' => GirlLink::STATUS_ACTIVE,
        ])->first();

        if ($link)
            return $link;

        $link = new GirlLink;
        $link->alias = md5(Str::random());
        $link->status = GirlLink::STATUS_ACTIVE;
        $link->expires_on = now()->addHours(2);
        $link->user()->associate($creator);
        $link->girl()->associate($girl);
        $link->saveOrFail();

        return $link;
    }

    /**
     * @param string $alias
     * @return mixed
     */
    public function getGirlByLinkAlias(string $alias): User
    {
        $link = GirlLink::with('girl')
            ->where('alias', $alias)
            ->where('status', GirlLink::STATUS_ACTIVE)
            ->firstOrFail();

        return $link->girl;
    }

    /**
     * @param $id
     * @param User $userFrom
     * @return bool
     */
    public function hasAccess($id, User $userFrom): bool
    {
        // если создает ссылку на себя
        if ($id == $userFrom->id)
            return true;

        // проверка, получал ли пользователь $userFrom предложение от юзера $id
        $firstCheckUser = User::join('offers', 'offers.user_id', 'users.id')
            ->join('offer_links', 'offer_links.offer_id', 'offers.id')
            ->where('offer_links.user_id', $userFrom->id)
            ->where('users.id', $id)
            ->selectRaw('users.*')
            ->first();

        if ($firstCheckUser)
            return true;

        // проверка, создавал ли пользователь $userFrom предложение, которое получил $id
        $secondCheckUser = User::join('offer_links', 'offer_links.user_id', 'users.id')
            ->join('offers', 'offers.id', 'offer_links.offer_id')
            ->where('offer_links.user_id', $id)
            ->where('offers.user_id', $userFrom->id)
            ->where('users.id', $id)
            ->selectRaw('users.*')
            ->first();

        return !!$secondCheckUser;
    }

}
