<?php

namespace App\Services\Users;

use App\DTO\Users\PushTokenCreateDto;
use App\DTO\Users\UserUpdateDto;
use App\Entity\Users\User;
use App\Entity\Users\UserExpoToken;
use App\Events\ManagerRequestVerify;
use App\Events\UserSendVerifyPhoto;
use App\Filters\UsersFilter;
use App\Helpers\HeicConverter;
use App\Jobs\SendVerifyPhotoNotification;
use App\Jobs\UserVerifyPhotoAdminNotifications;
use App\Notifications\GiveAccessOffersCreate;
use App\Notifications\GiveAccessToursCreate;
use App\Notifications\MediaChecked;
use App\Notifications\UserBlocked;
use App\Notifications\UserNotVerified;
use App\Notifications\UserVerified;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Image\Image;
use Spatie\MediaLibrary\Models\Media;

class UsersService
{

    /**
     * @param int $id
     * @return User
     */
    public function find(int $id): User
    {
        return User::findOrFail($id);
    }

    /**
     * Поиск пользователя с учетом контакта через предложение в прошлом
     * @param $id
     * @param User $userFrom
     * @return User
     */
    public function findUserFromUser($id, User $userFrom): User
    {
        if ($userFrom->id == $id || $userFrom->isAdmin())
            return $this->find($id);

        // проверка, получал ли пользователь $userFrom предложение от юзера $id
        $firstCheckUser = User::join('offers', 'offers.user_id', 'users.id')
                ->join('offer_users', 'offer_users.offer_id', 'offers.id')
                ->where('offer_users.user_id', $userFrom->id)
                ->where('users.id', $id)
                ->selectRaw('users.*')
                ->first();

        if ($firstCheckUser)
            return $firstCheckUser;

        // проверка, создавал ли пользователь $userFrom предложение, которое получил $id
        $secondCheckUser = User::join('offer_users', 'offer_users.user_id', 'users.id')
            ->join('offers', 'offers.id', 'offer_users.offer_id')
            ->where('offer_users.user_id', $id)
            ->where('offers.user_id', $userFrom->id)
            ->where('users.id', $id)
            ->selectRaw('users.*')
            ->firstOrFail();

        return $secondCheckUser;
    }

    /**
     * @param string $phone
     * @return mixed
     */
    public function findByPhone(string $phone): User
    {
        return User::where('phone', $phone)->firstOrFail();
    }

    /**
     * Change users password
     * @param User $user
     * @param string $new_password
     * @return bool
     * @throws \Throwable
     */
    public function changePassword(User $user, string $new_password): bool
    {
        $user->password = Hash::make($new_password);
        return $user->saveOrFail();
    }

    /**
     * @param User $user
     * @param UserUpdateDto $dto
     * @return bool
     * @throws \Throwable
     */
    public function update(User $user, UserUpdateDto $dto)
    {
        DB::beginTransaction();
        try {
            $user->name = $dto->getName();
            $user->whatsapp = $dto->getWhatsapp();

            if ($dto->getRole())
                $user->role = $dto->getRole();

            $user->aboutme = $dto->getAboutme();

            // изменения для девушек
            if ($user->isGirl()) {
                $user->country()->associate($dto->getCountry());
                $user->city()->associate($dto->getCity());
                $user->age = $dto->getAge();
                $user->boobs_size = $dto->getBoobsSize();
                $user->boobs_type = $dto->getBoobsType();
                $user->weight = $dto->getWeight();
                $user->height = $dto->getHeight();
                $user->bust = $dto->getBust();
                $user->waist = $dto->getWaist();
                $user->hip = $dto->getHip();
                $user->has_foreign_passport = $dto->isHasForeignPassport();
                $user->minimum_cost = $dto->getMinimumCost();


                if ($dto->getGender())
                    $user->gender = $dto->getGender();

                if ($dto->getEye()) {
                    $user->eye()->associate($dto->getEye());
                } else {
                    $user->eye_id = null;
                }

                if ($dto->getHair()) {
                    $user->hair()->associate($dto->getHair());
                } else {
                    $user->hair_id = null;
                }

                if ($dto->getNationality()) {
                    $user->nationality()->associate($dto->getNationality());
                } else {
                    $user->nationality_id = null;
                }

                // статусы, от которых может получать предложения
                $statuses = $dto->getModelStatuses();
                if (is_array($statuses)) {
                    $user->modelStatuses()->sync($statuses);
                }

                $visas = $dto->getVisas();
                if (is_array($visas)) {
                    $user->visas()->sync($visas);
                }

            }

            $user->website = $dto->getWebsite();

            // статусы, которым может отправлять предложения
            $statuses = $dto->getStatuses();
            if (is_array($statuses)) {
                $user->accessStatuses()->sync($statuses);
            }

            // категории, от которых модель хочет получать предложения
            $offer_categories = $dto->getOfferCategories();
            if (is_array($offer_categories)) {
                $user->offerCategories()->sync($offer_categories);
            }

            $createOffersNotitficationSend = false;
            $createToursNotitficationSend = false;

            if ($dto->canCreateOffers() !== null) {
                // если раньше не было доступа к созданию предложений
                if ($dto->canCreateOffers() && !$user->canCreateOffers())
                    $createOffersNotitficationSend = true;
                $user->can_create_offers = $dto->canCreateOffers();
            }

            if ($dto->canCreateTours() !== null) {
                // если раньше не было доступа к созданию туров
                if ($dto->canCreateTours() && !$user->canCreateTours())
                    $createToursNotitficationSend = true;
                $user->can_create_tours = $dto->canCreateTours();
            }

            // активируем пользователя (если не заблокирован)
            $user->activate();
            $result = $user->saveOrFail();

            DB::commit();

            // очищаем краткую инфу о пользователе с кеша
            Cache::pull('user_meta_' . $user->id);
            // оповещаем о выдачи доступа к созданию предложений
            if ($createOffersNotitficationSend)
                $user->notify(new GiveAccessOffersCreate);
            // оповещаем о выдачи доступа к созданию туров
            if ($createToursNotitficationSend)
                $user->notify(new GiveAccessToursCreate);

            // отправляем оповещение, если анкета еще не проверена
            if ($user->isNotVerified()) {
                dispatch((new SendVerifyPhotoNotification($user))->delay(random_int(60, 120)));
            }

            return $result;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function paginate(int $count, array $params=[])
    {
        $filter = new UsersFilter($params);
        return User::with('country', 'city', 'avatar')
            ->filter($filter)
            ->orderBy('created_at', 'desc')
            ->paginate($count);
    }

    public function chunkWithFilter(int $count, array $params=[], callable $callback)
    {
        $filter = new UsersFilter($params);
        return User::filter($filter)
            ->chunkById($count, $callback);
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Throwable
     */
    public function setVerifiedPhone(User $user)
    {
        return $user->setVerifiedPhone();
    }

    /**
     * Отправка на проверку
     * @param User $user
     * @throws \Throwable
     */
    public function setWaitVerify(User $user)
    {
        if ($user->setWaitVerify()) {
            // отправляем оповещения админам
            dispatch(new UserVerifyPhotoAdminNotifications($user));
        }
    }

    /**
     * Отправка на проверку менеджера
     * @param User $user
     * @throws \Throwable
     */
    public function setManagerWaitVerify(User $user)
    {
        if ($user->isNotVerified()) {
            $user->setWaitVerify();
            dispatch(new UserVerifyPhotoAdminNotifications($user));
        }
    }

    /**
     * Блокировка пользователя
     * @param User $user
     */
    public function setBlocked(User $user)
    {
        if ($user->block())
            $user->notify(new UserBlocked);
    }

    /**
     * Upload photo for user
     *
     * @param UploadedFile $file
     * @param User $user
     * @return Media
     * @throws \Exception
     */
    public function uploadPhoto(UploadedFile $file, User $user): Media
    {
        $heic_mimes = [
            'application/octet-stream',
            'image/heif',
            'image/heic',
            'image/HEIF',
            'image/HEIC',
            'image/heif-sequence',
            'image/heic-sequence'
        ];

        $extension = $file->extension();
        if(in_array($file->getMimeType(), $heic_mimes)) {
            $file = HeicConverter::convertToJPEG($file->path());
            $extension = 'jpg';
        }

        $added_media = $user->addMedia($file)
            ->withCustomProperties(['checked' => false])
            ->usingFileName(md5(random_bytes(6)) . '.' . $extension)
            ->toMediaCollection(User::PUBLIC_PHOTOS);

        // проставляем как аватарку, если еще нет проставленной аватарки
        $avatar = $user->getFirstMedia(User::PUBLIC_PHOTOS, ['avatar' => true]);
        if(!$avatar) {
            $added_media->setCustomProperty('avatar', true)->save();
        }

        return $added_media;
    }

    /**
     * Отправка фотографии на проверку
     * @param UploadedFile $file
     * @param User $user
     * @return Media
     * @throws \Throwable
     */
    public function uploadVerifyPhoto(UploadedFile $file, User $user): Media
    {
        if (!$user->isNotVerified())
            throw new \Exception('Фотография для проверки уже отправлена');

        $heic_mimes = [
            'application/octet-stream',
            'image/heif',
            'image/heic',
            'image/HEIF',
            'image/HEIC',
            'image/heif-sequence',
            'image/heic-sequence'
        ];

        $extension = $file->extension();
        if(in_array($file->getMimeType(), $heic_mimes)) {
            $file = HeicConverter::convertToJPEG($file->path());
            $extension = 'jpg';
        }

        // берем прошлую фотку
        $prevVerifyPhoto = $user->getFirstMedia(User::VERIFY_PHOTOS);

        // добавляем новую фотку
        $added_media = $user->addMedia($file)
            ->usingFileName(md5(random_bytes(6)) . '.' . $extension)
            ->toMediaCollection(User::VERIFY_PHOTOS);

        // удаляем прошлую фотку
        if($prevVerifyPhoto) {
            $prevVerifyPhoto->delete();
        }

        // отправляем на проверку админам
        $this->setWaitVerify($user);

        return $added_media;
    }

    /**
     * Загрузка видео для пользователя
     *
     * @param UploadedFile $file
     * @param User $user
     * @return Media
     * @throws \Exception
     */
    public function uploadVideo(UploadedFile $file, User $user): Media
    {
        $added_media = $user->addMedia($file)
            ->withCustomProperties(['checked' => false])
            ->usingFileName(md5(random_bytes(6)) . '.' . $file->extension())
            ->toMediaCollection(User::PUBLIC_VIDEOS);
        return $added_media;
    }

    /**
     * Delete user's photo
     * @param User $user
     * @param $media_id
     * @return bool
     */
    public function removePhoto(User $user, $media_id): bool
    {
        try {
            /** @var Media $media */
            $media = Media::where([
                'model_type' => 'users',
                'model_id' => $user->id,
                'id' => $media_id
            ])->firstOrFail();
            $is_avatar = $media->getCustomProperty('avatar', false);
            $media->delete();

            // если это была аватарка
            if ($is_avatar) {
                // ищем другую фотку и ставим ее аватаркой
                $photo = $user->getMedia(User::PUBLIC_PHOTOS)->first();
                if ($photo) {
                    $photo->setCustomProperty('avatar', true)->save();
                }
            }
        } catch (\Throwable $exception) {
            return false;
        }
        return true;
    }

    /**
     * Delete user's photo
     * @param User $user
     * @param $media_id
     * @return bool
     */
    public function checkMedia(User $user, $media_id): bool
    {
        try {
            /** @var Media $media */
            $media = Media::where([
                'model_type' => 'users',
                'model_id' => $user->id,
                'id' => $media_id
            ])->firstOrFail();
            $media->setCustomProperty('checked', true)->save();
            $user->notify(new MediaChecked($media));
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * @param User $user
     * @param $media_id
     * @return bool
     */
    public function setAvatar(User $user, $media_id): bool
    {
        try {
            $media = Media::where([
                'model_type' => get_class($user),
                'model_id' => $user->id,
                'id' => $media_id
            ])->firstOrFail();

            // прошлую аватарку убираем
            $previous_avatar = $user->getFirstMedia(User::PUBLIC_PHOTOS, ['avatar' => true]);
            if($previous_avatar)
                $previous_avatar->setCustomProperty('avatar', false)->save();
            // выбранную фотку ставим аватаркой
            $media->setCustomProperty('avatar', true)->save();
            // пробуем активировать профиль
            // $girl->activate();
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * @param User $user
     * @param PushTokenCreateDto $dto
     * @return UserExpoToken
     * @throws \Throwable
     */
    public function addPushToken(User $user, PushTokenCreateDto $dto)
    {
        $token = UserExpoToken::where('token', $dto->getToken())->first();

        if($token) {
            // если такой токен уже есть (но у другого пользователя), то удаляем
            if($token->user_id === $user->id) {
                return $token;
            } else {
                $token->delete();
            }
        }

        $token = new UserExpoToken;
        $token->token = $dto->getToken();
        $token->device = $dto->getDevice();
        $token->os = $dto->getOs();
        $token->user()->associate($user);
        $token->saveOrFail();
        Cache::pull('push_tokens_user_'.$user->id);
        return $token;
    }

    /**
     * @param User $user
     * @param PushTokenCreateDto $dto
     * @return bool
     * @throws \Throwable
     */
    public function removePushToken(User $user, PushTokenCreateDto $dto): bool
    {
        $token = UserExpoToken::where('token', $dto->getToken())
            ->where('user_id', $user->id)
            ->first();

        // если такой токен уже есть, то удаляем его (чтобы не было дубля)
        if($token) {
            $token->delete();
            Cache::pull('push_tokens_user_'.$user->id);
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Throwable
     */
    public function setVerified(User $user): bool
    {
        $result = $user->setVerified();
        if($result)
            $user->notify(new UserVerified);

        // увеличиваем бонусный баланс у реферрера
        if ($user->referrer) {
            $bonusService = new ReferralBonusService;
            $bonusService->incrementBonuses($user->referrer, $user);
        }

        return $result;
    }

    /**
     * @param User $user
     * @param string $reason
     * @return bool
     * @throws \Throwable
     */
    public function setUnverified(User $user, string $reason): bool
    {
        $result = $user->setNotVerified($reason);
        if($result)
            $user->notify(new UserNotVerified($reason));
        return $result;
    }

    /**
     * @param User $user
     * @param string|null $reason
     * @return bool
     * @throws \Throwable
     */
    public function delete(User $user, ?string $reason): bool
    {
        if ($reason) {
            $user->delete_reason = $reason;
            $user->saveOrFail();
        }
        return $user->delete();
    }

}
