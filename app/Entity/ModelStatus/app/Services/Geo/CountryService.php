<?php

namespace App\Services\Geo;

use App\Entity\Geo\Country\Country;
use App\Http\Requests\Countries\CreateRequest;
use App\Http\Requests\Countries\UpdateRequest;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Cache;

class CountryService
{

    protected $country;

    public function __construct(Country $country)
    {
        $this->country = $country;
    }

    public function create($name, $slug): Country
    {
        return $this->country->create([
            'name' => $name,
            'slug' => $slug
        ]);
    }

    public function createFromRequest(CreateRequest $request): Country
    {
        $country = $this->country->create($request->all());
        Cache::tags(Country::class)->put('country_'.$country->id, $country);
        return $country;
    }

    public function getAllCountries()
    {
        return Cache::tags(Country::class)->rememberForever('all_countries', function () {
            return Country::with('translations')
                ->get();
        });
    }

    public function getAllCountriesWithCities()
    {
        return Cache::tags(Country::class)->rememberForever('all_countries_cities', function () {
            $translationTable = 'country_translations';
            $locale = app()->getLocale();
            return Country::with(['translations', 'cities'])
                ->join($translationTable, function (JoinClause $join) use ($translationTable, $locale) {
                    $join->on($translationTable.'.country_id', '=', 'countries.id')
                        ->where($translationTable.'.locale', $locale);
                })
                ->selectRaw('countries.*, country_translations.name')
                ->orderBy($translationTable.'.name', 'asc')
                ->get();
        });
    }

    public function updateFromRequest(UpdateRequest $request, $id): bool
    {
        $country = $this->country->findOrFail($id);
        $status = $country->update($request->all());
        Cache::tags(Country::class)->put('country_'.$country->id, $country);
        return $status;
    }

    public function find($id): Country
    {
        return Cache::tags(Country::class)->remember('country_'.$id, 30, function () use ($id) {
            return Country::with(['translations', 'cities'])->findOrFail($id);
        });
    }

    public function findForAdmin($id): Country
    {
        return $this->country->active()
            ->findOrFail($id);
    }

}
