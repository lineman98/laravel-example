<?php

namespace App\Events;

use App\Entity\Offers\Offer;
use App\Http\Resources\Offers\OfferResource;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class OfferCreated implements ShouldBroadcast
{

    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $offer;

    public function __construct(Offer $offer)
    {
        $this->offer = new OfferResource($offer);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('Admin'),
        ];
    }

    public function broadcastAs()
    {
        return 'OfferCreated';
    }

}
