#!/usr/bin/env bash

sudo systemctl stop apache2 nginx mysql redis
docker-compose up -d  nginx phpmyadmin mysql redis laravel-echo-server php-fpm
