<?php

use Illuminate\Database\Seeder;

use App\Entity\Faq\Faq;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ru = [
            [
                'question' => 'Зачем нужна анкета?',
                'answer' => 'Анкета нужна чтобы агентства смогли посмотреть ваши параметры и фотографии. Без заполненной анкеты вы даже не будете получать предложения.',
            ],
            [
                'question' => 'Зачем заполнять Whatsapp?',
                'answer' => 'Как правило, агентам удобнее связываться с моделями по Whatsapp. Если вам тоже удобно связываться в Whatsapp, заполните это поле.',
            ],
            [
                'question' => 'Какую страну и город выбрать?',
                'answer' => 'Нужно выбирать страну и город, в которых вы находитесь на данный момент. Вы будете получать предложения, подходящие для вашего местоположения из анкеты. Важно менять страну и город, если вы переехали или путешествуете чтобы получать подходящие предложения.'
            ],
            [
                'question' => 'Почему так много параметров?',
                'answer' => 'Агентства ищут моделей по многим параметрам, таким как рост, вес, цвет волос, глаз и так далее. Например, на показ одежды могут потребоваться модели plus size, поэтому агентству будет удобно настроить поиск на моделей на определенные параметры.'
            ],
            [
                'question' => 'Мою анкету видят все?',
                'answer' => 'Нет. Вашу анкету видит только создатель предложения (агент) только в том случае, если вы подойдете под параметры его предложения.'
            ],
            [
                'question' => 'Когда я получу предложения?',
                'answer' => 'Когда заполните анкету, загрузите фотографии и пройдете проверку. После этого вам начнут приходить наиболее подходящие для вас предложения.'
            ],
            [
                'question' => 'А я смогу удалиться?',
                'answer' => 'Да. Вы в любой момент можете удалить свою анкету в разделе Настройки.'
            ]
        ];

        foreach ($ru as $el) {
            Faq::create([
                'ru' => $el
            ]);
        }
    }
}
