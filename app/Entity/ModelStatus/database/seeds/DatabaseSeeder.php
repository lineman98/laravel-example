<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            GeoSeed::class,
            OfferCategorySeed::class,
            EyesSeed::class,
            HairsSeed::class,
            NationalitiesSeed::class,
            PurposesSeed::class,
            StatusesSeed::class,
            PlansSeed::class,
            VisasSeed::class,
        ]);
    }
}
