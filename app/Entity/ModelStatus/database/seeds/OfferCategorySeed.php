<?php

use Illuminate\Database\Seeder;

class OfferCategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [[
            'en' => 'Meeting',
            'ru' => 'Вписка'
        ], [
            'en' => 'Journey',
            'ru' => 'Путешествие'
        ]];

        foreach ($categories as $category) {
            \App\Entity\Offers\Category\OfferCategory::create([
                'ru' => ['name' => $category['ru']],
                'en' => ['name' => $category['en']],
            ]);
        }
    }
}
