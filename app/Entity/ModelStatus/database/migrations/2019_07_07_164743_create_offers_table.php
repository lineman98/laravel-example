<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('description');
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('age_from')->nullable();
            $table->unsignedInteger('age_to')->nullable();
            $table->double('weight_from')->nullable();
            $table->double('weight_to')->nullable();
            $table->unsignedInteger('height_from')->nullable();
            $table->unsignedInteger('height_to')->nullable();
            $table->unsignedInteger('boobs_from')->nullable();
            $table->unsignedInteger('boobs_to')->nullable();
            $table->string('boobs_type')->default('all');
            $table->unsignedInteger('cost');
            $table->boolean('possible_increase_cost')->default('0');
            $table->boolean('only_with_reviews')->default('0');
            $table->boolean('only_verified')->default('0');
            $table->string('status')->default('inspection');
            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('set null');

            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('set null');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
