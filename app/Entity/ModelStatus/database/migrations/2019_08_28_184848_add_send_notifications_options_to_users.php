<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSendNotificationsOptionsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('send_news_notifications')->default(true);
            $table->boolean('send_offers_notifications')->default(true);
            $table->boolean('send_messages_notifications')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('send_news_notifications');
            $table->dropColumn('send_offers_notifications');
            $table->dropColumn('send_messages_notifications');
        });
    }
}
