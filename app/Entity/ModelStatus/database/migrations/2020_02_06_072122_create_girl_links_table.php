<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGirlLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('girl_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('girl_id');
            $table->unsignedBigInteger('user_id');
            $table->string('alias')->unique();
            $table->string('status');
            $table->dateTime('expires_on');
            $table->timestamps();
            $table->index(['girl_id', 'user_id', 'status']);
            $table->index(['alias', 'status']);

            $table->foreign('girl_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('girl_links');
    }
}
