<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferEyesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_eyes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('eye_id');
            $table->unsignedBigInteger('offer_id');
            $table->unique(['offer_id', 'eye_id']);
            $table->foreign('offer_id')
                ->references('id')
                ->on('offers')
                ->onDelete('cascade');
            $table->foreign('eye_id')
                ->references('id')
                ->on('eyes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_eyes');
    }
}
