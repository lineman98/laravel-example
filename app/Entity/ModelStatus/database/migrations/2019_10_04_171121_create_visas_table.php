<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visas', function (Blueprint $table) {
            $table->increments('id');
        });
        Schema::create('visa_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('visa_id');
            $table->string('name');
            $table->string('locale')->index('locale_index');
            $table->unique(['visa_id', 'locale']);
            $table->foreign('visa_id')
                ->references('id')
                ->on('visas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_translations');
        Schema::dropIfExists('visas');
    }
}
