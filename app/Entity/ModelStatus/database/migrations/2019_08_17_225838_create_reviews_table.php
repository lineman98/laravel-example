<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('user_from_id');
            $table->unsignedBigInteger('offer_link_id')->nullable();
            $table->text('content');
            $table->boolean('positive')->default(true);
            $table->timestamps();

            $table->unique(['user_from_id', 'offer_link_id']);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('user_from_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('offer_link_id')
                ->references('id')
                ->on('offer_links')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
