<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('status', 16)->default('not_active');
            $table->string('role', 16)->default('user');
            $table->string('verified', 16)->default('not');
            $table->string('type', 16)->default('girl');
            $table->rememberToken();

            $table->unsignedInteger('age')->nullable();
            $table->unsignedInteger('boobs_size')->nullable();
            $table->unsignedInteger('weight')->nullable();
            $table->unsignedInteger('height')->nullable();
            $table->unsignedInteger('bust')->nullable();
            $table->unsignedInteger('waist')->nullable();
            $table->unsignedInteger('hip')->nullable();
            $table->boolean('has_foreign_passport')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
