<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMassMessageUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mass_message_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mass_message_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('message_id')->nullable();
            $table->boolean('sended')->default(false);
            $table->timestamps();

            $table->unique(['mass_message_id', 'user_id']);

            $table->foreign('mass_message_id')
                ->references('id')
                ->on('mass_messages')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('message_id')
                ->references('id')
                ->on('messages')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mass_message_users');
    }
}
