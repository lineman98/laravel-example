CREATE TABLE `wp_pimwick_gift_card_activity` (  `pimwick_gift_card_activity_id` int(11) NOT NULL AUTO_INCREMENT,  `pimwick_gift_card_id` int(11) NOT NULL,  `user_id` bigint(20) unsigned DEFAULT NULL,  `activity_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  `action` varchar(60) NOT NULL,  `amount` decimal(15,6) DEFAULT NULL,  `note` text,  `reference_activity_id` int(11) DEFAULT NULL,  PRIMARY KEY (`pimwick_gift_card_activity_id`),  KEY `wp_ix_pimwick_gift_card_id` (`pimwick_gift_card_id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `wp_pimwick_gift_card_activity` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_pimwick_gift_card_activity` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
