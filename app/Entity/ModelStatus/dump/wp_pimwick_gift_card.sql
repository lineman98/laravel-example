CREATE TABLE `wp_pimwick_gift_card` (  `pimwick_gift_card_id` int(11) NOT NULL AUTO_INCREMENT,  `number` text NOT NULL,  `active` tinyint(1) NOT NULL DEFAULT '1',  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  `expiration_date` date DEFAULT NULL,  PRIMARY KEY (`pimwick_gift_card_id`),  UNIQUE KEY `wp_ix_pimwick_gift_card_number` (`number`(128))) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `wp_pimwick_gift_card` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_pimwick_gift_card` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
