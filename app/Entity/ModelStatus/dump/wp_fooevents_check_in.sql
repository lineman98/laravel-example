CREATE TABLE `wp_fooevents_check_in` (  `id` bigint(20) NOT NULL AUTO_INCREMENT,  `tid` bigint(20) unsigned NOT NULL,  `eid` bigint(20) unsigned NOT NULL,  `day` int(3) DEFAULT NULL,  `checkin` int(10) DEFAULT NULL,  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,  UNIQUE KEY `id` (`id`),  KEY `tid` (`tid`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `wp_fooevents_check_in` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_fooevents_check_in` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
