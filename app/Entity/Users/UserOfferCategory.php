<?php

namespace App\Entity\Users;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserOfferCategory extends Pivot
{

    public $timestamps = false;

}
