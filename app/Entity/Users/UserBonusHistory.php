<?php

namespace App\Entity\Users;

use Illuminate\Database\Eloquent\Model;

class UserBonusHistory extends Model
{

    protected $fillable = [
        'user_id',
        'type',
        'balance_before',
        'balance_after'
    ];

}
