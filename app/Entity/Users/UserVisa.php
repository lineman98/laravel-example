<?php

namespace App\Entity\Users;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserVisa extends Pivot
{

    public $timestamps = false;

}
