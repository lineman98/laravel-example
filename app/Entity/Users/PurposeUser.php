<?php

namespace App\Entity\Users;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PurposeUser extends Pivot
{

    public $timestamps = false;

}
