<?php

namespace App\Entity\Users;

use Illuminate\Database\Eloquent\Model;

class GirlLink extends Model
{

    protected $dates = ['expires_on'];

    public const STATUS_ACTIVE = 'active';
    public const STATUS_CLOSED = 'closed';

    public function getUrl(): string
    {
        return config('links.short_link_host') . '/' . $this->alias;
    }

    public function girl()
    {
        return $this->belongsTo(User::class, 'girl_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
