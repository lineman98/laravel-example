<?php

namespace App\Entity\Users;

use Illuminate\Database\Eloquent\Model;

class UserExpoToken extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
