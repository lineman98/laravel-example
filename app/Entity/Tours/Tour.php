<?php

namespace App\Entity\Tours;

use App\Entity\Geo\Country\Country;
use App\Entity\Users\User;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tour extends Model
{

    use SoftDeletes;

    // Ожидает оплату
    public const STATUS_WAIT_PAY = 'wait_pay';
    // Тур на проверке
    public const STATUS_INSPECTION = 'inspection';
    // Тур заблокирован на модерации
    public const STATUS_BLOCKED = 'blocked';
    // Тур активен
    public const STATUS_ACTIVE = 'active';
    // Тур закрыт
    public const STATUS_CLOSED = 'closed';

    public function isWaitPay(): bool
    {
        return $this->status === self::STATUS_WAIT_PAY;
    }

    public function onInspection(): bool
    {
        return $this->status === self::STATUS_INSPECTION;
    }

    public function isBlocked(): bool
    {
        return $this->status === self::STATUS_BLOCKED;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isClosed(): bool
    {
        return $this->status === self::STATUS_CLOSED;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function setInspection(): bool
    {
        $this->status = self::STATUS_INSPECTION;
        $this->saveOrFail();
        return true;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function activate(): bool
    {
        if(!$this->onInspection())
            return false;

        $this->status = self::STATUS_ACTIVE;
        $this->saveOrFail();

        return true;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function block(): bool
    {
        if($this->isBlocked())
            return false;

        $this->status = self::STATUS_BLOCKED;
        $this->saveOrFail();

        return true;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function close(): bool
    {
        if(!$this->isActive())
            return false;

        $this->status = self::STATUS_CLOSED;
        $this->saveOrFail();

        return true;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * @param Builder $builder
     * @param BaseFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $builder, BaseFilter $filter)
    {
        return $filter->apply($builder);
    }

}
