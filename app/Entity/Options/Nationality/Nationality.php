<?php

namespace App\Entity\Options\Nationality;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Nationality extends Model implements TranslatableContract
{

    use Translatable;

    public $translatedAttributes = ['name_male', 'name_female'];
    public $timestamps = false;

}
