<?php

namespace App\Entity\Options\Eye;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Eye extends Model implements TranslatableContract
{

    use Translatable;

    public $translatedAttributes = ['name'];
    public $timestamps = false;

}
