<?php

namespace App\Entity\Options\Purpose;

use Illuminate\Database\Eloquent\Model;

class PurposeTranslation extends Model
{

    protected $fillable = ['name'];
    public $timestamps = false;

}
