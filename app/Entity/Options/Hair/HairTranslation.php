<?php

namespace App\Entity\Options\Hair;

use Illuminate\Database\Eloquent\Model;

class HairTranslation extends Model
{

    protected $fillable = ['name'];
    public $timestamps = false;

}
