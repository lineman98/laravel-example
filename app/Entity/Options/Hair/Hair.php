<?php

namespace App\Entity\Options\Hair;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Hair extends Model implements TranslatableContract
{

    use Translatable;

    public $translatedAttributes = ['name'];
    public $timestamps = false;

}
