<?php

namespace App\Entity\Bill;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    public const STATUS_FAIL = 'fail';
    public const STATUS_PAID = 'paid';

    public function bill()
    {
        return $this->belongsTo(Bill::class, 'bill_id');
    }

    public function isPaid(): bool
    {
        return $this->status == static::STATUS_PAID;
    }

    public function isFailed(): bool
    {
        return $this->status == static::STATUS_FAIL;
    }

    public function setPaid(): bool
    {
        $this->status = static::STATUS_PAID;
        $this->save();
        return true;
    }

}
