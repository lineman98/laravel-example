<?php

namespace App\Entity\Offers;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OfferPurpose extends Pivot
{
    public $timestamps = false;
}
