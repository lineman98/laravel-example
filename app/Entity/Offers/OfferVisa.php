<?php

namespace App\Entity\Offers;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OfferVisa extends Pivot
{
    public $timestamps = false;
}
