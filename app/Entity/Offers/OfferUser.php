<?php

namespace App\Entity\Offers;

use App\Entity\Users\User;
use App\Entity\Offers\Offer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class OfferUser extends Model
{
    protected $fillable = ['id', 'user_id', 'offer_id', 'message', 'status'];
    protected $dates = ['created_at','updated_at'];
    protected $primaryKey = 'id';

    public const STATUS_SENDED = 'sended';
    public const STATUS_IGNORE = 'ignored';
    public const STATUS_REJECT = 'rejected';
    public const STATUS_ACCEPT = 'accepted';

    public function isIgnored() : bool
    {
        return $this->status === STATUS_IGNORE;
    }

    public function isSended() : bool
    {
        return $this->status === STATUS_SENDED;
    }

    public function isRejected() : bool
    {
        return $this->status === STATUS_REJECT;
    }

    public function isAccepted() : bool
    {
        return $this->status === STATUS_ACCEPT;
    }

    public function ignore(): bool
    {
        $this->status = self::STATUS_IGNORE;
        $this->edited_at = now();
        $this->saveOrFail();

        return true;
    }

    public function send(): bool
    {
        $this->status = self::STATUS_SENDED;
        $this->edited_at = now();
        $this->saveOrFail();

        return true;
    }

    public function reject(): bool
    {
        $this->status = self::STATUS_REJECT;
        $this->edited_at = now();
        $this->saveOrFail();

        return true;
    }

    public function accept(): bool
    {
        $this->status = self::STATUS_ACCEPT;
        $this->edited_at = now();
        $this->saveOrFail();

        return true;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }
}
