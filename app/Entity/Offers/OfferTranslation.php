<?php

namespace App\Entity\Offers;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class OfferTranslation extends Model
{

    use Translatable;

    public $translatedAttributes = ['name'];
    public $timestamps = false;

}
