<?php

namespace App\Entity\Offers\Category;

use Illuminate\Database\Eloquent\Model;

class OfferCategoryTranslation extends Model
{

    protected $fillable = ['name'];
    public $timestamps = false;

}
