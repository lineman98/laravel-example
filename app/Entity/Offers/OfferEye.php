<?php

namespace App\Entity\Offers;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OfferEye extends Pivot
{
    public $timestamps = false;
}
