<?php

namespace App\Entity\Messages;

use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entity\Offers\Offer;

class Dialog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type',
        'name'
    ];

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            DialogUser::class,
            'dialog_id',
            'user_id',
            'id',
            'id'
        );

        //return $this->hasManyThrough(User::class, DialogUsers::class, 'user_id', 'id', 'id');
    }

    public function lastMessage()
    {
        return $this->belongsTo(Message::class, 'last_message_id');
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }
}
