<?php

namespace App\Entity\Messages;

use App\Entity\Users\User;
use Illuminate\Database\Eloquent\Model;

class DialogUser extends Model
{
    protected $fillable = ['dialog_id', 'user_id', 'readed'];

    public function dialog()
    {
        return $this->belongsTo(Dialog::class, 'dialog_id');
    }
}
