import $ from 'jquery';
window.$ = window.jQuery = $;
require('slick-carousel');
require('../../public/js/fancybox.min');

$(document).ready(function () {

    $('[data-fancybox]').fancybox({
        toolbar  : false,
        smallBtn : true,
        iframe : {
            preload : false
        }
    });

    let prev_fancybox_video = null;
    $('[data-fancybox="video-gallery"]').fancybox({
        toolbar  : false,
        smallBtn : true,
        iframe : {
            preload : false
        },
        afterShow: function( instance, current ) {
            let video = current.$content.find('video');
            if(video) {
                video.trigger('play');
            }
            if (prev_fancybox_video) {
                prev_fancybox_video.trigger('pause');
            }
            prev_fancybox_video = video;
        },
        afterClose: function(instance, current) {
            let video = current.$content.find('video');
            if(video) {
                video.trigger('pause');
            }
        }
    });

    //  Profile Slider
    $('.girl-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        infinite: false
    });

    //  Profile Slider min
    $('.girl-slider-min').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        draggable: false,
        asNavFor: '.girl-slider',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        variableWidth: false,
        infinite: true,
        arrows: false
    });

    // скролл чтобы увидеть шапку universal links ios
    window.scrollTo(0, -100);
    setTimeout(() => {
        window.scrollTo(0, -100);
    }, 500);
    setTimeout(() => {
        window.scrollTo(0, -100);
    }, 1000);
});
