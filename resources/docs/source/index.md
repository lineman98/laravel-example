---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://vpiski.ra-ru.ru/docs/collection.json)

<!-- END_INFO -->

#Auth

Class ResetPasswordController
<!-- START_2e1c96dcffcfe7e0eb58d6408f1d619e -->
## Registration

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/auth/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"Alexander","phone":"+793243243243","invite":"GoodEnv","birthday":"20-12-2000","gender":"male"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/auth/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "Alexander",
    "phone": "+793243243243",
    "invite": "GoodEnv",
    "birthday": "20-12-2000",
    "gender": "male"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "Myrtice Feil",
    "type": "girl",
    "role": "user",
    "email": null,
    "phone": "1-317-360-7492 x00851",
    "status": null,
    "created_at": null,
    "country": null,
    "countryName": null,
    "city": null,
    "cityName": null,
    "token": null,
    "photos": [],
    "videos": [],
    "hair": null,
    "hairName": null,
    "eye": null,
    "eyeName": null,
    "nationality": null,
    "nationalityName": null,
    "age": "",
    "hip": "",
    "waist": "",
    "bust": "",
    "boobs_size": "",
    "boobs_type": null,
    "weight": "",
    "height": "",
    "avatar": null,
    "verified": null,
    "phone_verified": null,
    "minimum_cost": null,
    "whatsapp": null,
    "invite": null,
    "status_id": null,
    "model_statuses": [],
    "manager_statuses": [],
    "modelStatusesName": "",
    "managerStatusesName": "",
    "has_foreign_passport": null,
    "visas": [],
    "visasName": "",
    "subscription_payed": null,
    "subscription_ends_at": null,
    "receive_messages_notifications": null,
    "receive_offers_notifications": null,
    "in_blacklist": false,
    "in_users_blacklist": false,
    "referrer": null,
    "invite_at_registration": null,
    "referral_balance": null,
    "subscription_trial": null,
    "unverified_reason": null,
    "verifyPhoto": null,
    "last_seen": null,
    "online": null,
    "can_create_offers": null,
    "can_create_tours": null,
    "gender": null,
    "website": null,
    "aboutme": null
}
```

### HTTP Request
`POST api/auth/register`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | User name.
        `phone` | string |  required  | User phone.
        `invite` | string |  optional  | Invite code.
        `birthday` | string |  required  | Birthday date.
        `gender` | string |  required  | male \ female.
    
<!-- END_2e1c96dcffcfe7e0eb58d6408f1d619e -->

<!-- START_a925a8d22b3615f12fca79456d286859 -->
## Login

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"phone":"+789343242424","password":"Mefe3fHh8efh"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "phone": "+789343242424",
    "password": "Mefe3fHh8efh"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "Dr. Taylor Leannon Sr.",
    "type": "girl",
    "role": "user",
    "email": null,
    "phone": "+12186104480",
    "status": null,
    "created_at": null,
    "country": null,
    "countryName": null,
    "city": null,
    "cityName": null,
    "token": null,
    "photos": [],
    "videos": [],
    "hair": null,
    "hairName": null,
    "eye": null,
    "eyeName": null,
    "nationality": null,
    "nationalityName": null,
    "age": "",
    "hip": "",
    "waist": "",
    "bust": "",
    "boobs_size": "",
    "boobs_type": null,
    "weight": "",
    "height": "",
    "avatar": null,
    "verified": null,
    "phone_verified": null,
    "minimum_cost": null,
    "whatsapp": null,
    "invite": null,
    "status_id": null,
    "model_statuses": [],
    "manager_statuses": [],
    "modelStatusesName": "",
    "managerStatusesName": "",
    "has_foreign_passport": null,
    "visas": [],
    "visasName": "",
    "subscription_payed": null,
    "subscription_ends_at": null,
    "receive_messages_notifications": null,
    "receive_offers_notifications": null,
    "in_blacklist": false,
    "in_users_blacklist": false,
    "referrer": null,
    "invite_at_registration": null,
    "referral_balance": null,
    "subscription_trial": null,
    "unverified_reason": null,
    "verifyPhoto": null,
    "last_seen": null,
    "online": null,
    "can_create_offers": null,
    "can_create_tours": null,
    "gender": null,
    "website": null,
    "aboutme": null
}
```

### HTTP Request
`POST api/auth/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `phone` | string |  required  | User phone.
        `password` | string |  required  | User password.
    
<!-- END_a925a8d22b3615f12fca79456d286859 -->

<!-- START_7c4c8c21aa8bf7ffa0ae617fb274806d -->
## Get the authenticated User.

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/auth/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/auth/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "Mustafa Aufderhar",
    "type": "girl",
    "role": "user",
    "email": null,
    "phone": "(209) 495-4232",
    "status": null,
    "created_at": null,
    "country": null,
    "countryName": null,
    "city": null,
    "cityName": null,
    "token": null,
    "photos": [],
    "videos": [],
    "hair": null,
    "hairName": null,
    "eye": null,
    "eyeName": null,
    "nationality": null,
    "nationalityName": null,
    "age": "",
    "hip": "",
    "waist": "",
    "bust": "",
    "boobs_size": "",
    "boobs_type": null,
    "weight": "",
    "height": "",
    "avatar": null,
    "verified": null,
    "phone_verified": null,
    "minimum_cost": null,
    "whatsapp": null,
    "invite": null,
    "status_id": null,
    "model_statuses": [],
    "manager_statuses": [],
    "modelStatusesName": "",
    "managerStatusesName": "",
    "has_foreign_passport": null,
    "visas": [],
    "visasName": "",
    "subscription_payed": null,
    "subscription_ends_at": null,
    "receive_messages_notifications": null,
    "receive_offers_notifications": null,
    "in_blacklist": false,
    "in_users_blacklist": false,
    "referrer": null,
    "invite_at_registration": null,
    "referral_balance": null,
    "subscription_trial": null,
    "unverified_reason": null,
    "verifyPhoto": null,
    "last_seen": null,
    "online": null,
    "can_create_offers": null,
    "can_create_tours": null,
    "gender": null,
    "website": null,
    "aboutme": null
}
```

### HTTP Request
`GET api/auth/me`


<!-- END_7c4c8c21aa8bf7ffa0ae617fb274806d -->

<!-- START_16928cb8fc6adf2d9bb675d62a2095c5 -->
## Log the user out (Invalidate the token).

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/auth/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/auth/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (204):

```json
{}
```

### HTTP Request
`GET api/auth/logout`


<!-- END_16928cb8fc6adf2d9bb675d62a2095c5 -->

<!-- START_c4cb71484ffcd214f3e00ddde4ec52a8 -->
## Send verify sms code to users phone

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/auth/reset/send" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"phone":"793242434344"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/auth/reset/send"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "phone": "793242434344"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true
}
```

### HTTP Request
`POST api/auth/reset/send`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `phone` | string |  required  | User phone.
    
<!-- END_c4cb71484ffcd214f3e00ddde4ec52a8 -->

<!-- START_8cb6ef3a0251d9c81673e2bc8d78fba0 -->
## Check verify code

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/auth/reset/verify" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"phone":"794324324343","code":"fjwo","new_password":"efyEehfh3f"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/auth/reset/verify"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "phone": "794324324343",
    "code": "fjwo",
    "new_password": "efyEehfh3f"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true
}
```
> Example response (200):

```json
{
    "success": "false",
    "error": "Error message"
}
```

### HTTP Request
`POST api/auth/reset/verify`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `phone` | string |  required  | User phone.
        `code` | string |  required  | Verification code (4 symbols).
        `new_password` | string |  required  | New user password.
    
<!-- END_8cb6ef3a0251d9c81673e2bc8d78fba0 -->

#Geo

Class CountriesController
<!-- START_316a4c3b4f6a4c4ff34e5893943cdebd -->
## Get countries

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/countries" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/countries"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": "Mali"
    },
    {
        "id": null,
        "name": "Namibia"
    }
]
```

### HTTP Request
`GET api/countries`


<!-- END_316a4c3b4f6a4c4ff34e5893943cdebd -->

<!-- START_254349f0265fada41f09fed8771a5553 -->
## Add country

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/countries" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"slug":"Moscow"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/countries"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "slug": "Moscow"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "Gambia"
}
```

### HTTP Request
`POST api/countries`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `slug` | string |  required  | Slug.
    
<!-- END_254349f0265fada41f09fed8771a5553 -->

<!-- START_cc02d0cab34b935aaa1fd88f9e96d4da -->
## Show country

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/countries/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/countries/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "United States Virgin Islands"
}
```

### HTTP Request
`GET api/countries/{country}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | Id of country

<!-- END_cc02d0cab34b935aaa1fd88f9e96d4da -->

<!-- START_1253a528930346f4304083aa578fdf25 -->
## Update country

> Example request:

```bash
curl -X PUT \
    "http://vpiski.ra-ru.ru/api/countries/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/countries/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "true"
}
```

### HTTP Request
`PUT api/countries/{country}`

`PATCH api/countries/{country}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | Id of country

<!-- END_1253a528930346f4304083aa578fdf25 -->

<!-- START_1fd3a6d4b6e5e66aac5898415efb7566 -->
## Remove country (dont works)

> Example request:

```bash
curl -X DELETE \
    "http://vpiski.ra-ru.ru/api/countries/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/countries/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/countries/{country}`


<!-- END_1fd3a6d4b6e5e66aac5898415efb7566 -->

<!-- START_53fbc8b45f386f7d9164e1efb9477cf7 -->
## Get most popular cities

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/cities/top?count=15" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/cities/top"
);

let params = {
    "count": "15",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": "Port Marianaborough",
        "image_path": null
    },
    {
        "id": null,
        "name": "Narcisohaven",
        "image_path": null
    }
]
```

### HTTP Request
`GET api/cities/top`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `count` |  optional  | int Count of cities (max 20)

<!-- END_53fbc8b45f386f7d9164e1efb9477cf7 -->

<!-- START_56d7be9447e2ce39ac69b09141bf5902 -->
## Get cities

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/cities?country_id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/cities"
);

let params = {
    "country_id": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "Kailynstad",
    "image_path": null
}
```

### HTTP Request
`GET api/cities`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `country_id` |  optional  | int Id of country.

<!-- END_56d7be9447e2ce39ac69b09141bf5902 -->

<!-- START_4d36a2828ff43205fcdf97b0cf6fdcfe -->
## Add city

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/cities" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/cities"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "South Tyshawn",
    "image_path": null
}
```

### HTTP Request
`POST api/cities`


<!-- END_4d36a2828ff43205fcdf97b0cf6fdcfe -->

<!-- START_0651fff87b81a4d1e8d166065f7676f0 -->
## Show city

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/cities/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/cities/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "name": "Port Carlosburgh",
    "image_path": null
}
```

### HTTP Request
`GET api/cities/{city}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | Id of city.

<!-- END_0651fff87b81a4d1e8d166065f7676f0 -->

<!-- START_876156d0dfb9d96d9a806f37cfa97680 -->
## Update city

> Example request:

```bash
curl -X PUT \
    "http://vpiski.ra-ru.ru/api/cities/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/cities/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "true"
}
```

### HTTP Request
`PUT api/cities/{city}`

`PATCH api/cities/{city}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | Id of city.

<!-- END_876156d0dfb9d96d9a806f37cfa97680 -->

<!-- START_ca7712807e8d39d79e5efc44555cb8b5 -->
## Remove city (dont works)

> Example request:

```bash
curl -X DELETE \
    "http://vpiski.ra-ru.ru/api/cities/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/cities/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/cities/{city}`


<!-- END_ca7712807e8d39d79e5efc44555cb8b5 -->

#Help


<!-- START_fc69925eeaf83e770a9474a7b1eabfc8 -->
## Get Faq Questions &amp; Answers

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/faq" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/faq"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "answer": null,
        "question": null
    },
    {
        "answer": null,
        "question": null
    }
]
```

### HTTP Request
`GET api/faq`


<!-- END_fc69925eeaf83e770a9474a7b1eabfc8 -->

#Messages

Class MessagesController
<!-- START_c61e9c2b3fdeea56ee207c8db3d88546 -->
## Get user dialogs

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/messages" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/messages"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "lastMessage": null,
        "last_message_time": null,
        "users": [],
        "name": null,
        "type": null,
        "offer_id": {},
        "readed": ""
    },
    {
        "id": null,
        "lastMessage": null,
        "last_message_time": null,
        "users": [],
        "name": null,
        "type": null,
        "offer_id": {},
        "readed": ""
    }
]
```

### HTTP Request
`GET api/messages`


<!-- END_c61e9c2b3fdeea56ee207c8db3d88546 -->

<!-- START_35df1f44031ea96b6e03eca6e38ceda7 -->
## Send message to User

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/messages" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"user_to":1,"content":"Hello world"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/messages"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "user_to": 1,
    "content": "Hello world"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "content": null,
    "user_from_id": null,
    "user_to_id": null,
    "read": null,
    "created_at": null,
    "dialog_id": null
}
```

### HTTP Request
`POST api/messages`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `user_to` | integer |  optional  | reqired Id of recipient.
        `content` | string |  optional  | reqired Text of message.
    
<!-- END_35df1f44031ea96b6e03eca6e38ceda7 -->

<!-- START_a20ae0f1b7d36be978ebe9b1083fd79a -->
## Send message to Dialog

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/messages/dialog/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"dialog_id":1,"content":"Hello world"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/messages/dialog/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "dialog_id": 1,
    "content": "Hello world"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": null,
    "content": null,
    "user_from_id": null,
    "user_to_id": null,
    "read": null,
    "created_at": null,
    "dialog_id": null
}
```

### HTTP Request
`POST api/messages/dialog/{dialog_id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `dialog_id` | integer |  required  | Id of dialog.
        `content` | string |  required  | Text of message.
    
<!-- END_a20ae0f1b7d36be978ebe9b1083fd79a -->

<!-- START_bcef7e564bb1d747ad74f0a10e135c20 -->
## Show dialog messages

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/messages/listDialog?dialog_id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/messages/listDialog"
);

let params = {
    "dialog_id": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "content": null,
        "user_from_id": null,
        "user_to_id": null,
        "read": null,
        "created_at": null,
        "dialog_id": null
    },
    {
        "id": null,
        "content": null,
        "user_from_id": null,
        "user_to_id": null,
        "read": null,
        "created_at": null,
        "dialog_id": null
    }
]
```

### HTTP Request
`GET api/messages/listDialog`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `dialog_id` |  optional  | int required Id of dialog:

<!-- END_bcef7e564bb1d747ad74f0a10e135c20 -->

<!-- START_c512ae8ebb6ef88158cb41e5e0c20c60 -->
## Get unread dialogs

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/messages/unreadCount" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/messages/unreadCount"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "unread_count": 10
}
```

### HTTP Request
`GET api/messages/unreadCount`


<!-- END_c512ae8ebb6ef88158cb41e5e0c20c60 -->

<!-- START_9208f8b7d811fd9192bf3aa204a6e4b4 -->
## Mark message as read

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/messages/markAsRead" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"id":18}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/messages/markAsRead"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "id": 18
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "true"
}
```

### HTTP Request
`POST api/messages/markAsRead`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | Id of the message
    
<!-- END_9208f8b7d811fd9192bf3aa204a6e4b4 -->

<!-- START_d8abf20c4be37e9e5ac526cf4b3b361d -->
## Mark dialog as read

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/messages/markDialogAsRead" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"id":1}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/messages/markDialogAsRead"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "true"
}
```

### HTTP Request
`POST api/messages/markDialogAsRead`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | Dialog id.
    
<!-- END_d8abf20c4be37e9e5ac526cf4b3b361d -->

#Offers

Class OffersController
<!-- START_6667c808211f89d88298e991a9b82f91 -->
## Activate offer

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/activate" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/activate"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{id}/activate`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_6667c808211f89d88298e991a9b82f91 -->

<!-- START_80b8b3a0f656cc311cdad18bfa782aac -->
## Block offer

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/block" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/block"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{id}/block`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_80b8b3a0f656cc311cdad18bfa782aac -->

<!-- START_3df6fa8581da7284eb82d91693e597b9 -->
## Accept offer application

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/users/20/agree" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/users/20/agree"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{offer}/users/{userId}/agree`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer` |  optional  | int Id of offer
    `userId` |  optional  | int Id applicated user

<!-- END_3df6fa8581da7284eb82d91693e597b9 -->

<!-- START_ffe823f9470adecc342f82f7866e5601 -->
## Refuse offer application

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/users/20/refuse" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/users/20/refuse"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{offer}/users/{userId}/refuse`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer` |  optional  | int Id of offer
    `userId` |  optional  | int Id applicated user

<!-- END_ffe823f9470adecc342f82f7866e5601 -->

<!-- START_315ddade644f5c6600cf0531bbcdd3f3 -->
## All offers (for admin)

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/all?count=30" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/all"
);

let params = {
    "count": "30",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/offers/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `count` |  optional  | int Count of offers, Default 20.

<!-- END_315ddade644f5c6600cf0531bbcdd3f3 -->

<!-- START_e5707a12bcfe562452b253f81e3a46dd -->
## Get own offers

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/my" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/my"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/offers/my`


<!-- END_e5707a12bcfe562452b253f81e3a46dd -->

<!-- START_4c2d304880298a3b0e5e4c67bf291b55 -->
## Show offer

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/1/forModel" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/forModel"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/offers/{id}/forModel`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_4c2d304880298a3b0e5e4c67bf291b55 -->

<!-- START_99f9fda88c6cde0d1e521a25845ef16e -->
## End offer

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/setEnded" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/setEnded"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{id}/setEnded`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_99f9fda88c6cde0d1e521a25845ef16e -->

<!-- START_e8f12da01a99fd22786e7b49ac7d17ef -->
## Set viewed offer

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/setViewed" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/setViewed"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{offer}/setViewed`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer` |  optional  | int Id of offer

<!-- END_e8f12da01a99fd22786e7b49ac7d17ef -->

<!-- START_7d29e69357c0020eed85486bd7b7148f -->
## Close offer

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/close" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/close"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{id}/close`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_7d29e69357c0020eed85486bd7b7148f -->

<!-- START_9089d05c56927bf85e9ff67799057377 -->
## End offer (offer happend)

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/end" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/end"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`POST api/offers/{id}/end`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_9089d05c56927bf85e9ff67799057377 -->

<!-- START_515c52be06bbcaa6352893dae7989d94 -->
## Получить счет на оплату публикации

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/1/getBill" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/getBill"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/offers/{id}/getBill`


<!-- END_515c52be06bbcaa6352893dae7989d94 -->

<!-- START_2741c31dd89e5d192b39977e6ec69bb2 -->
## Get offers

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers?city_id=1&category=1&userStatus=not_viewed" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers"
);

let params = {
    "city_id": "1",
    "category": "1",
    "userStatus": "not_viewed",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/offers`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `city_id` |  optional  | int Id of city.
    `category` |  optional  | int Id of category.
    `userStatus` |  optional  | string User status for offer {viewed, not_viewed, applicated}.

<!-- END_2741c31dd89e5d192b39977e6ec69bb2 -->

<!-- START_a45eaa0bc07a2833fc15fdfb8cd32142 -->
## Create offer

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"Best party","description":"Lorem ipsum dolor sit amen...","country":1,"city":1,"category":1,"age_from":23,"age_to":60,"meeting_date":"30.12.2000","only_verified":true,"nationalities":[1],"eyes":[1],"hairs":[1],"visas":[1],"country_from":12,"city_from":24,"search_from_same_city":true,"nearest_city_range":100}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "Best party",
    "description": "Lorem ipsum dolor sit amen...",
    "country": 1,
    "city": 1,
    "category": 1,
    "age_from": 23,
    "age_to": 60,
    "meeting_date": "30.12.2000",
    "only_verified": true,
    "nationalities": [
        1
    ],
    "eyes": [
        1
    ],
    "hairs": [
        1
    ],
    "visas": [
        1
    ],
    "country_from": 12,
    "city_from": 24,
    "search_from_same_city": true,
    "nearest_city_range": 100
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/offers`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Name.
        `description` | string |  required  | Description(10-1000)
        `country` | integer |  required  | Id of country
        `city` | integer |  required  | Id of city
        `category` | integer |  required  | Id of category
        `age_from` | integer |  optional  | Minimal age
        `age_to` | integer |  optional  | Maximal age
        `meeting_date` | date |  required  | Date (d.m.Y)
        `only_verified` | boolean |  required  | Search only verivied (only verified can enable)
        `nationalities.*` | integer |  optional  | Ids of nationalities
        `eyes.*` | integer |  optional  | Ids of eye types
        `hairs.*` | integer |  optional  | Ids of hairs types
        `visas.*` | integer |  optional  | Ids of visas
        `country_from` | integer |  optional  | Id of searching users country
        `city_from` | integer |  optional  | Id of searching users city
        `search_from_same_city` | boolean |  optional  | Search only from same city
        `nearest_city_range` | integer |  optional  | {50,100,250,500}
    
<!-- END_a45eaa0bc07a2833fc15fdfb8cd32142 -->

<!-- START_512ca1cc43ac8514662aba3aba6157db -->
## Show offer (for admins)

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/offers/{offer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_512ca1cc43ac8514662aba3aba6157db -->

<!-- START_ed57e3631710f509cae3099325cb183d -->
## Update offer

> Example request:

```bash
curl -X PUT \
    "http://vpiski.ra-ru.ru/api/offers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"description":"Lorem ipsum dolor sit amen"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "description": "Lorem ipsum dolor sit amen"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`PUT api/offers/{offer}`

`PATCH api/offers/{offer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer` |  optional  | int Id of offer
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `description` | string |  optional  | Description (10-1000)
    
<!-- END_ed57e3631710f509cae3099325cb183d -->

<!-- START_f7b14e58800200c9dd82259343ecea98 -->
## Delete offer

> Example request:

```bash
curl -X DELETE \
    "http://vpiski.ra-ru.ru/api/offers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`DELETE api/offers/{offer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of offer

<!-- END_f7b14e58800200c9dd82259343ecea98 -->

<!-- START_fafb820386e4103435d30d05c44a24dc -->
## Get offer applications

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/1/applications" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/applications"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "user": null,
        "offer_id": null,
        "status": null,
        "message": null,
        "created_at": null,
        "updated_at": null
    },
    {
        "id": null,
        "user": null,
        "offer_id": null,
        "status": null,
        "message": null,
        "created_at": null,
        "updated_at": null
    }
]
```

### HTTP Request
`GET api/offers/{offer}/applications`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer_id` |  optional  | int Id of offer

<!-- END_fafb820386e4103435d30d05c44a24dc -->

<!-- START_4caff6921cd88db537cea7374aa5b717 -->
## Send application or ignore offer

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/offers/1/applications" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"message":"lorem ipsum..","status":"ignored"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/applications"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "message": "lorem ipsum..",
    "status": "ignored"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "user": null,
        "offer_id": null,
        "status": null,
        "message": null,
        "created_at": null,
        "updated_at": null
    },
    {
        "id": null,
        "user": null,
        "offer_id": null,
        "status": null,
        "message": null,
        "created_at": null,
        "updated_at": null
    }
]
```

### HTTP Request
`POST api/offers/{offer}/applications`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer_id` |  optional  | int Id of offer
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `message` | string |  optional  | Message(128)
        `status` | string |  optional  | Status (sended, ignored)
    
<!-- END_4caff6921cd88db537cea7374aa5b717 -->

<!-- START_e3ccd64698f6e4cb40b6eda056d3cc23 -->
## Show application

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/1/applications/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/applications/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "user": null,
        "offer_id": null,
        "status": null,
        "message": null,
        "created_at": null,
        "updated_at": null
    },
    {
        "id": null,
        "user": null,
        "offer_id": null,
        "status": null,
        "message": null,
        "created_at": null,
        "updated_at": null
    }
]
```

### HTTP Request
`GET api/offers/{offer}/applications/{application}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer_id` |  optional  | int Id of offer
    `id` |  optional  | int Id of application

<!-- END_e3ccd64698f6e4cb40b6eda056d3cc23 -->

<!-- START_cd2e3280dcecff196d655a96ba502be8 -->
## Update application

> Example request:

```bash
curl -X PUT \
    "http://vpiski.ra-ru.ru/api/offers/1/applications/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"message":"lorem ipsum..","status":"sended"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/applications/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "message": "lorem ipsum..",
    "status": "sended"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "result": "true"
}
```

### HTTP Request
`PUT api/offers/{offer}/applications/{application}`

`PATCH api/offers/{offer}/applications/{application}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `offer_id` |  optional  | int Id of offer
    `id` |  optional  | int Id of application
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `message` | string |  optional  | Message(128)
        `status` | string |  optional  | Status (sended, rejected, accepted)
    
<!-- END_cd2e3280dcecff196d655a96ba502be8 -->

<!-- START_33568546b01284743d864cdbae656013 -->
## Remove application (dont works)

> Example request:

```bash
curl -X DELETE \
    "http://vpiski.ra-ru.ru/api/offers/1/applications/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/1/applications/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/offers/{offer}/applications/{application}`


<!-- END_33568546b01284743d864cdbae656013 -->

#Options

Class OptionsController
<!-- START_6123c5d8c5d6161e58948ec29f97cd24 -->
## Get main site options (eyes, hairs, nationalities, etc.)

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/options" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/options"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/options`


<!-- END_6123c5d8c5d6161e58948ec29f97cd24 -->

<!-- START_a2f44f7cc86c6d05eed92eb1d4f9320b -->
## Get eyes

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/eyes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/eyes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": null
    },
    {
        "id": null,
        "name": null
    }
]
```

### HTTP Request
`GET api/eyes`


<!-- END_a2f44f7cc86c6d05eed92eb1d4f9320b -->

<!-- START_0b668f9af6ff736363016bc13c1728b8 -->
## Get nationalities

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/nationalities" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/nationalities"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name_male": null,
        "name_female": null
    },
    {
        "id": null,
        "name_male": null,
        "name_female": null
    }
]
```

### HTTP Request
`GET api/nationalities`


<!-- END_0b668f9af6ff736363016bc13c1728b8 -->

<!-- START_0055544d0f8c47dab2f490692bf90727 -->
## Get hairs

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/hairs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/hairs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": null
    },
    {
        "id": null,
        "name": null
    }
]
```

### HTTP Request
`GET api/hairs`


<!-- END_0055544d0f8c47dab2f490692bf90727 -->

<!-- START_d8db86c5834131cf21834211de0dc70a -->
## Get purposes

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/purposes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/purposes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": null
    },
    {
        "id": null,
        "name": null
    }
]
```

### HTTP Request
`GET api/purposes`


<!-- END_d8db86c5834131cf21834211de0dc70a -->

<!-- START_5cebe4a4af8f771e00d65dee3d4e664b -->
## Get offers categories

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/offers/categories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/offers/categories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": null
    },
    {
        "id": null,
        "name": null
    }
]
```

### HTTP Request
`GET api/offers/categories`


<!-- END_5cebe4a4af8f771e00d65dee3d4e664b -->

#Referrals

Class ReferralsController
<!-- START_17b629d4e8eaf50ce1403effd4a49c01 -->
## Get referrals

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/referrals?id=est" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/referrals"
);

let params = {
    "id": "est",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": "Prof. Vella Kling",
        "phone": "795*****91",
        "created_at": null,
        "avatar": null
    },
    {
        "id": null,
        "name": "Carole Volkman",
        "phone": "+1 *****13",
        "created_at": null,
        "avatar": null
    }
]
```

### HTTP Request
`GET api/referrals`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `id` |  optional  | int Id of requested user (For admins)

<!-- END_17b629d4e8eaf50ce1403effd4a49c01 -->

<!-- START_5c022885b5920e5cd5da32915f050ffa -->
## Get referrals stats

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/referrals/stats?id=sequi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/referrals/stats"
);

let params = {
    "id": "sequi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "day_referrals_count": 10,
    "all_referrals_count": 11,
    "bonus_months_count": 2
}
```

### HTTP Request
`GET api/referrals/stats`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `id` |  optional  | int Id of requested user (For admins)

<!-- END_5c022885b5920e5cd5da32915f050ffa -->

#Users

Class ProfileController
<!-- START_7e1905a985d1103fff431233baaeaf43 -->
## Change password

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/profile/changePassword" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"password":"mypassword123"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/profile/changePassword"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "password": "mypassword123"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/profile/changePassword`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `password` | string |  required  | Password (6-16)
    
<!-- END_7e1905a985d1103fff431233baaeaf43 -->

<!-- START_f5f57c8655db4d68ad5d9627cb0b56e4 -->
## Change notifications

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/profile/notificationsChange" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"receive_offers_notifications":true,"receive_messages_notifications":true}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/profile/notificationsChange"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "receive_offers_notifications": true,
    "receive_messages_notifications": true
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/profile/notificationsChange`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `receive_offers_notifications` | boolean |  required  | Recieve offers notifications
        `receive_messages_notifications` | boolean |  required  | Recieve messages notifications
    
<!-- END_f5f57c8655db4d68ad5d9627cb0b56e4 -->

<!-- START_e490d771810084790d972d2795d5b944 -->
## Add to black list

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/profile/addToBlackList" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"id":1}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/profile/addToBlackList"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/profile/addToBlackList`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | Id of blocking user.
    
<!-- END_e490d771810084790d972d2795d5b944 -->

<!-- START_47457cc060907288c2e3b4af24c7b5df -->
## Remove from black list

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/profile/removeFromBlackList" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"id":1}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/profile/removeFromBlackList"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/profile/removeFromBlackList`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | Id of blocked user.
    
<!-- END_47457cc060907288c2e3b4af24c7b5df -->

<!-- START_fc1e4f6a697e3c48257de845299b71d5 -->
## Get all users (For admins)

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/users?count=enim" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users"
);

let params = {
    "count": "enim",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": null,
        "name": "Keira Feil",
        "type": "girl",
        "role": "user",
        "email": null,
        "phone": "372.305.2934",
        "status": null,
        "created_at": null,
        "country": null,
        "countryName": null,
        "city": null,
        "cityName": null,
        "token": "0",
        "photos": [],
        "videos": [],
        "hair": null,
        "hairName": null,
        "eye": null,
        "eyeName": null,
        "nationality": null,
        "nationalityName": null,
        "age": "",
        "hip": "",
        "waist": "",
        "bust": "",
        "boobs_size": "",
        "boobs_type": null,
        "weight": "",
        "height": "",
        "avatar": null,
        "verified": null,
        "phone_verified": null,
        "minimum_cost": null,
        "whatsapp": null,
        "invite": null,
        "status_id": null,
        "model_statuses": [],
        "manager_statuses": [],
        "modelStatusesName": "",
        "managerStatusesName": "",
        "has_foreign_passport": null,
        "visas": [],
        "visasName": "",
        "subscription_payed": null,
        "subscription_ends_at": null,
        "receive_messages_notifications": null,
        "receive_offers_notifications": null,
        "in_blacklist": false,
        "in_users_blacklist": false,
        "referrer": null,
        "invite_at_registration": null,
        "referral_balance": null,
        "subscription_trial": null,
        "unverified_reason": null,
        "verifyPhoto": null,
        "last_seen": null,
        "online": null,
        "can_create_offers": null,
        "can_create_tours": null,
        "gender": null,
        "website": null,
        "aboutme": null
    },
    {
        "id": null,
        "name": "Avis Donnelly",
        "type": "girl",
        "role": "user",
        "email": null,
        "phone": "453-696-9711 x85220",
        "status": null,
        "created_at": null,
        "country": null,
        "countryName": null,
        "city": null,
        "cityName": null,
        "token": "1",
        "photos": [],
        "videos": [],
        "hair": null,
        "hairName": null,
        "eye": null,
        "eyeName": null,
        "nationality": null,
        "nationalityName": null,
        "age": "",
        "hip": "",
        "waist": "",
        "bust": "",
        "boobs_size": "",
        "boobs_type": null,
        "weight": "",
        "height": "",
        "avatar": null,
        "verified": null,
        "phone_verified": null,
        "minimum_cost": null,
        "whatsapp": null,
        "invite": null,
        "status_id": null,
        "model_statuses": [],
        "manager_statuses": [],
        "modelStatusesName": "",
        "managerStatusesName": "",
        "has_foreign_passport": null,
        "visas": [],
        "visasName": "",
        "subscription_payed": null,
        "subscription_ends_at": null,
        "receive_messages_notifications": null,
        "receive_offers_notifications": null,
        "in_blacklist": false,
        "in_users_blacklist": false,
        "referrer": null,
        "invite_at_registration": null,
        "referral_balance": null,
        "subscription_trial": null,
        "unverified_reason": null,
        "verifyPhoto": null,
        "last_seen": null,
        "online": null,
        "can_create_offers": null,
        "can_create_tours": null,
        "gender": null,
        "website": null,
        "aboutme": null
    }
]
```

### HTTP Request
`GET api/users`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `count` |  optional  | int Count of users

<!-- END_fc1e4f6a697e3c48257de845299b71d5 -->

<!-- START_12e37982cc5398c7100e59625ebb5514 -->
## Create user (dont works)

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/users`


<!-- END_12e37982cc5398c7100e59625ebb5514 -->

<!-- START_8653614346cb0e3d444d164579a0a0a2 -->
## Show user

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/users/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user.

<!-- END_8653614346cb0e3d444d164579a0a0a2 -->

<!-- START_48a3115be98493a3c866eb0e23347262 -->
## Update user

> Example request:

```bash
curl -X PUT \
    "http://vpiski.ra-ru.ru/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"Alexander","country":1,"city":1,"age":39,"weight":88,"height":180,"eye":1,"hair":1,"nationality":1,"minimum_cost":20,"whatsapp":"+79343424324","visas":[1],"gender":"female","website":"http:\/\/myblog.info","offer_categories":[1],"aboutme":"Hello! I'm very funny and pretty man"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "Alexander",
    "country": 1,
    "city": 1,
    "age": 39,
    "weight": 88,
    "height": 180,
    "eye": 1,
    "hair": 1,
    "nationality": 1,
    "minimum_cost": 20,
    "whatsapp": "+79343424324",
    "visas": [
        1
    ],
    "gender": "female",
    "website": "http:\/\/myblog.info",
    "offer_categories": [
        1
    ],
    "aboutme": "Hello! I'm very funny and pretty man"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/users/{user}`

`PATCH api/users/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  optional  | int User for update.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Name.
        `country` | integer |  required  | Id of country:
        `city` | integer |  required  | Id of city.
        `age` | integer |  optional  | User age.
        `weight` | integer |  required  | User weight.
        `height` | integer |  required  | User height.
        `eye` | integer |  optional  | Id of eye type.
        `hair` | integer |  optional  | Id of hair type.
        `nationality` | integer |  optional  | Id of nationality.
        `minimum_cost` | integer |  optional  | Minimal cost.
        `whatsapp` | string |  optional  | Whatsapp number.
        `visas.*` | integer |  optional  | Ids of visas types.
        `gender` | string |  optional  | male \ female.
        `website` | string |  optional  | Url of website.
        `offer_categories.*` | integer |  optional  | Ids of offer categories.
        `aboutme` | string |  optional  | My Description (10-1000).
    
<!-- END_48a3115be98493a3c866eb0e23347262 -->

<!-- START_d2db7a9fe3abd141d5adbc367a88e969 -->
## Delete user

> Example request:

```bash
curl -X DELETE \
    "http://vpiski.ra-ru.ru/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"reason":"SPAM!!!!!!!!!!!"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "reason": "SPAM!!!!!!!!!!!"
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`DELETE api/users/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  optional  | int Id of user.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `reason` | string |  optional  | Reason of deleting.
    
<!-- END_d2db7a9fe3abd141d5adbc367a88e969 -->

<!-- START_e18cf66e80aba2d509a3d981530c0aee -->
## Add push token

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/sint/addPushToken" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"token":"EewbfbYGfyef7ew78fewfewYEfge","device":"phone","os":"ios"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/sint/addPushToken"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "token": "EewbfbYGfyef7ew78fewfewYEfge",
    "device": "phone",
    "os": "ios"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/addPushToken`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `token` | string |  required  | 
        `device` | string |  optional  | 
        `os` | string |  optional  | {ios, androind, web}
    
<!-- END_e18cf66e80aba2d509a3d981530c0aee -->

<!-- START_81efc489fcd4db8c6d3eab59784f2c96 -->
## Remove push token

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/eos/removePushToken" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"token":"EewbfbYGfyef7ew78fewfewYEfge","device":"phone","os":"ios"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/eos/removePushToken"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "token": "EewbfbYGfyef7ew78fewfewYEfge",
    "device": "phone",
    "os": "ios"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/removePushToken`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `token` | string |  required  | 
        `device` | string |  optional  | 
        `os` | string |  optional  | {ios, androind, web}
    
<!-- END_81efc489fcd4db8c6d3eab59784f2c96 -->

<!-- START_2febbdef1a7c0d655a27a6cff5821116 -->
## Upload Photo

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/mollitia/photos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"file":"aut"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/mollitia/photos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "file": "aut"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/users/{id}/photos`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `file` | image |  required  | Image for uploading
    
<!-- END_2febbdef1a7c0d655a27a6cff5821116 -->

<!-- START_5be3759747813bc4d254becb188589fd -->
## Upload video

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/1/videos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"file":"dolorum"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1/videos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "file": "dolorum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/users/{id}/videos`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `file` | video |  required  | Image for uploading
    
<!-- END_5be3759747813bc4d254becb188589fd -->

<!-- START_b4ce6ad65a3b27b4a4e502c7d978815a -->
## Upload verify photo

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/1/uploadVerifyPhoto" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"file":"natus"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1/uploadVerifyPhoto"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "file": "natus"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/users/{id}/uploadVerifyPhoto`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `file` | image |  required  | Image for uploading
    
<!-- END_b4ce6ad65a3b27b4a4e502c7d978815a -->

<!-- START_346394ee3dcd85c85112c4865b7c0dd7 -->
## Remove photo

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/1/removePhoto" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"id":1}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1/removePhoto"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/removePhoto`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | Id of photo.
    
<!-- END_346394ee3dcd85c85112c4865b7c0dd7 -->

<!-- START_491c0755d6871f574b535e590da34942 -->
## Check media (for Admins)

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/1/checkMedia" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"id":1}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1/checkMedia"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/checkMedia`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | Id of media.
    
<!-- END_491c0755d6871f574b535e590da34942 -->

<!-- START_9f160fa5e86a28491ee65efed3b59301 -->
## Send verify request

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/sunt/sendVerifyRequest" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/sunt/sendVerifyRequest"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/sendVerifyRequest`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user

<!-- END_9f160fa5e86a28491ee65efed3b59301 -->

<!-- START_14e1ef77139fa597b541870eebb9bdf9 -->
## Set avatar

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/iste/setAvatar" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"id":1}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/iste/setAvatar"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/setAvatar`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | Id of photo.
    
<!-- END_14e1ef77139fa597b541870eebb9bdf9 -->

<!-- START_cab16f99d2fee302e9213e16d5132742 -->
## Set user virified (for admins)

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/minus/setVerified" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/minus/setVerified"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/setVerified`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user

<!-- END_cab16f99d2fee302e9213e16d5132742 -->

<!-- START_95c79dd09e8c14e5d1bbf687e11cc492 -->
## Set user unvirified (for admins)

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/alias/setUnverified" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"reason":"Dont shown face"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/alias/setUnverified"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "reason": "Dont shown face"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/setUnverified`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `reason` | string |  optional  | 
    
<!-- END_95c79dd09e8c14e5d1bbf687e11cc492 -->

<!-- START_7e05f7aa0989b32789e6553e8f5cceb7 -->
## Set virified phone (for admins)

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/1/setVerifiedPhone" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1/setVerifiedPhone"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/setVerifiedPhone`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user.

<!-- END_7e05f7aa0989b32789e6553e8f5cceb7 -->

<!-- START_f942ddb4eafab2179c4eb3adf295226a -->
## Выдача доступа на месяц

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/1/extend" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/1/extend"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/extend`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user.

<!-- END_f942ddb4eafab2179c4eb3adf295226a -->

<!-- START_609f3cddb4337a6e7045802ada0c9164 -->
## Block user (for admins)

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/users/vel/block" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/vel/block"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```

### HTTP Request
`POST api/users/{id}/block`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user for blocking

<!-- END_609f3cddb4337a6e7045802ada0c9164 -->

<!-- START_1c75de5d58b1c786486aa9d0d83b743a -->
## Get user reviews

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/users/corrupti/reviews" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/corrupti/reviews"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "user": null,
        "positive": null,
        "content": null,
        "created_at": null
    },
    {
        "user": null,
        "positive": null,
        "content": null,
        "created_at": null
    }
]
```

### HTTP Request
`GET api/users/{id}/reviews`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user

<!-- END_1c75de5d58b1c786486aa9d0d83b743a -->

<!-- START_1c36831fc3117f6c8bc0df0f10d40877 -->
## Get user short link

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/users/aut/link" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/users/aut/link"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "alias": null,
    "expires_on": null,
    "link": "http:\/\/links.top-models.vip\/"
}
```

### HTTP Request
`GET api/users/{id}/link`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int Id of user

<!-- END_1c36831fc3117f6c8bc0df0f10d40877 -->

#Verification

Class PhoneVerificationsController
<!-- START_9d22e92cbad8544435f3d6cfb0506d5a -->
## Send verify sms code to users phone

response {
  "success": "true"
}
response {
  "success": "true",
  "already_verified": "true"
}

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/verify/send" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/verify/send"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/verify/send`


<!-- END_9d22e92cbad8544435f3d6cfb0506d5a -->

<!-- START_1dd7c1dd3041aa7d8b5c75fee578ff5a -->
## Check verification code

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/verify/verify" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/verify/verify"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```
> Example response (200):

```json
{
    "success": "false",
    "error": "Error message"
}
```

### HTTP Request
`POST api/verify/verify`


<!-- END_1dd7c1dd3041aa7d8b5c75fee578ff5a -->

<!-- START_950cb56fddde9740f036aef474f3a4f9 -->
## Verify by Firebase

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/verify/verifyByFirebase" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"token":"excepturi"}'

```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/verify/verifyByFirebase"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "token": "excepturi"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```
> Example response (200):

```json
{
    "success": "false",
    "error": "Error message"
}
```

### HTTP Request
`POST api/verify/verifyByFirebase`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `token` | string |  required  | Token
    
<!-- END_950cb56fddde9740f036aef474f3a4f9 -->

<!-- START_b27529155f58eabfe4c844b0e441befe -->
## Send call code to users phone

response {
  "success": "true"
}
response {
  "success": "true",
  "already_verified": "true"
}

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/verify/sendCall" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/verify/sendCall"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/verify/sendCall`


<!-- END_b27529155f58eabfe4c844b0e441befe -->

<!-- START_294da5c22e4eac712d7a21336cef2e7e -->
## Verify Call code

> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/verify/verifyCall" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/verify/verifyCall"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": "true"
}
```
> Example response (200):

```json
{
    "success": "false",
    "error": "Error message"
}
```

### HTTP Request
`POST api/verify/verifyCall`


<!-- END_294da5c22e4eac712d7a21336cef2e7e -->

#general


<!-- START_66df3678904adde969490f2278b8f47f -->
## Authenticate the request for channel access.

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/broadcasting/auth" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/broadcasting/auth"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET broadcasting/auth`

`POST broadcasting/auth`


<!-- END_66df3678904adde969490f2278b8f47f -->

<!-- START_6b67a10116f49797d9fbe1c2ea7bad28 -->
## api/version
> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/version" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/version"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "version": "4.3.6"
}
```

### HTTP Request
`GET api/version`


<!-- END_6b67a10116f49797d9fbe1c2ea7bad28 -->

<!-- START_2c5553fa9f67c27f1f00afd8ac8f55eb -->
## api/intercom/initialize
> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/intercom/initialize" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/intercom/initialize"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/intercom/initialize`


<!-- END_2c5553fa9f67c27f1f00afd8ac8f55eb -->

<!-- START_0f263e1e8d08b93ea544cd678d706e78 -->
## api/intercom/webhook
> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/intercom/webhook" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/intercom/webhook"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/intercom/webhook`


<!-- END_0f263e1e8d08b93ea544cd678d706e78 -->

<!-- START_50999491bc3a54c383d134cba750f58d -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/reviews" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/reviews"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/reviews`


<!-- END_50999491bc3a54c383d134cba750f58d -->

<!-- START_addef74ffb3ebdd2a128f801a9009fb7 -->
## api/reviews
> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/reviews" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/reviews"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/reviews`


<!-- END_addef74ffb3ebdd2a128f801a9009fb7 -->

<!-- START_f28c14203b1b4d273f19fb792d2eaa33 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/reviews/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/reviews/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/reviews/{review}`


<!-- END_f28c14203b1b4d273f19fb792d2eaa33 -->

<!-- START_55feaade8405effe70367323ddb75ed0 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://vpiski.ra-ru.ru/api/reviews/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/reviews/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/reviews/{review}`

`PATCH api/reviews/{review}`


<!-- END_55feaade8405effe70367323ddb75ed0 -->

<!-- START_3fa0b81a7faa0bd8e3f805e8a0b3a31d -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://vpiski.ra-ru.ru/api/reviews/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/reviews/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/reviews/{review}`


<!-- END_3fa0b81a7faa0bd8e3f805e8a0b3a31d -->

<!-- START_bfcd6d74bbc83bce32ce631c8899c314 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/claims" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/claims"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/claims`


<!-- END_bfcd6d74bbc83bce32ce631c8899c314 -->

<!-- START_b2c660fc4ecb1ea992ad130654b16682 -->
## api/claims
> Example request:

```bash
curl -X POST \
    "http://vpiski.ra-ru.ru/api/claims" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/claims"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/claims`


<!-- END_b2c660fc4ecb1ea992ad130654b16682 -->

<!-- START_b87f1fc8ca99a2fd9c1b562aa239ba40 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/claims/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/claims/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/claims/{claim}`


<!-- END_b87f1fc8ca99a2fd9c1b562aa239ba40 -->

<!-- START_5a26db0668ae284c67b0d2fee0e1a600 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://vpiski.ra-ru.ru/api/claims/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/claims/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/claims/{claim}`

`PATCH api/claims/{claim}`


<!-- END_5a26db0668ae284c67b0d2fee0e1a600 -->

<!-- START_09729ba6993c774cde50b46714401612 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://vpiski.ra-ru.ru/api/claims/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/claims/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/claims/{claim}`


<!-- END_09729ba6993c774cde50b46714401612 -->

<!-- START_a01264eb840920e7d0c2bf9e6f5d7b74 -->
## api/links/{alias}
> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/api/links/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/api/links/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/links/{alias}`


<!-- END_a01264eb840920e7d0c2bf9e6f5d7b74 -->

<!-- START_4f73af5dc79748b6174fb186322db867 -->
## {alias}
> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET {alias}`


<!-- END_4f73af5dc79748b6174fb186322db867 -->

<!-- START_4f73af5dc79748b6174fb186322db867 -->
## {alias}
> Example request:

```bash
curl -X GET \
    -G "http://vpiski.ra-ru.ru/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://vpiski.ra-ru.ru/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET {alias}`


<!-- END_4f73af5dc79748b6174fb186322db867 -->


