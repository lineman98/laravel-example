<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('public.widgets.head')

<body>

<div class="main @yield('main-class')">
    <div class="container">
        <div class="row">
            @yield('content')
        </div>
    </div>
</div>

@include('public.widgets.scripts')

</body>
</html>
