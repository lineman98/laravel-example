@extends('public.layout')

@section('title')
    Модель {{ $girl->name }}
@endsection

@section('main-class')
    girl
@endsection

@section('content')
    <style>
        .slick-slide a {
            height: 500px;
            background-size: cover;
            display: block;
            background-repeat: no-repeat;
        }

        .fancybox-video {
            width: auto;
            height: 90%;
            overflow: hidden;
        }
    </style>

    <div class="col-md-12">
        <div class="profile">
            <div class="row">
                <div class="col-md-5 col-sm-6">
                    <div class="girl-item">
                        <div class="slider-wraper">
                            @if($girl->publicVideos->count())
                                <div class="play" data-fancybox="video-gallery"
                                     href="#video{{$girl->publicVideos->first()->id}}">
                                    <img src="/img/play.png" alt="Смотреть видео">
                                    <span>Видео</span>
                                </div>
                                @foreach($girl->publicVideos as $video)
                                    @if($video->id != $girl->publicVideos->first()->id)
                                        <div style="display: none" data-fancybox="video-gallery"
                                             href="#video{{$video->id}}">
                                        </div>
                                    @endif
                                    <div style="display: none" id="video{{$video->id}}" class="fancybox-video">
                                        <video controls width="100%" height="100%">
                                            <source src="{{ $video->getUrl() }}" type="video/mp4">
                                        </video>
                                    </div>
                                @endforeach
                            @endif
                            <div class="girl-slider">
                                @foreach($girl->publicPhotos as $photo)
                                    <div>
                                        <a href="{{ $photo->getUrl('big') }}" data-fancybox="gallery"
                                           style="background-image: url({{ $photo->getUrl('big') }})"></a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="girl-slider-min">
                            @foreach($girl->publicPhotos as $photo)
                                <div>
                                    <div
                                        style="background-image:url({{ $photo->getUrl('small') }});width: 87px;height: 87px;background-size: cover;"></div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-6">
                    <div class="profile-desc">
                        <div class="name verify">
                            {{$girl->name}}
                            <img src="/img/verify.png" alt="Подтверждённая страница" title="Подтверждённая страница">
                        </div>

                        <div class="profile-info">
                            <h4>Данные</h4>
                            <div class="profile-list">

                                @if($girl->country)
                                    <div class="profile-item">
                                        <strong>Страна</strong> {{ $girl->country->name }}
                                    </div>
                                @endif
                                @if($girl->city)
                                    <div class="profile-item">
                                        <strong>Город</strong> {{ $girl->city->name }}
                                    </div>
                                @endif

                                @if($girl->age)
                                    <div class="profile-item">
                                        <strong>Возраст</strong> {{$girl->age}} лет
                                    </div>
                                @endif

                                @if($girl->height)
                                    <div class="profile-item">
                                        <strong>Рост</strong> {{$girl->height}} см
                                    </div>
                                @endif

                                @if($girl->weight)
                                    <div class="profile-item">
                                        <strong>Вес</strong> {{$girl->weight}} кг
                                    </div>
                                @endif

                                @if($girl->waist)
                                    <div class="profile-item">
                                        <strong>Параметры</strong>
                                        {{ $girl->bust }}/{{ $girl->waist }}/{{ $girl->hip }}
                                    </div>
                                @endif

                                @if($girl->hair)
                                    <div class="profile-item">
                                        <strong>Цвет волос</strong> {{ $girl->hair->name }}
                                    </div>
                                @endif

                                @if($girl->eye)
                                    <div class="profile-item">
                                        <strong>Цвет глаз</strong> {{ $girl->eye->name }}
                                    </div>
                                @endif

                                @if($girl->nationality)
                                    <div class="profile-item">
                                        <strong>Национальность</strong> {{ $girl->nationality->name }}
                                    </div>
                                @endif


                                @if($girl->boobs_size)
                                    <div class="profile-item">
                                        <strong>Размер груди</strong> {{ $girl->boobs_size }}
                                    </div>
                                @endif

                                <div class="profile-item">
                                    <strong>Тип
                                        груди</strong> {{ $girl->boobs_type === 'silicon' ? 'Силикон' : 'Натуральная' }}
                                </div>

                                <div class="profile-item">
                                    <strong>Загран паспорт</strong> @if($girl->has_foreign_passport)Есть@elseНет@endif
                                </div>

                                <div class="profile-item">
                                    <strong>Наличие виз</strong>
                                    @if($girl->visas->count())
                                        {{ $girl->visas->pluck('name')->join(',') }}
                                    @else
                                        <span class="trn">Нет</span>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
