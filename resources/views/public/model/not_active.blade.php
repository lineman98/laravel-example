@extends('public.layout')

@section('title')
    Ссылка недействительна
@endsection

@section('content')

    <div class="col-md-12 text-center">
        <h3>Ссылка недействительна. Ссылка активна в течение 2-х часов после выдачи.</h3>
    </div>

@endsection
