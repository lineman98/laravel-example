<?php

namespace Tests\Unit\Auth;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{

    use DatabaseTransactions;

    public function testMe()
    {
        $body = [
            'phone' => '+7969449133'.rand(0,9),
            'password' => 'qwerty123',
            'type' => 'girl',
            'name' => 'qqq',
            'invite' => 'good'
        ];

        // регаем юзера
        $response = $this->postJson('/api/auth/register', $body);
        $response->assertStatus(201);

        $data = $response->json('data');

        // входим
        $response = $this->get('/api/auth/me', [
            'Authorization' => 'Bearer '.$data['token']
        ]);

        $response->assertStatus(200);
        $data = $response->json('data');
        $this->assertEquals($body['phone'], $data['phone']);
        $this->assertEquals($body['type'], $data['type']);
        $this->assertEquals($body['name'], $data['name']);
    }

    public function testRegister()
    {
        $body = [
            'phone' => '+7999453422'.rand(0,9),
            'password' => 'qwerty123',
            'type' => 'girl',
            'name' => 'qqq',
            'invite' => 'good'
        ];
        $response = $this->postJson('/api/auth/register', $body);
        $response->assertStatus(201);

        $data = $response->json('data');
        $this->assertArrayHasKey('token', $data);
        $this->assertEquals($body['phone'], $data['phone']);
        $this->assertEquals($body['type'], $data['type']);
        $this->assertEquals($body['name'], $data['name']);
    }

    public function testLogin()
    {
        $body = [
            'phone' => '+7999473422'.rand(0,9),
            'password' => 'qwerty123',
            'type' => 'girl',
            'name' => 'qqq',
            'invite' => 'good'
        ];

        // регаем юзера
        $response = $this->postJson('/api/auth/register', $body);
        $response->assertStatus(201);

        // входим
        $response = $this->postJson('/api/auth/login', $body);
        $response->assertStatus(200);

        $data = $response->json('data');
        $this->assertArrayHasKey('token', $data);
        $this->assertEquals($body['phone'], $data['phone']);
    }

    public function testLogout()
    {
        $body = [
            'phone' => '+7999343422'.rand(0,9),
            'password' => 'qwerty123',
            'type' => 'girl',
            'name' => 'qqq',
            'invite' => 'good'
        ];

        // регаем юзера
        $response = $this->postJson('/api/auth/register', $body);
        $response->assertStatus(201);

        $data = $response->json('data');

        // входим
        $response = $this->get('/api/auth/me', [
            'Authorization' => 'Bearer '.$data['token']
        ]);

        $response->assertStatus(200);

        // выходим
        $response = $this->get('/api/auth/logout', [
            'Authorization' => 'Bearer '.$data['token']
        ]);

        $response->assertStatus(204);
    }
}
