<?php

namespace Tests\Unit\Auth;

use App\Entity\Verification\PhoneActivate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ResetPasswordControllerTest extends TestCase
{

    use DatabaseTransactions;

    public function testSend()
    {
        $body = [
            'phone' => '+7969443422'.rand(0,9),
            'password' => 'qwerty123',
            'type' => 'girl',
            'name' => 'qqq',
            'invite' => 'good'
        ];

        // регаем юзера
        $response = $this->postJson('/api/auth/register', $body);
        $response->assertStatus(201);

        // пробуем восстановить пароль
        $data = ['phone' => $body['phone']];
        $response = $this->postJson('/api/auth/reset/send', $data);

        $response->assertStatus(200);
        $response->assertJson(['success' => true]);

        // достаем код, который выслали юзеру
        $code = PhoneActivate::where('phone', $body['phone'])
            ->where('type', 'restore_password')
            ->first()
            ->code;

        // отправляем код
        $body = [
            'phone' => $data['phone'],
            'code' => $code,
            'new_password' => 'qwerty_check'
        ];

        $response = $this->postJson('/api/auth/reset/verify', $body);
        $response->assertStatus(200);
        $response->assertJson(['success' => true]);

        // входим
        $body['password'] = 'qwerty_check';
        $response = $this->postJson('/api/auth/login', $body);
        $response->assertStatus(200);

        $data = $response->json('data');
        $this->assertArrayHasKey('token', $data);
        $this->assertEquals($body['phone'], $data['phone']);
    }

}
