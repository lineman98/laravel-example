<?php

namespace Tests\Unit\Offers;

use App\Entity\Geo\City\City;
use App\Entity\Offers\Category\OfferCategory;
use App\Entity\Users\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class OffersControllerTest extends TestCase
{

    use DatabaseTransactions;

    public function testEdit()
    {

    }

    public function testIndex()
    {

    }

    public function testShow()
    {
        $body = [
            'phone' => '+7969449133'.rand(0,9),
            'password' => 'qwerty123',
            'type' => 'manager',
            'name' => 'qqq',
            'invite' => 'good',
        ];

        // регаем юзера
        $response = $this->postJson('/api/auth/register', $body);
        $response->assertStatus(201);

        $data = $response->json('data');

        // проставляем юзеру подтвержденный телефон
        User::find($data['id'])->setVerifiedPhone();

        $token = $data['token'];

        $city = City::query()->first();

        $category = OfferCategory::query()->first();

        $body = [
            'name' => 'test name',
            'category' => $category->id,
            'description' => 'Test text description',
            'country' => $city->country_id,
            'city' => $city->id,
            'age_from' => 18,
            'age_to' => 20,
            'weight_from' => 50,
            'weight_to' => 80,
            'height_from' => 160,
            'height_to' => 180,
            'boobs_from' => 1,
            'boobs_to' => 5,
            'boobs_type' => 'natural',
            'cost' => 1000,
            'meeting_date' => '22.03.2019',
            'possible_increase_cost' => '1',
            'only_with_reviews' => '0',
            'only_verified' => '1'
        ];

        // создаем оффер
        $response = $this->postJson('/api/offers', $body, [
            'Authorization' => 'Bearer '.$token
        ]);

        $response->assertStatus(201);

        $data = $response->json()['data'];

        $response = $this->get('/api/offers/'.$data['id'], [
            'Authorization' => 'Bearer '.$token
        ]);
        $data = $response->json()['data'];

        $this->assertEquals($body['name'], $data['name']);
        $this->assertEquals($body['description'], $data['description']);
        $this->assertEquals($body['country'], $data['country']['id']);
        $this->assertEquals($body['city'], $data['city']['id']);
        $this->assertEquals($body['age_from'], $data['age_from']);
        $this->assertEquals($body['age_to'], $data['age_to']);
        $this->assertEquals($body['weight_from'], $data['weight_from']);
        $this->assertEquals($body['weight_to'], $data['weight_to']);
        $this->assertEquals($body['height_from'], $data['height_from']);
        $this->assertEquals($body['height_to'], $data['height_to']);
        $this->assertEquals($body['boobs_from'], $data['boobs_from']);
        $this->assertEquals($body['boobs_to'], $data['boobs_to']);
        $this->assertEquals($body['boobs_type'], $data['boobs_type']);
        $this->assertEquals($body['cost'], $data['cost']);
        $this->assertIsBool(Carbon::parse($body['meeting_date'])->equalTo(Carbon::parse($data['meeting_date'])));
        $this->assertEquals((bool)$body['possible_increase_cost'], $data['possible_increase_cost']);
        $this->assertEquals((bool)$body['only_with_reviews'], $data['only_with_reviews']);
        $this->assertEquals((bool)$body['only_verified'], $data['only_verified']);
    }

    public function testCreate()
    {
        $body = [
            'phone' => '+7969449133'.rand(0,9),
            'password' => 'qwerty123',
            'type' => 'manager',
            'name' => 'qqq',
            'invite' => 'good',
        ];

        // регаем юзера
        $response = $this->postJson('/api/auth/register', $body);
        $response->assertStatus(201);

        $data = $response->json('data');

        // проставляем юзеру подтвержденный телефон
        User::find($data['id'])->setVerifiedPhone();

        $token = $data['token'];

        $city = City::query()->first();

        $category = OfferCategory::query()->first();

        $body = [
            'name' => 'test name',
            'category' => $category->id,
            'description' => 'Test text description',
            'country' => $city->country_id,
            'city' => $city->id,
            'age_from' => 18,
            'age_to' => 20,
            'weight_from' => 50,
            'weight_to' => 80,
            'height_from' => 160,
            'height_to' => 180,
            'boobs_from' => 1,
            'boobs_to' => 5,
            'boobs_type' => 'natural',
            'cost' => 1000,
            'meeting_date' => '22.03.2019',
            'possible_increase_cost' => '1',
            'only_with_reviews' => '0',
            'only_verified' => '1'
        ];

        // создаем оффер
        $response = $this->postJson('/api/offers', $body, [
            'Authorization' => 'Bearer '.$token
        ]);

        $response->assertStatus(201);

        $data = $response->json()['data'];
        $this->assertEquals($body['name'], $data['name']);
        $this->assertEquals($body['description'], $data['description']);
        $this->assertEquals($body['country'], $data['country']['id']);
        $this->assertEquals($body['city'], $data['city']['id']);
        $this->assertEquals($body['age_from'], $data['age_from']);
        $this->assertEquals($body['age_to'], $data['age_to']);
        $this->assertEquals($body['weight_from'], $data['weight_from']);
        $this->assertEquals($body['weight_to'], $data['weight_to']);
        $this->assertEquals($body['height_from'], $data['height_from']);
        $this->assertEquals($body['height_to'], $data['height_to']);
        $this->assertEquals($body['boobs_from'], $data['boobs_from']);
        $this->assertEquals($body['boobs_to'], $data['boobs_to']);
        $this->assertEquals($body['boobs_type'], $data['boobs_type']);
        $this->assertEquals($body['cost'], $data['cost']);
        $this->assertIsBool(Carbon::parse($body['meeting_date'])->equalTo(Carbon::parse($data['meeting_date'])));
        $this->assertEquals((bool)$body['possible_increase_cost'], $data['possible_increase_cost']);
        $this->assertEquals((bool)$body['only_with_reviews'], $data['only_with_reviews']);
        $this->assertEquals((bool)$body['only_verified'], $data['only_verified']);
    }

    public function testDestroy()
    {

    }

    public function testUpdate()
    {

    }

    public function testStore()
    {

    }
}
