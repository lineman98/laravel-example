<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGendersToNationalityTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nationality_translations', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('name_male')->nullable();
            $table->string('name_female')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nationality_translations', function (Blueprint $table) {
            $table->string('name');
            $table->dropColumn('name_male');
            $table->dropColumn('name_female');
        });
    }
}
