<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMethodInfoToPhoneActivates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phone_activates', function (Blueprint $table) {
            $table->string('method')->default('sms');
            $table->string('info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phone_activates', function (Blueprint $table) {
            $table->dropColumn('method');
            $table->dropColumn('info');
        });
    }
}
