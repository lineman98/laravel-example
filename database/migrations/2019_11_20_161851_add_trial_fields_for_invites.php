<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrialFieldsForInvites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invites', function (Blueprint $table) {
            $table->boolean('give_trial')->default('0');
            $table->boolean('trial_for_girls')->default('0');
            $table->boolean('trial_for_managers')->default('0');
            $table->unsignedInteger('trial_days')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invites', function (Blueprint $table) {
            $table->dropColumn('give_trial');
            $table->dropColumn('trial_for_girls');
            $table->dropColumn('trial_for_managers');
            $table->dropColumn('trial_days');
        });
    }
}
