<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_categories', function (Blueprint $table) {
            $table->increments('id');
        });
        Schema::create('offer_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('offer_category_id');
            $table->string('name');
            $table->string('locale')->index('locale_index');
            $table->unique(['offer_category_id', 'locale']);
            $table->foreign('offer_category_id')
                ->references('id')
                ->on('offer_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_category_translations');
        Schema::dropIfExists('offer_categories');
    }
}
