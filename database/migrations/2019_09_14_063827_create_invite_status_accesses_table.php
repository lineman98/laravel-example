<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInviteStatusAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invite_status_accesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invite_id');
            $table->unsignedBigInteger('status_id');
            $table->timestamps();

            $table->foreign('invite_id')
                ->references('id')
                ->on('invites')
                ->onDelete('cascade');

            $table->foreign('status_id')
                ->references('id')
                ->on('model_statuses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invite_status_accesses');
    }
}
