<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferHairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_hairs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('hair_id');
            $table->unsignedBigInteger('offer_id');
            $table->unique(['offer_id', 'hair_id']);
            $table->foreign('offer_id')
                ->references('id')
                ->on('offers')
                ->onDelete('cascade');
            $table->foreign('hair_id')
                ->references('id')
                ->on('hairs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_hairs');
    }
}
