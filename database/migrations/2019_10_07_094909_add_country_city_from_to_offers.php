<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCountryCityFromToOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->unsignedInteger('country_from_id')->nullable();
            $table->unsignedInteger('city_from_id')->nullable();

            $table->foreign('city_from_id')
                ->references('id')
                ->on('cities')
                ->onDelete('set null');

            $table->foreign('country_from_id')
                ->references('id')
                ->on('countries')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('country_from_id');
            $table->dropColumn('city_from_id');
        });
        //DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
