<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInviteIdForUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('invite_id')->nullable();
            $table->unsignedBigInteger('referrer_id')->nullable();

            $table->foreign('invite_id')
                ->references('id')
                ->on('invites')
                ->onDelete('set null');

            $table->foreign('referrer_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('invite_id');
            $table->dropColumn('referrer_id');
        });
    }
}
