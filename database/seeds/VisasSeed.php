<?php

use Illuminate\Database\Seeder;

class VisasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entity\Options\Visa\Visa::create([
            'ru' => [
                'name' => 'Шенгенская',
            ],
            'en' => [
                'name' => 'Schengen',
            ]
        ]);
        \App\Entity\Options\Visa\Visa::create([
            'ru' => [
                'name' => 'США',
            ],
            'en' => [
                'name' => 'USA',
            ]
        ]);
        \App\Entity\Options\Visa\Visa::create([
            'ru' => [
                'name' => 'Британская',
            ],
            'en' => [
                'name' => 'United Kingdom',
            ]
        ]);
    }
}
