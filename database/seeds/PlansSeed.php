<?php

use Illuminate\Database\Seeder;

class PlansSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan_model = app('rinvex.subscriptions.plan')->create([
            'name' => '3 месяца',
            'description' => '3 месяца функционала модели',
            'price' => 3000,
            'signup_fee' => 0,
            'invoice_period' => 3,
            'invoice_interval' => 'month',
            'trial_period' => 0,
            'trial_interval' => 'day',
            'sort_order' => 1,
            'currency' => 'RUB',
        ]);

        $plan_manager = app('rinvex.subscriptions.plan')->create([
            'name' => '1 месяц',
            'description' => '1 месяц функционала менеджера',
            'price' => 5000,
            'signup_fee' => 0,
            'invoice_period' => 1,
            'invoice_interval' => 'month',
            'trial_period' => 0,
            'trial_interval' => 'day',
            'sort_order' => 1,
            'currency' => 'RUB',
        ]);

    }
}
