<?php

use Illuminate\Database\Seeder;

class StatusesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['New', 'VIP', 'GOLD'];
        foreach ($statuses as $status) {
            \App\Entity\ModelStatus\ModelStatus::create([
                'name' => $status
            ]);
        }
    }
}
