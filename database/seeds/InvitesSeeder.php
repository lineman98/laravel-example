<?php

use Illuminate\Database\Seeder;

class InvitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entity\Invites\Invite::create([
            'code' => 'good',
            'type' => 'trial',
            'active' => 1,
            'user_id' => 1,
            'give_trial' => 1
        ]);
    }
}
