<?php

use Illuminate\Database\Seeder;

class GeoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Entity\Geo\Country\Country::class, 10)->create()->each(function ($country) {
           $country->cities()->save(factory(\App\Entity\Geo\City\City::class)->make());
        });
    }
}
