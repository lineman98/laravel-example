<?php

return [
    'map_relations' => [
        'offers' => \App\Entity\Offers\Offer::class,
        'users' => \App\Entity\Users\User::class,
        'tours' => \App\Entity\Tours\Tour::class,
    ],
];
