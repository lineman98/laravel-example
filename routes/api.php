<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function() {
    Route::post('register', 'Auth\AuthController@register');
    Route::post('login', 'Auth\AuthController@login');
    Route::get('me', 'Auth\AuthController@me');
    Route::get('logout', 'Auth\AuthController@logout');
    Route::prefix('reset')->group(function() {
        Route::post('/send', 'Auth\ResetPasswordController@send');
        Route::post('/verify', 'Auth\ResetPasswordController@verify');
    });
});

// GEO
Route::apiResource('countries', 'Geo\CountriesController');
Route::get('cities/top', 'Geo\CitiesController@getTop');
Route::apiResource('cities', 'Geo\CitiesController');

// Options
Route::get('options', 'Options\OptionsController@index');
Route::get('eyes', 'Options\EyesController@index');
Route::get('nationalities', 'Options\NationalitiesController@index');
Route::get('hairs', 'Options\HairsController@index');
Route::get('purposes', 'Options\PurposesController@index');
Route::get('offers/categories', 'Offers\CategoriesController@index');
/*Route::apiResource('eyes', 'Options\EyesController')-;
Route::apiResource('nationalities', 'Options\NationalitiesController');
Route::apiResource('hairs', 'Options\HairsController');
Route::apiResource('purposes', 'Options\PurposesController');
Route::apiResource('offers/categories', 'Offers\CategoriesController');*/
Route::get('/version', 'VersionController');
Route::post('/intercom/initialize', 'IntercomController@initialize');
Route::post('/intercom/webhook', 'IntercomController@webhook');

Route::middleware('auth:api')->group(function () {
    Route::middleware('phone_not_verified')->prefix('verify')->group(function() {
        Route::post('/send', 'Verification\PhoneVerificationsController@send');
        Route::post('/verify', 'Verification\PhoneVerificationsController@verify');
        Route::post('/verifyByFirebase', 'Verification\PhoneVerificationsController@verifyByFirebase');

        Route::post('/sendCall', 'Verification\PhoneVerificationsController@sendCall');
        Route::post('/verifyCall', 'Verification\PhoneVerificationsController@verifyCall');
    });

    // Profile
    Route::prefix('profile')->group(function () {
        Route::post('/changePassword', 'ProfileController@changePassword');
        Route::post('/notificationsChange', 'ProfileController@notificationsChange');
        /*Route::get('/getSubscriptionBill', 'ProfileController@getSubscriptionBill');
        Route::get('/getBill', 'ProfileController@getBill');*/
        Route::post('/addToBlackList', 'ProfileController@addToBlackList');
        Route::post('/removeFromBlackList', 'ProfileController@removeFromBlackList');
    });

    // Faq
    Route::get('/faq', 'Faq\FaqController@index');

    /*
    // проверка транзакции in app purchase
    Route::post('/transactions/check', 'Transactions\TransactionsController@check');
    Route::get('/transactions/payBillFromBalance/{id}', 'Transactions\TransactionsController@payBillFromBalance');
    */

    Route::middleware(['phone_verified', 'user_not_blocked'])->group(function() {

        // OFFERS ALL
        Route::middleware('admin')->group(function () {
            Route::prefix('offers')->group(function () {
                Route::post('{id}/activate', 'Offers\OffersController@activate');
                Route::post('{id}/block', 'Offers\OffersController@block');
            });
        });

        Route::prefix('offers/{offer}/users/{userId}')->group(function() {
            Route::post('/agree', 'Offers\OfferUserController@agree');
            Route::post('/refuse', 'Offers\OfferUserController@refuse');
        });

        Route::get('offers/all', 'Offers\OffersController@indexAll');
        Route::get('offers/my', 'Offers\OffersController@my');
        Route::get('offers/{id}/forModel', 'Offers\OffersController@showForModel');
        Route::post('offers/{id}/setEnded', 'Offers\OffersController@setEnded');
        Route::post('offers/{offer}/setViewed', 'Offers\OffersController@setViewed');
        Route::post('offers/{id}/close', 'Offers\OffersController@close');
        Route::post('offers/{id}/end', 'Offers\OffersController@end');
        Route::get('offers/{id}/getBill', 'Offers\OffersController@getBill');



        Route::apiResource('offers', 'Offers\OffersController');
        Route::apiResource('offers.applications', 'Offers\OfferUserController');

        /*Route::prefix('offer-links')->group(function() {
            Route::middleware('girl')->group(function () {
                Route::post('{id}/refuseFromModel', 'Offers\OfferLinksController@refuseFromModel');
                Route::post('{id}/agreeFromModel', 'Offers\OfferLinksController@agreeFromModel');
            });

            Route::middleware('manager')->group(function () {
                Route::post('modelsCount', 'Offers\OfferLinksController@modelsCount');
                Route::post('{id}/refuseFromManager', 'Offers\OfferLinksController@refuseFromManager');
                Route::post('{id}/agreeFromManager', 'Offers\OfferLinksController@agreeFromManager');
            });
        });*/

        // Tours
        /*Route::middleware('admin')->group(function () {
            Route::prefix('tours')->group(function () {
                Route::post('{id}/activate', 'Tours\ToursController@activate');
                Route::post('{id}/block', 'Tours\ToursController@block');
            });
            Route::apiResource('mass-messages', 'Messages\MassMessagesController');
            Route::get('mass-messages/{id}/start', 'Messages\MassMessagesController@start');

            // Admin Messages
            Route::prefix('messages-admin')->group(function() {
                Route::get('/', 'Messages\MessagesAdminController@index');
                Route::post('/', 'Messages\MessagesAdminController@create');
                Route::get('/listDialog', 'Messages\MessagesAdminController@listDialog');
                Route::get('/unreadCount', 'Messages\MessagesAdminController@unreadCount');
                Route::post('/markAsRead', 'Messages\MessagesAdminController@markAsRead');
            });
        });

        Route::middleware('user_verified')->group(function () {
            Route::get('tours/all', 'Tours\ToursController@indexAll');
            Route::get('tours/my', 'Tours\ToursController@my');
            Route::get('tours/{id}/forModel', 'Tours\ToursController@showForModel');
            Route::get('tours/{id}/getBill', 'Tours\ToursController@getBill');
            Route::post('tours/{id}/close', 'Tours\ToursController@close');
            Route::apiResource('tours', 'Tours\ToursController');
        });*/

        // Users
        Route::apiResource('users', 'Users\UsersController');
        Route::prefix('users')->group(function() {
            Route::post('{id}/addPushToken', 'Users\UsersController@addPushToken');
            Route::post('{id}/removePushToken', 'Users\UsersController@removePushToken');
            Route::post('{id}/photos', 'Users\UsersController@uploadPhoto');
            Route::post('{id}/videos', 'Users\UsersController@uploadVideo');
            Route::post('{id}/uploadVerifyPhoto', 'Users\UsersController@uploadVerifyPhoto');
            Route::post('{id}/removePhoto', 'Users\UsersController@removePhoto');
            Route::post('{id}/checkMedia', 'Users\UsersController@checkMedia');
            Route::post('{id}/sendVerifyRequest', 'Users\UsersController@sendVerifyRequest');
            Route::post('{id}/setAvatar', 'Users\UsersController@setAvatar');
            Route::post('{id}/setVerified', 'Users\UsersController@setVerified');
            Route::post('{id}/setUnverified', 'Users\UsersController@setUnverified');
            Route::post('{id}/setVerifiedPhone', 'Users\UsersController@setVerifiedPhone');
            Route::post('{id}/extend', 'Users\UsersController@extendSubscription');

            Route::post('{id}/block', 'Users\UsersController@setBlock');
            Route::get('{id}/reviews', 'Users\UsersController@reviews');
            Route::get('{id}/link', 'Users\UsersController@link');
        });

        // Messages
        Route::prefix('messages')->group(function() {
            Route::get('/', 'Messages\MessagesController@index');
            Route::post('/', 'Messages\MessagesController@create');
            Route::post('/dialog/{dialog_id}', 'Messages\MessagesController@createByDialog');
            Route::get('/listDialog', 'Messages\MessagesController@listDialog');
            Route::get('/unreadCount', 'Messages\MessagesController@unreadCount');
            Route::post('/markAsRead', 'Messages\MessagesController@markAsRead');
            Route::post('/markDialogAsRead', 'Messages\MessagesController@markDialogAsRead');
        });

        Route::apiResource('reviews', 'Reviews\ReviewsController');
        Route::apiResource('claims', 'Claims\ClaimsController');

        Route::prefix('referrals')->group(function() {
            Route::get('/', 'Users\ReferralsController@index');
            Route::get('/stats', 'Users\ReferralsController@stats');
        });

        Route::prefix('links')->group(function () {
            Route::get('{alias}', 'Links\LinksController');
        });
    });
});
