<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Notifications\OfferApplicationAgreed;
use App\Notifications\NewOfferApplication;

Route::domain(config('links.short_link_domain'))->group(function () {
    Route::get('{alias}', 'GirlLinksController');
});

Route::domain(config('links.short_link_domain_two'))->group(function () {
    Route::get('{alias}', 'GirlLinksController');
});

/*
Route::get('foreign/payment/yandex', 'CheckoutController@yandex');
Route::post('foreign/payment/yandex', 'CheckoutController@yandex');
*/

Route::get('/test', 'TestController@index');
